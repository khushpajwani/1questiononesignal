package com.parental.control.teen.kid.math.education.utility;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.parental.control.teen.kid.math.education.R;
import com.google.android.material.textfield.TextInputLayout;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class PaddingTextInputLayout extends TextInputLayout {

    public View paddingView;

    public PaddingTextInputLayout(Context context) {
        super(context);
    }

    public PaddingTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaddingTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        refreshPaddingView();
    }

    public void addPaddingView(){
        paddingView = new View(getContext());
        int height = (int) getContext().getResources()
                .getDimension(R.dimen._50sdp);
        ViewGroup.LayoutParams paddingParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                height);
        super.addView(paddingView, 0, paddingParams);
    }

    public void refreshPaddingView(){
        if (paddingView != null) {
            removeView(paddingView);
            paddingView = null;
        }
        addPaddingView();
    }
}