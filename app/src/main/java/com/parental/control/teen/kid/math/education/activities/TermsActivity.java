package com.parental.control.teen.kid.math.education.activities;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.databinding.ActivityTermsBinding;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.Utils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class TermsActivity extends AppCompatActivity implements ClickHandler {



    ActivityTermsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(TermsActivity.this);
        super.onCreate(savedInstanceState);
        binding=ActivityTermsBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        binding.headerBase.txtTitle.setText(getResources().getString(R.string.tc));

        binding.headerBase.layoutHeaderBase.setBackgroundColor(ContextCompat.getColor(TermsActivity.this,android.R.color.white));

        Utils.createLink(binding.txtDesc, binding.txtDesc.getText().toString(), "Email@Website.com", new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Toast.makeText(TermsActivity.this, "dsf", Toast.LENGTH_SHORT).show();
            }
        });
//        Linkify.addLinks(binding.txtDesc, Linkify.EMAIL_ADDRESSES);

//        Email@Website.com

    }

    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        onBackPressed();

    }

}