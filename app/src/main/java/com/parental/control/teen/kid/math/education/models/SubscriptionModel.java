package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class SubscriptionModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("statusCode")
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("is_active")
        private boolean is_active;

        public boolean getIs_active() {
            return is_active;
        }

        public void setIs_active(boolean is_active) {
            this.is_active = is_active;
        }
    }
}
