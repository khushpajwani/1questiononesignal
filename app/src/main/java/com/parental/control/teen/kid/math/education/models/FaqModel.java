package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */
public class FaqModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("statusCode")
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("contact_email")
        private String contact_email;
        @SerializedName("contact_message")
        private String contact_message;
        @SerializedName("faqs")
        private List<Faqs> faqs;

        public String getContact_email() {
            return contact_email;
        }

        public void setContact_email(String contact_email) {
            this.contact_email = contact_email;
        }

        public String getContact_message() {
            return contact_message;
        }

        public void setContact_message(String contact_message) {
            this.contact_message = contact_message;
        }

        public List<Faqs> getFaqs() {
            return faqs;
        }

        public void setFaqs(List<Faqs> faqs) {
            this.faqs = faqs;
        }
    }

    public static class Faqs {
        @SerializedName("sub_questions")
        private List<Sub_questions> sub_questions;
        @SerializedName("title")
        private String title;

        public List<Sub_questions> getSub_questions() {
            return sub_questions;
        }

        public void setSub_questions(List<Sub_questions> sub_questions) {
            this.sub_questions = sub_questions;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class Sub_questions {
        @SerializedName("description")
        private String description;
        @SerializedName("sub_question")
        private String sub_question;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSub_question() {
            return sub_question;
        }

        public void setSub_question(String sub_question) {
            this.sub_question = sub_question;
        }
    }
}
