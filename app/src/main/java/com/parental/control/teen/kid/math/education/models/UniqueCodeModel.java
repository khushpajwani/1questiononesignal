package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class UniqueCodeModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("statusCode")
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("device_token")
        private String device_token;
        @SerializedName("child_link_code")
        private String child_link_code;
        @SerializedName("avatar")
        private String avatar;
        @SerializedName("device_info")
        private String device_info;
        @SerializedName("android_id")
        private String android_id;
        @SerializedName("no_of_questions")
        private String no_of_questions;
        @SerializedName("topic")
        private String topic;
        @SerializedName("subject")
        private String subject;
        @SerializedName("grade")
        private String grade;
        @SerializedName("childname")
        private String childname;
        @SerializedName("parent_code")
        private String parent_code;
        @SerializedName("parent_id")
        private String parent_id;
        @SerializedName("child_id")
        private String child_id;
        @SerializedName("remember_token")
        private String remember_token;

        public String getRemember_token() {
            return remember_token;
        }

        public void setRemember_token(String remember_token) {
            this.remember_token = remember_token;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getChild_link_code() {
            return child_link_code;
        }

        public void setChild_link_code(String child_link_code) {
            this.child_link_code = child_link_code;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getDevice_info() {
            return device_info;
        }

        public void setDevice_info(String device_info) {
            this.device_info = device_info;
        }

        public String getAndroid_id() {
            return android_id;
        }

        public void setAndroid_id(String android_id) {
            this.android_id = android_id;
        }

        public String getNo_of_questions() {
            return no_of_questions;
        }

        public void setNo_of_questions(String no_of_questions) {
            this.no_of_questions = no_of_questions;
        }

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getChildname() {
            return childname;
        }

        public void setChildname(String childname) {
            this.childname = childname;
        }

        public String getParent_code() {
            return parent_code;
        }

        public void setParent_code(String parent_code) {
            this.parent_code = parent_code;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getChild_id() {
            return child_id;
        }

        public void setChild_id(String child_id) {
            this.child_id = child_id;
        }
    }
}
