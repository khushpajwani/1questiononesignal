package com.parental.control.teen.kid.math.education.listeners;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface RatingDialogListener {
    public void notnow(int popupType);
    public void ratenow(int popupType);
    public void emailus(int popupType);

}
