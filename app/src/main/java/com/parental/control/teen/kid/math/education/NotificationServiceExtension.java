package com.parental.control.teen.kid.math.education;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import com.onesignal.OSNotification;
import com.onesignal.OSMutableNotification;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal.OSRemoteNotificationReceivedHandler;

@SuppressWarnings("unused")
public class NotificationServiceExtension implements OSRemoteNotificationReceivedHandler {

    @Override
    public void remoteNotificationReceived(Context context, OSNotificationReceivedEvent notificationReceivedEvent) {
        OSNotification notification = notificationReceivedEvent.getNotification();

        // Example of modifying the notification's accent color
        OSMutableNotification mutableNotification = notification.mutableCopy();
        mutableNotification.setExtender(builder ->
                builder.setColor(context.getResources().getColor(R.color.colorPrimary))
                        .setShowWhen(false));

        JSONObject data = notification.getAdditionalData();
        String data1;
        data1 = notification.getRawPayload();
        Log.i("OneSignalExample1", "Received Notification Data: " + data);
        Log.i("OneSignalExample1", "Received Notification Data: " + data1.contains("bgn"));

        // If complete isn't call within a time period of 25 seconds, OneSignal internal logic will show the original notification
        // To omit displaying a notification, pass `null` to complete()
        if (data1.contains("bgn") == true) {
            notificationReceivedEvent.complete(null);/*mutableNotification*/
        } else {
            notificationReceivedEvent.complete(mutableNotification);/*mutableNotification*/
        }
    }
}