package com.parental.control.teen.kid.math.education.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.ActivityResult;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.Task;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.AppConfig;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivitySplashBinding;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.Utils;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class SplashActivity extends AppCompatActivity {

    Context context;
    int MY_REQUEST_CODE = 101;
    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(SplashActivity.this);
        super.onCreate(savedInstanceState);
        binding=ActivitySplashBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);


        Utils.statusbarTransparent(this, true, true);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        context = SplashActivity.this;


        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("SplashEvent");
//        if (isTablet(context)) {
//
//        }else {

            if (getIntent() != null && getIntent().getBooleanExtra("killapp", false)) {
                //now there is no backstack, so just finish
                SplashActivity.this.finish();
            } else {
                checkUpdate();
//            splashFlow();

//            }
        }



    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }

    /**
     *inbuilt app update
     */
    private void checkUpdate() {
        InstallStateUpdatedListener listener = state -> {
            if (state.installStatus() == InstallStatus.FAILED || state.installStatus() == InstallStatus.CANCELED ) {
                Utils.setToast(context,context.getResources().getString(R.string.some_error_downloading_update));
            }
        };

        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateManager.registerListener(listener);
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            Log.e("TAG", "checkUpdate: "+ appUpdateInfo.availableVersionCode());

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE, SplashActivity.this, MY_REQUEST_CODE);
                } catch (Exception e) {
                    splashFlow();
                }
            }else  {
                splashFlow();
            }
        });
        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                splashFlow();
            }
        });

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE && (resultCode == RESULT_CANCELED||resultCode == ActivityResult.RESULT_IN_APP_UPDATE_FAILED)) {
            SplashActivity.this.finish();
        }
    }

    /**
     * get hash, sha, redirect
     */
    private void splashFlow() {
        hash();
        getCertificateSHA1Fingerprint();

        if (StringUtils.isEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.RATING_DATE))) {
            try {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_MONTH, 9);
                Date c = cal.getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String installDate = df.format(c);
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.RATING_DATE, installDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        Utils.resetNewDay();
        redirectToNewScreen();
    }


    private void redirectToNewScreen() {
        long milli = 3000;
        if (context.getResources().getBoolean(R.bool.isGoDevice)) {
            milli = 1500;
            SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, true);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                moveAnotherScreen();
            }
        }, milli);
    }


    private void moveAnotherScreen() {
        Intent intent = null;

        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.ISLOGGEDIN, false)) {
            if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.FIRSTTIME_USER, true)) {
                intent = new Intent(context, OnboardingScreenActivity.class);
            } else {
//                intent = new Intent(context, LoginNewDesignActivity.class);
                intent = new Intent(context, DashboardActivity.class);
            }
        } else {
            intent = new Intent(context, GetStartedActivity.class);
        }

        startActivity(intent);
        finish();
    }

    //
    @SuppressLint("PackageManagerGetSignatures")
    private void hash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("SHA key : ", md.toString());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash key : ", something);
                System.out.println("Hash key : " + something);
            }
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private void getCertificateSHA1Fingerprint() {
        PackageManager pm = this.getPackageManager();
        String packageName = this.getPackageName();
        int flags = PackageManager.GET_SIGNATURES;
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Signature[] signatures = packageInfo.signatures;
        byte[] cert = signatures[0].toByteArray();
        InputStream input = new ByteArrayInputStream(cert);
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        X509Certificate c = null;
        try {
            c = (X509Certificate) cf.generateCertificate(input);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        String hexString = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(c.getEncoded());
            Log.e("SHA : ", byte2HexFormatted(publicKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String byte2HexFormatted(byte[] arr) {
        StringBuilder str = new StringBuilder(arr.length * 2);
        for (int i = 0; i < arr.length; i++) {
            String h = Integer.toHexString(arr[i]);
            int l = h.length();
            if (l == 1) h = "0" + h;
            if (l > 2) h = h.substring(l - 2, l);
            str.append(h.toUpperCase());
            if (i < (arr.length - 1)) str.append(':');
        }
        return str.toString();
    }


}
