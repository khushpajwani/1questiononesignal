package com.parental.control.teen.kid.math.education.activities;

import android.annotation.SuppressLint;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityPermissionBinding;
import com.parental.control.teen.kid.math.education.receivers.AdminReceiver;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.Utils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */


@SuppressLint("UseSwitchCompatOrMaterialCode")
public class PermissionActivity extends AppCompatActivity implements ClickHandler {




    DevicePolicyManager dpm;
    ComponentName componentName;
    PowerManager powerManager;
    Context context;
    ActivityPermissionBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(PermissionActivity.this);
        super.onCreate(savedInstanceState);
        binding=ActivityPermissionBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = PermissionActivity.this;
        SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.FIRSTTIME_USER, false);

        dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        componentName = AdminReceiver.getComponentName(PermissionActivity.this);
        powerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);

        nextbtnState();


    }

    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutAppUsage:
                if (!Utils.isAccessibilitySettingsOn(PermissionActivity.this)) {
                    startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                }
                break;
            case R.id.layoutOverlay:
                if (!Settings.canDrawOverlays(PermissionActivity.this)) {
                    startActivity(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION));
                }
                break;
//            case R.id.layoutAdmin:
//                if (!dpm.isAdminActive(componentName)) {
//                    startActivity(new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN).putExtra(EXTRA_DEVICE_ADMIN, componentName));
//                }
//                break;
            case R.id.layoutBattery:
                if (!powerManager.isIgnoringBatteryOptimizations(PermissionActivity.this.getPackageName())) {
                    @SuppressLint("BatteryLife")
                    Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    intent.setData(Uri.parse("package:" + PermissionActivity.this.getPackageName()));
                    startActivity(intent);
                }
                break;
            case R.id.btnAction:
                startActivity(new Intent(PermissionActivity.this, DashboardActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;


        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        selfCheck();
    }

    /**
     * check permission status
     */
    private void selfCheck() {


        if (Utils.isAccessibilitySettingsOn(PermissionActivity.this)) {
            binding.trackAppUsage.setImageResource(R.drawable.switch_on);
        } else {
            binding.trackAppUsage.setImageResource(R.drawable.switch_off);
        }
        if (Settings.canDrawOverlays(PermissionActivity.this)) {
            binding.trackOverlay.setImageResource(R.drawable.switch_on);
        } else {
            binding.trackOverlay.setImageResource(R.drawable.switch_off);
        }
//        if (dpm.isAdminActive(componentName)) {
//            setSelectColor(true, txtAdmin, layoutAdmin, imgAdmin, swtAdmin);
//        } else {
//            setSelectColor(false, txtAdmin, layoutAdmin, imgAdmin, swtAdmin);
//        }

        if (powerManager.isIgnoringBatteryOptimizations(PermissionActivity.this.getPackageName())) {
            binding.trackBattery.setImageResource(R.drawable.switch_on);
        } else {
            binding.trackBattery.setImageResource(R.drawable.switch_off);
        }


        nextbtnState();
    }

    /**
     * change state of confirm buttom
     */
    private void nextbtnState() {
        if (Utils.checkAllPermissionsGranted(PermissionActivity.this, dpm, componentName, powerManager)) {
            binding.btnAction.setBackgroundResource(R.drawable.btn_new_normal);
            binding.btnAction.setTextColor(ContextCompat.getColor(context, android.R.color.white));
            binding.btnAction.setEnabled(true);
        } else {
            binding.btnAction.setBackgroundResource(R.drawable.btn_new_disable);
            binding.btnAction.setTextColor(ContextCompat.getColor(context, android.R.color.black));
            binding.btnAction.setEnabled(false);
        }
    }


    /**
     * back pressed from screen
     */
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (getIntent() != null && getIntent().getBooleanExtra(AppConstants.IntentKeys.FORM_PERMISSION_DIALOG, false)) {
            onBackPressed();
        } else {
            Utils.exitApp(PermissionActivity.this);
        }
    }





}