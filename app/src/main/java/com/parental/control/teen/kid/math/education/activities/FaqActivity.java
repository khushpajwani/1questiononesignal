package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.adapters.FaqAdapter;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityFaqBinding;
import com.parental.control.teen.kid.math.education.models.FaqModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class FaqActivity extends AppCompatActivity implements ServiceCallBack, ClickHandler {


    Context context;
    FaqAdapter faqAdapter;
    ArrayList<FaqModel.Faqs> faqsArrayList;
    ActivityFaqBinding binding;

    /**
     * initalize , api call, adapter
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(FaqActivity.this);
        super.onCreate(savedInstanceState);
        binding=ActivityFaqBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = FaqActivity.this;
        binding.headerBase.txtTitle.setText(getResources().getString(R.string.contact));

        binding.headerBase.layoutHeaderBase.setBackgroundColor(ContextCompat.getColor(context,android.R.color.white));
        faqsArrayList=new ArrayList<>();

        faqAdapter = new FaqAdapter(context, faqsArrayList);
        binding.recFaq.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        binding.recFaq.setAdapter(faqAdapter);
        binding.txtEmail.setPaintFlags(binding.txtEmail.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        RxApiRequestHandler.getFaq(context, FaqActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));
    }

    /**
     * 200 success respons of api
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_FAQ)) {
            try {
                FaqModel faqModel = new Gson().fromJson(data, FaqModel.class);
                binding.txtMessage.setText(faqModel.getData().getContact_message());
                binding.txtEmail.setText(faqModel.getData().getContact_email());
                faqsArrayList.addAll(faqModel.getData().getFaqs());
                faqAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * fail response from api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        Utils.setToast(context, message);
    }


    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        Utils.hideKeypad(context, view);
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.txtEmail:
                Utils.logEvent("contact_us", null);
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{binding.txtEmail.getText().toString()});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
                startActivity(Intent.createChooser(emailIntent, context.getResources().getString(R.string.send_email)));
                break;

        }
    }
}