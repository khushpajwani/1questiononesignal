package com.parental.control.teen.kid.math.education.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.databinding.ActivityGetStartedBinding;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.Utils;


/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class GetStartedActivity extends AppCompatActivity implements ClickHandler {



    ActivityGetStartedBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(GetStartedActivity.this);
        super.onCreate(savedInstanceState);
        binding=ActivityGetStartedBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false,android.R.color.white);

    }


    /**
     * any view clicked form api
     * @param view
     */

    @Override
    public void onViewClicked(View view) {
        startActivity(new Intent(GetStartedActivity.this, LinkAccountActivity.class));

    }
}
