package com.parental.control.teen.kid.math.education.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.databinding.ItemsFaqBinding;
import com.parental.control.teen.kid.math.education.databinding.ItemsSubBinding;
import com.parental.control.teen.kid.math.education.models.FaqModel;
import com.parental.control.teen.kid.math.education.utility.ExpandableLayout;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.RewardViewHolder> {
    private Context context;
    private ArrayList<FaqModel.Faqs> faqsArrayList;
    LayoutInflater inflater;

    public FaqAdapter(Context context, ArrayList<FaqModel.Faqs> faqsArrayList) {
        this.context = context;
        this.faqsArrayList = faqsArrayList;

    }

    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(context);
        return new RewardViewHolder(ItemsFaqBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RewardViewHolder holder, @SuppressLint("RecyclerView") final int position) {



        holder.binding.txtSubTitle.setText(faqsArrayList.get(position).getTitle());

        try {
            for (int i = 0; i <faqsArrayList.get(position).getSub_questions().size() ; i++) {
                ItemsSubBinding bindingsub = ItemsSubBinding.inflate(inflater, holder.binding.layourDesc,false);
                holder.binding.layourDesc.addView(bindingsub.getRoot());
                if(StringUtils.isNoneEmpty(faqsArrayList.get(position).getSub_questions().get(i).getSub_question())) {
                    bindingsub.txtSubQue.setVisibility(View.VISIBLE);
                    bindingsub.txtSubQue.setText(faqsArrayList.get(position).getSub_questions().get(i).getSub_question());
                }else {
                    bindingsub.txtSubQue.setVisibility(View.GONE);
                }
                if(StringUtils.isNoneEmpty(faqsArrayList.get(position).getSub_questions().get(i).getDescription())) {
                    bindingsub.txtSubMessage.setVisibility(View.VISIBLE);
                    bindingsub.txtSubMessage.setText(faqsArrayList.get(position).getSub_questions().get(i).getDescription());
                }else {
                    bindingsub.txtSubMessage.setVisibility(View.GONE);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.binding.layoutQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(holder.binding.layoutExpand.isExpanded()){
                        holder.binding.layoutExpand.collapse(true);
                        holder.binding.imgArrow.animate().rotation(0).start();
                    }else {
                        holder.binding.layoutExpand.expand(true);
                        holder.binding.imgArrow.animate().rotation(90).start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return faqsArrayList.size();
    }


    class RewardViewHolder extends RecyclerView.ViewHolder {

        ItemsFaqBinding binding;

        RewardViewHolder(@NonNull ItemsFaqBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;

        }
    }
}
