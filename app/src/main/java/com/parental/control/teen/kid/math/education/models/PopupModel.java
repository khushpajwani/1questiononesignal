package com.parental.control.teen.kid.math.education.models;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class PopupModel {

    private String id;
    private boolean checked;
    private String name;

    public PopupModel(String id, boolean checked, String name) {
        this.id = id;
        this.checked = checked;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
