package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class QuestionsNewModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<Data> data;
    @SerializedName("statusCode")
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("answer")
        private List<Answer> answer;
        @SerializedName("question_name")
        private String question_name;
        @SerializedName("topic_name")
        private String topic_name;
        @SerializedName("fq_id")
        private String fq_id;

        public String getTopic_name() {
            return topic_name;
        }

        public void setTopic_name(String topic_name) {
            this.topic_name = topic_name;
        }

        public List<Answer> getAnswer() {
            return answer;
        }

        public void setAnswer(List<Answer> answer) {
            this.answer = answer;
        }

        public String getQuestion_name() {
            return question_name;
        }

        public void setQuestion_name(String question_name) {
            this.question_name = question_name;
        }

        public String getFq_id() {
            return fq_id;
        }

        public void setFq_id(String fq_id) {
            this.fq_id = fq_id;
        }
    }

    public static class Answer {
        @SerializedName("is_correct")
        private String is_correct;
        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private String id;
        @SerializedName("index")
        private String index;

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getIs_correct() {
            return is_correct;
        }

        public void setIs_correct(String is_correct) {
            this.is_correct = is_correct;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
