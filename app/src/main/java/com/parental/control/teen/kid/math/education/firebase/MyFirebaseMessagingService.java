/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain popup_bg copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.parental.control.teen.kid.math.education.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.models.NotificationModel;
import com.parental.control.teen.kid.math.education.receivers.NotificationReceiver;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import java.util.Random;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private LocalBroadcastManager broadcaster;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.DEVICE_ID, s);
//        onMessageReceived(new RemoteMessage(new Bundle()));

    }

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Bundle[{google.delivered_priority=normal, google.sent_time=1622789091763,
//                notification_type=Value for key_1, google.ttl=2419200,
//                google.original_priority=normal, body=Body of Your Notification in Data12,
//                from=936875113782, title=Title of Your Notification in Title, user_type=Value for key_2,
//                google.message_id=0:1622789091775428%774b28a1f9fd7ecd, google.c.sender.id=936875113782}]
//        ArrayMap@19857, size = 4
//        {"notification_type":"Value for key_1","body":"Body of Your Notification in Data12","title":"Title of Your Notification in Title","user_type":"Value for key_2"}
        try {
            if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.ISLOGGEDIN, false)) {
                Log.e("TAG", "onMessageReceived: Data > " + remoteMessage.getData());
                if (remoteMessage.getNotification() == null) {

//                    NotificationModel notificationModel = new Gson().fromJson("{\"notification_type\":\"Value for key_1\",\"body\":\"Body of Your Notification in Data12\",\"title\":\"Title of Your Notification in Title\",\"user_type\":\"Value for key_2\"}", NotificationModel.class);

                    NotificationModel notificationModel = new Gson().fromJson(new Gson().toJson(remoteMessage.getData()), NotificationModel.class);

                    if (notificationModel.getNotification_type().equalsIgnoreCase("restart")) {
                        SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, false);
                        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                    } else if (notificationModel.getNotification_type().equalsIgnoreCase("pause_all_apps")) {
                        SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, true);
                        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                    } else if (notificationModel.getNotification_type().equalsIgnoreCase("app_update")) {
                        getUpdateAppList();
                    }/*else if (notificationModel.getNotification_type().matches("(?i)101|102|103")) {
                        sendNotification_general(notificationModel, true);

                    }*/
                    else{
                        sendNotification_general(notificationModel, false);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void getUpdateAppList() {
        RxApiRequestHandler.getAppList(this, new ServiceCallBack() {
            @Override
            public void onSuccess(String requestTag, JsonElement data) {
                try {
                    AppLockNewModel appLockNewModel = new Gson().fromJson(data, AppLockNewModel.class);
                    if (appLockNewModel != null && appLockNewModel.getData() != null && appLockNewModel.getData().size() > 0) {
                        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(appLockNewModel.getData()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String requestTag, String message, String errorcode) {

            }
        }, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));
    }


    private void sendNotification_general(NotificationModel notificationModel, boolean showActionButton) {
        try {
            Random rnd = new Random();
            int notificationID = 100000 + rnd.nextInt(900000);
            NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            String CHANNEL_ID = "1Q_ID_OTHER";
            if (notificationManager != null && Build.VERSION.SDK_INT >= 26) {
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "1Q_OTHER", NotificationManager.IMPORTANCE_HIGH);
                mChannel.setSound(soundUri, audioAttributes);
                notificationManager.createNotificationChannel(mChannel);
            }

            PendingIntent pendingIntentPositive = null;
            PendingIntent pendingIntentNegative = null;
            if (showActionButton) {
                //Yes intent
                Intent yesReceive = new Intent();
                yesReceive.setAction("poitive");
                yesReceive.setClass(this, NotificationReceiver.class);
                yesReceive.putExtra("data", new Gson().toJson(notificationModel));
                yesReceive.putExtra("notificationID", notificationID);
                pendingIntentPositive = PendingIntent.getBroadcast(this, notificationID, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);

                //No intent
                Intent noReceive = new Intent();
                noReceive.setAction("negative");
                noReceive.setClass(this, NotificationReceiver.class);
                noReceive.putExtra("data", new Gson().toJson(notificationModel));
                noReceive.putExtra("notificationID", notificationID);
                pendingIntentNegative = PendingIntent.getBroadcast(this, notificationID, noReceive, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            Intent generalintent = new Intent();
            generalintent.setClass(this, NotificationReceiver.class);
            generalintent.putExtra("data", new Gson().toJson(notificationModel));
            generalintent.putExtra("notificationID", notificationID);
            PendingIntent pendingIntentGeneral = PendingIntent.getBroadcast(this, notificationID, generalintent, PendingIntent.FLAG_UPDATE_CURRENT);

            int icn = 0;
            if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
                icn = R.drawable.ic_noti_go;
            } else {
                icn = R.drawable.ic_noti;
            }

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setSmallIcon(icn)
                    .setColor(ContextCompat.getColor(this, R.color.selected))
                    .setContentTitle(notificationModel.getTitle())
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setWhen(0)
                    .setDefaults(Notification.DEFAULT_ALL);


            if (showActionButton) {
                notificationBuilder.addAction(0, "Ok", pendingIntentPositive);
                notificationBuilder.addAction(0, "Cancel", pendingIntentNegative);
            } else {
                notificationBuilder.setContentIntent(pendingIntentGeneral);
            }

            String message = "";
            message = notificationModel.getBody();
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            notificationBuilder.setContentText(message);
            if (notificationManager != null) {
                notificationManager.notify(notificationID, notificationBuilder.build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
