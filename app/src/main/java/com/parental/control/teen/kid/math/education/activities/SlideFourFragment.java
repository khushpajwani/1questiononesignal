package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.FragmentSlideFourBinding;
import com.parental.control.teen.kid.math.education.listeners.SelectedAvatarListener;
import com.parental.control.teen.kid.math.education.models.AvatarModel;
import com.parental.control.teen.kid.math.education.models.UpdateAvatarModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class SlideFourFragment extends Fragment implements ServiceCallBack, ClickHandler {


    Context context;
    ArrayList<AvatarModel.Data> avatarModelArrayList;
    AvatarModel.Data selected_avatarModel;
    FragmentSlideFourBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding=FragmentSlideFourBinding.inflate(inflater,container,false);
        binding.setClickHandler(this);
        View view=binding.getRoot();
        return view;
    }

    /**
     * check if current screen is visible
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            context = getActivity();
            initData();
        }
        else {
        }
    }

    /**
     * get avatar list
     */
    private void initData() {
        avatarModelArrayList= new ArrayList<>();
        Glide.with(context).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(binding.imgAvatar);

        RxApiRequestHandler.getAvatarList(context, SlideFourFragment.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID),true);



    }


    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if (context instanceof OnboardingScreenActivity) {
                    DialogUtils.avatarDialog(getActivity(), -1,avatarModelArrayList, new SelectedAvatarListener() {
                        @Override
                        public void madeselection(int popupType, AvatarModel.Data avatarModel) {
                            Glide.with(context).load(avatarModel.getName()).placeholder(R.drawable.placeholder_avatar).into(binding.imgAvatar);
                            selected_avatarModel=avatarModel;
                            RxApiRequestHandler.updateAvatar(context,SlideFourFragment.this
                                    ,SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                                    ,avatarModel.getId());
                        }

                        @Override
                        public void cancelselection(int popupType) {

                        }
                    });
                }
                break;
            case R.id.txtSkip:
                if (context instanceof OnboardingScreenActivity) {
                    ((OnboardingScreenActivity) context).skipall();
                }

                break;
        }
    }

    /**
     * 200 success api response
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_AVATAR_LIST)) {
            try {
                AvatarModel avatarModel=new Gson().fromJson(data,AvatarModel.class);
                avatarModelArrayList.addAll(avatarModel.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else if(requestTag.equalsIgnoreCase(AppConstants.ApiTags.UPDATE_AVATAR)){
            try {
                UpdateAvatarModel updateAvatarModel=new Gson().fromJson(data,UpdateAvatarModel.class);
                Utils.setToast(context,updateAvatarModel.getMessage());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.AVATAR_URL,selected_avatarModel.getName());
                startActivity(new Intent(context, PermissionActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * fail response of api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        Utils.setToast(context, message);
    }




}