package com.parental.control.teen.kid.math.education.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.parental.control.teen.kid.math.education.activities.PauseActivity;
import com.parental.control.teen.kid.math.education.databinding.ItemsHoursBinding;
import com.parental.control.teen.kid.math.education.models.HoursModel;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class HoursAdapter extends RecyclerView.Adapter<HoursAdapter.RewardViewHolder> {
    private Context context;
    private ArrayList<HoursModel.Data> hoursModelArrayList;
    PopupWindow popUp;

    public HoursAdapter(Context context, ArrayList<HoursModel.Data> hoursModelArrayList, PopupWindow popUp) {
        this.context = context;
        this.hoursModelArrayList = hoursModelArrayList;
        this.popUp = popUp;

    }

    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new RewardViewHolder(ItemsHoursBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RewardViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (position + 1 == hoursModelArrayList.size()) {
            holder.binding.dividerBottom.setVisibility(View.GONE);
        } else {
            holder.binding.dividerBottom.setVisibility(View.VISIBLE);

        }


        holder.binding.txtName.setText(hoursModelArrayList.get(position).getName());
        holder.binding.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (context instanceof PauseActivity) {
                        ((PauseActivity) context).selectedHour(hoursModelArrayList.get(position));
                        popUp.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return hoursModelArrayList.size();
    }


    class RewardViewHolder extends RecyclerView.ViewHolder {

        ItemsHoursBinding binding;

        RewardViewHolder(@NonNull ItemsHoursBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;

        }
    }
}
