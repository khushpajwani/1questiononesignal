package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public  class GetNotiModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("statusCode")
    private String statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("updated_at")
        private String updated_at;
        @SerializedName("created_at")
        private String created_at;
        @SerializedName("in_app_notification")
        private String in_app_notification;
        @SerializedName("push_notification")
        private String push_notification;
        @SerializedName("email")
        private String email;
        @SerializedName("parent_code")
        private String parent_code;
        @SerializedName("id")
        private String id;

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getIn_app_notification() {
            return in_app_notification;
        }

        public void setIn_app_notification(String in_app_notification) {
            this.in_app_notification = in_app_notification;
        }

        public String getPush_notification() {
            return push_notification;
        }

        public void setPush_notification(String push_notification) {
            this.push_notification = push_notification;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getParent_code() {
            return parent_code;
        }

        public void setParent_code(String parent_code) {
            this.parent_code = parent_code;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
