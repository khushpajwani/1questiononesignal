package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.adapters.HoursAdapter;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityPauseBinding;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.models.CancelPauseModel;
import com.parental.control.teen.kid.math.education.models.CurrentPauseModel;
import com.parental.control.teen.kid.math.education.models.HoursModel;
import com.parental.control.teen.kid.math.education.models.SavePauseModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class PauseActivity extends AppCompatActivity implements ServiceCallBack, ClickHandler, SimpleDialogListener {


    Context context;
    ArrayList<HoursModel.Data> hoursModelArrayList;
    HoursAdapter hoursAdapter;
    HoursModel.Data selectedHour;
    Handler handler;
    Runnable runnable;
    SimpleDateFormat sdf;
    ActivityPauseBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(PauseActivity.this);
        super.onCreate(savedInstanceState);
        binding = ActivityPauseBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        View view = binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = PauseActivity.this;
        initData();
    }

    /**
     * api call , get current pause status, get hours list
     */
    private void initData() {
        hoursModelArrayList = new ArrayList<>();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        handler = new Handler();
        Glide.with(context).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(binding.imgAvatar);
        binding.txtChildName.setText(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_NAME));
        RxApiRequestHandler.getcurrentPauseStatus(context, PauseActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID));
        RxApiRequestHandler.getHoursList(context, PauseActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID), true);
        binding.btnUpdate.setBackgroundResource(R.drawable.btn_new_disable);
        binding.btnUpdate.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        binding.btnUpdate.setEnabled(false);


    }

    /**
     * any view clicked form layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.txtPause:
                binding.layoutPauseBar.setVisibility(View.GONE);
                binding.layoutUpdate.setVisibility(View.VISIBLE);
                binding.txtSelect.setText("");
                binding.txtSelect.clearFocus();
                break;
            case R.id.layoutRestart:
                RxApiRequestHandler.cancelPause(context, PauseActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                        , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID));
                break;
            case R.id.btnUpdate:
                if (selectedHour != null) {
                    Calendar futureCal = Calendar.getInstance();
                    long futureCOffset = -1;
                    if (selectedHour.getValue().equalsIgnoreCase("0.5")) {
                        futureCal.add(Calendar.MINUTE, 30);
                        futureCOffset=futureCal.getTimeInMillis();
//                        futureCOffset = System.currentTimeMillis() + ((1000 * 60) * 30);
                    } else if (selectedHour.getValue().equalsIgnoreCase("0")) {
                        futureCal.add(Calendar.YEAR, 10);
                        futureCOffset=futureCal.getTimeInMillis();
//                        futureCOffset = System.currentTimeMillis() + (((1000 * 60) * 60) * 24/* * 365  * 10*/);
                    } else {
                        futureCal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(selectedHour.getValue()));
                        futureCOffset=futureCal.getTimeInMillis();
//                        futureCOffset = System.currentTimeMillis() + (((1000 * 60) * 60) * Integer.parseInt(selectedHour.getValue()));
                    }
//                    futureCOffset = System.currentTimeMillis() + (1000 * 60);

                    futureCal.setTimeInMillis(futureCOffset);
                    Calendar currentCal = Calendar.getInstance();
//                    Log.e("TAG", "onViewClicked: current "+ sdf.format(currentCal.getTime()));
//                    Log.e("TAG", "onViewClicked: future "+ sdf.format(futureCal.getTime()));

                    RxApiRequestHandler.setPauseSettings(context, PauseActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                            , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID), selectedHour.getId(), sdf.format(currentCal.getTime())
                            , sdf.format(futureCal.getTime()), TimeZone.getDefault().getID());
                } else {
                    Utils.setToast(context, context.getResources().getString(R.string.please_select_hours));
                }
                break;
            case R.id.txtSelect:
                try {
                    View mView = LayoutInflater.from(PauseActivity.this).inflate(R.layout.popup_pause, null, false);
                    PopupWindow popUp = new PopupWindow(mView, (int) context.getResources().getDimension(R.dimen._180sdp), LinearLayout.LayoutParams.WRAP_CONTENT, false);
                    RecyclerView recHours = mView.findViewById(R.id.recHours);
                    hoursAdapter = new HoursAdapter(context, hoursModelArrayList, popUp);
                    recHours.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                    recHours.setAdapter(hoursAdapter);
                    popUp.setTouchable(true);
                    popUp.setFocusable(true);
                    popUp.setOutsideTouchable(true);
                    popUp.showAsDropDown(findViewById(R.id.txtSelect));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    /**
     * 200 success response from api
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_CURRENT_PAUSE)) {
            try {
                CurrentPauseModel currentPauseModel = new Gson().fromJson(data, CurrentPauseModel.class);
                binding.layoutPauseBar.setVisibility(View.VISIBLE);
                binding.layoutUpdate.setVisibility(View.GONE);

                if (currentPauseModel.getData().getCurrentpausestatus()) {
                    SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, true);
                    SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                    binding.txtPause.setVisibility(View.GONE);
                    binding.layoutRestart.setVisibility(View.VISIBLE);
                    if (StringUtils.isNotEmpty(currentPauseModel.getData().getPause_future_time())) {
//                        if(!currentPauseModel.getData().getValue().equalsIgnoreCase("0")) {
//                            Date futuremDate = sdf.parse(currentPauseModel.getData().getPause_future_time());
//                            checkTimer(futuremDate.getTime());
//                        }else {
//                            binding.txtPauseTime.setVisibility(View.VISIBLE);
//                            binding.txtPauseTime.setText(context.getResources().getString(R.string.pausing_for) + " " +context.getResources().getString(R.string.untill_turn_on));
//                        }
                        binding.txtPauseTime.setVisibility(View.VISIBLE);
                        binding.txtPauseTime.setText(context.getResources().getString(R.string.pausing_for) + " " +currentPauseModel.getData().getName());

                    }
                } else {
                    SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, false);
                    SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                    binding.txtPause.setVisibility(View.VISIBLE);
                    binding.layoutRestart.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_HOURS_LIST)) {
            try {
                HoursModel hoursModel = new Gson().fromJson(data, HoursModel.class);
                for (int i = 0; i < hoursModel.getData().size(); i++) {
                    if (!hoursModel.getData().get(i).getValue().equalsIgnoreCase("0.5")) {
                        hoursModelArrayList.add(new HoursModel.Data(hoursModel.getData().get(i).getValue()
                                , hoursModel.getData().get(i).getName()
                                , hoursModel.getData().get(i).getId()));
                    }

                }
                hoursAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SET_PAUSE)) {
            try {
                SavePauseModel savePauseModel = new Gson().fromJson(data, SavePauseModel.class);
                Utils.setToast(context, savePauseModel.getMessage());
                DialogUtils.simpleDialog(context, context.getResources().getString(R.string.app_updated_dialog), context.getResources().getString(R.string.close)
                        , AppConstants.PopupType.PAUSE_SET, PauseActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.CANCEL_PAUSE)) {
            try {
                CancelPauseModel cancelPauseModel = new Gson().fromJson(data, CancelPauseModel.class);
                Utils.setToast(context, cancelPauseModel.getMessage());
                getLatestPauseStatus();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * show pause time
     * @param futuretime
     */
    private void checkTimer(long futuretime) {
        try {

            if (runnable != null) {
                handler.removeCallbacks(runnable);
                runnable = null;
            }
            runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        handler.postDelayed(this, 1000);
                        Calendar currentCal = Calendar.getInstance();
                        String currentDateString = sdf.format(currentCal.getTime());
                        Date currentDate = sdf.parse(currentDateString);
                        long diff = futuretime - currentDate.getTime();
                        //                                long Days = diff / (24 * 60 * 60 * 1000);
                        long Hours = diff / (60 * 60 * 1000)/* % 24*/;
                        long Minutes = diff / (60 * 1000) % 60;
                        long Seconds = diff / 1000 % 60;
                        String timer = String.format("%02d", Hours) + "h:" + String.format("%02d", Minutes) + "m:" + String.format("%02ds", Seconds);
                        if (timer.startsWith("00h:")) {
                            timer = timer.replace("00h:", "");
                        }
                        if (Hours <= 0 && Minutes <= 0 && Seconds <= 0) {
                            binding.txtPauseTime.setVisibility(View.GONE);
                            SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, false);
                            SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                            binding.txtPause.setVisibility(View.VISIBLE);
                            binding.layoutRestart.setVisibility(View.GONE);
                        } else {
                            binding.txtPauseTime.setVisibility(View.VISIBLE);
                        }
                        timer = context.getResources().getString(R.string.pausing_for) + " " + timer;
                        binding.txtPauseTime.setText(timer);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.postDelayed(runnable, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * get latest pause time
     */
    private void getLatestPauseStatus() {
        RxApiRequestHandler.getcurrentPauseStatus(context, PauseActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID));
    }


    /**
     * fail response of api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        Utils.setToast(context, message);
    }

    /**
     * selected hours from list
     * @param data
     */
    public void selectedHour(HoursModel.Data data) {
        selectedHour = data;
        binding.txtSelect.setText(selectedHour.getName());
        binding.btnUpdate.setBackgroundResource(R.drawable.btn_new_normal);
        binding.btnUpdate.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        binding.btnUpdate.setEnabled(true);
    }

    /**
     * button pressed from alert dialog
     * @param popupType
     */
    @Override
    public void buttonPressed(int popupType) {
        if(popupType==AppConstants.PopupType.PAUSE_SET){
            getLatestPauseStatus();
        }
    }
}