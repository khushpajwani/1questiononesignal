package com.parental.control.teen.kid.math.education.webservice;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.models.ReportRequestModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class RxApiRequestHandler {


    /*@SuppressLint("HardwareIds")
    public static void registerApi(Context context, ServiceCallBack callBack, String name, String childname, String email, String password) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("name", name);
            jsonObject.addProperty("childname", childname);
            jsonObject.addProperty("email", email);
            jsonObject.addProperty("password", password);
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "parent_registration.php", AppConstants.ApiTags.REGISTER, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    @SuppressLint("HardwareIds")
    public static void registerApiV2(Context context, ServiceCallBack callBack, String name, String email, String password) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("name", name);
            jsonObject.addProperty("email", email);
            jsonObject.addProperty("password", password);
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/parent_registration.php", AppConstants.ApiTags.REGISTER, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public static void loginApiV2(Context context, ServiceCallBack callBack, String email, String password) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", email);
            jsonObject.addProperty("password", password);
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/login.php", AppConstants.ApiTags.LOGIN, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void forogtApi(Context context, ServiceCallBack callBack, String email, String android_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", email);
            jsonObject.addProperty("android_id", android_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "forget_password.php", AppConstants.ApiTags.FORGOT, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void updateParentApi(Context context, ServiceCallBack callBack, String parent_id, String parent_name, String parent_email, String parent_password) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_name", parent_name);
            jsonObject.addProperty("parent_email", parent_email);
            jsonObject.addProperty("parent_password", parent_password);
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "update_parent.php", AppConstants.ApiTags.UPDATE_PARENT, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static void updateParentApiv2(Context context, ServiceCallBack callBack, String parent_id, String parent_name, String parent_email, String parent_password, String parent_code) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_name", parent_name);
            jsonObject.addProperty("parent_email", parent_email);
            jsonObject.addProperty("parent_code", parent_code);

            jsonObject.addProperty("parent_password", parent_password);
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/parent_update_profile.php", AppConstants.ApiTags.UPDATE_PARENT, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void meterApi(Context context, ServiceCallBack callBack, String parent_id, String current_date) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("current_date", current_date);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "login_data.php", AppConstants.ApiTags.METER, callBack, jsonObject, null, false, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void validatePasswordAPI(Context context, ServiceCallBack callBack, String email, String password) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", email);
            jsonObject.addProperty("password", password);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/child_details_validate.php", AppConstants.ApiTags.VERIFY_PASSWORD, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void sendReportApi(Context context, ServiceCallBack callBack, String child_id, boolean is_first_attempt
            , boolean is_correct_answer, String current_date, String fq_id, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            if (is_first_attempt) {
                jsonObject.addProperty("is_first_attempt", "yes");
            } else {
                jsonObject.addProperty("is_first_attempt", "no");
            }
            if (is_correct_answer) {
                jsonObject.addProperty("is_correct_answer", "yes");
            } else {
                jsonObject.addProperty("is_correct_answer", "no");
            }
            jsonObject.addProperty("current_date", current_date);
            jsonObject.addProperty("fq_id", fq_id);


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "child_attempt.php", AppConstants.ApiTags.SEND_REPORT, callBack, jsonObject, null, false, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendReportApi2(Context context, ServiceCallBack callBack, String child_id, boolean is_first_attempt
            , boolean is_correct_answer, String current_date, String fq_id, String parent_id, ReportRequestModel.Answer answerModel) {
        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            if (is_first_attempt) {
                jsonObject.addProperty("is_first_attempt", "yes");
            } else {
                jsonObject.addProperty("is_first_attempt", "no");
            }
            if (is_correct_answer) {
                jsonObject.addProperty("is_correct_answer", "yes");
            } else {
                jsonObject.addProperty("is_correct_answer", "no");
            }
            jsonObject.addProperty("current_date", current_date);
            jsonObject.addProperty("fq_id", fq_id);
            jsonObject.add("answer", new Gson().fromJson(new Gson().toJson(answerModel), JsonElement.class));


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2_child_attempt.php", AppConstants.ApiTags.SEND_REPORT, callBack, jsonObject, null, false, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void getQuestionsJsonApi(Context context, ServiceCallBack callBack) {

        try {
            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "questions.json", AppConstants.ApiTags.GET_QUESTIONS, callBack, null, null, true, AppConstants.ApiType.GET, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static void getOverlayQuestionsApi(Context context, ServiceCallBack callBack, String parent_id, String child_id, String current_date) {

        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("current_date", current_date);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "get-question.php", AppConstants.ApiTags.GET_OVERLAY_QUESTIONS, callBack, jsonObject, null, false, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getQuestionSettingsApi(Context context, ServiceCallBack callBack, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "get-topic-settings.php", AppConstants.ApiTags.GET_QUESTION_SETTINGS, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getGradesApi(Context context, ServiceCallBack callBack, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "grades.php", AppConstants.ApiTags.GET_GRADES, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getSubjectsApi(Context context, ServiceCallBack callBack, String grade_id, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("grade_id", grade_id);
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "get-subject.php", AppConstants.ApiTags.GET_SUBJECTS, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getTopicsApi(Context context, ServiceCallBack callBack, String grade_id, String subject_id, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("grade_id", grade_id);
            jsonObject.addProperty("subject_id", subject_id);
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "get-topic.php", AppConstants.ApiTags.GET_TOPICS, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveQuestionSettingsApi(Context context, ServiceCallBack callBack, String grade_id, String subject_id, String topic_id,
                                               String unselected_topic_id, String parent_id, String number_of_question
            , String daily_question_limit, String code, String change_grade_subject, String child_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("grade_id", grade_id);
            jsonObject.addProperty("subject_id", subject_id);
            jsonObject.addProperty("topic_id", topic_id);
            jsonObject.addProperty("old_topic_id", unselected_topic_id);
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("number_of_question", number_of_question);
            jsonObject.addProperty("daily_question_limit", daily_question_limit);
            jsonObject.addProperty("code", code);
            jsonObject.addProperty("change_grade_subject", Integer.parseInt(change_grade_subject));
            jsonObject.addProperty("child_id", child_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "save-topic-settings.php", AppConstants.ApiTags.SAVE_QUESTION_SETTINGS, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void saveQuestionSettingsApi2(Context context, ServiceCallBack callBack, String grade_id, String subject_id, String topic_id,
                                                String unselected_topic_id, String parent_id, String number_of_question
            , String daily_question_limit, String code, String change_grade_subject, String reqTag) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("grade_id", grade_id);
            jsonObject.addProperty("subject_id", subject_id);
            jsonObject.addProperty("topic_id", topic_id);
            jsonObject.addProperty("old_topic_id", unselected_topic_id);
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("number_of_question", number_of_question);
            jsonObject.addProperty("daily_question_limit", daily_question_limit);
            jsonObject.addProperty("code", code);
            jsonObject.addProperty("change_grade_subject", Integer.parseInt(change_grade_subject));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "save-topic-settings.php", reqTag, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void addChild(Context context, ServiceCallBack callBack, String parent_id, String child_name, String parent_code) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_code", parent_code);
            jsonObject.addProperty("child_name", child_name);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/add_child.php", AppConstants.ApiTags.ADD_CHILD, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public static void linkChild(Context context, ServiceCallBack callBack, String parent_id, String child_id, String parent_code) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_code", parent_code);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/link_child.php", AppConstants.ApiTags.LINK_CHILD, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public static void getChilds(Context context, ServiceCallBack callBack, String parent_id, String parent_code) {

        try {

            Calendar cal = Calendar.getInstance();
            Date c = cal.getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String current_date = df.format(c);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_code", parent_code);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonObject.addProperty("week_start_date", current_date);
            jsonObject.addProperty("week_end_date", current_date);


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/child_list.php", AppConstants.ApiTags.GET_CHILDS, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void getParentProfile(Context context, ServiceCallBack callBack, String parent_id, String parent_code) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_code", parent_code);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/get_parent_info.php", AppConstants.ApiTags.PARENT_PROFILE, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendfcmToken(Context context, ServiceCallBack callBack, String child_id, String parent_code, String device_token, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("parent_code", parent_code);
            jsonObject.addProperty("device_token", device_token);
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v2/add_child_device_token.php", AppConstants.ApiTags.SEND_FCM_TOKEN, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void loginuniquecode(Context context, ServiceCallBack callBack, String child_link_code, String device_token) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("child_link_code", child_link_code);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);
            jsonObject.addProperty("device_token", device_token);
            jsonObject.addProperty("platform", "android");


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/child_login.php", AppConstants.ApiTags.LOGIN_UNIQUE_CODE, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getAvatarList(Context context, ServiceCallBack callBack, String parent_id,boolean isProgressNeeded) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/get_avatar.php", AppConstants.ApiTags.GET_AVATAR_LIST, callBack, jsonObject, null, isProgressNeeded, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateAvatar(Context context, ServiceCallBack callBack, String parent_id, String avatar_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("avatar_id", avatar_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/update_child_avatar.php", AppConstants.ApiTags.UPDATE_AVATAR, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("HardwareIds")
    public static void getAppList(Context context, ServiceCallBack callBack, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/get_app_lock_unlock.php", AppConstants.ApiTags.GET_APP_LIST, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public static void updateAppList(Context context, ServiceCallBack callBack, String parent_id, String child_id, List object) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("is_notification_send", false);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonObject.addProperty("device_info", "" + Build.MODEL + " " + Build.DEVICE + " " + Build.VERSION.RELEASE);
            jsonObject.add("app_list", new Gson().toJsonTree(object).getAsJsonArray());


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/app_lock_unlock.php", AppConstants.ApiTags.UPLOAD_APP_LIST, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkParentLogin(Context context, ServiceCallBack callBack, String parent_id, String password, String parent_code) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("pin", password);
            jsonObject.addProperty("parent_code", parent_code);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/parent_login.php", AppConstants.ApiTags.PARENT_LOGIN, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getDashboard(Context context, ServiceCallBack callBack, String parent_id, String child_id, String current_date) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("current_date", current_date);
            jsonObject.addProperty("filter_type", "day");


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/child_info.php", AppConstants.ApiTags.GET_DASHBOARD, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getHoursList(Context context, ServiceCallBack callBack, String parent_id,boolean isProgressNeeded) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/pause_question_time.php", AppConstants.ApiTags.GET_HOURS_LIST, callBack, jsonObject, null, isProgressNeeded, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public static void getcurrentPauseStatus(Context context, ServiceCallBack callBack, String parent_id, String child_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/get_pause_question_time_status.php", AppConstants.ApiTags.GET_CURRENT_PAUSE, callBack, jsonObject, null, false, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    public static void setPauseSettings(Context context, ServiceCallBack callBack, String parent_id, String child_id, String pause_question_time_id
            , String pause_current_time, String pause_future_time, String pause_timezone) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("pause_question_time_id", pause_question_time_id);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            jsonObject.addProperty("pause_current_time", pause_current_time);
            jsonObject.addProperty("pause_future_time", pause_future_time);
            jsonObject.addProperty("pause_timezone", pause_timezone);
            jsonObject.addProperty("platform", "android");


            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/save_pause_question_time.php", AppConstants.ApiTags.SET_PAUSE, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("HardwareIds")
    public static void cancelPause(Context context, ServiceCallBack callBack, String parent_id, String child_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("platform", "android");
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/cancel_pause_question_time.php", AppConstants.ApiTags.CANCEL_PAUSE, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getFaq(Context context, ServiceCallBack callBack, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("app_type", "child");

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/faqs.php", AppConstants.ApiTags.GET_FAQ, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void getCurrentSubStatus(Context context, ServiceCallBack callBack, String parent_id, String child_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("child_id", child_id);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/subscription_status.php", AppConstants.ApiTags.GET_CUR_SUB, callBack, jsonObject, null, false, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("HardwareIds")
    public static void forogtApiNew(Context context, ServiceCallBack callBack, String parent_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/forgot_pin.php", AppConstants.ApiTags.FORGOT_NEW, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void getnotificationSettings(Context context, ServiceCallBack callBack, String parent_id, String parent_code) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_code", parent_code);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/get_notification_setting.php", AppConstants.ApiTags.GET_NOTI_SETTING, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void savenotificationSettings(Context context, ServiceCallBack callBack, String parent_id, String parent_code
            , String in_app_notification, String push_notification, String email) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("parent_code", parent_code);
            jsonObject.addProperty("in_app_notification", in_app_notification);
            jsonObject.addProperty("push_notification", push_notification);
            jsonObject.addProperty("email", email);

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/save_notification_setting.php", AppConstants.ApiTags.SAVE_NOTI_SETTING, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("HardwareIds")
    public static void logoutApi(Context context, ServiceCallBack callBack, String parent_id, String child_id) {

        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("child_id", child_id);
            jsonObject.addProperty("parent_id", parent_id);
            jsonObject.addProperty("android_id", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

            RXRequestHandler handler = new RXRequestHandler();
            handler.makeApiRequest(context, "v3/child_log_out.php", AppConstants.ApiTags.LOGOUT, callBack, jsonObject, null, true, AppConstants.ApiType.POST, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





}