package com.parental.control.teen.kid.math.education.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.services.LockService;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.Utils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(@NonNull Context context, Intent intent) {
        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.ISLOGGEDIN, false)){
            SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
            Utils.startService(context, LockService.class, AppBroadcastReceiver.class, AppConstants.ServiceTypes.LOCK_REQ);
        }

    }
}
