package com.parental.control.teen.kid.math.education.webservice;

import com.google.gson.JsonElement;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface ServiceCallBack {
    void onSuccess(String requestTag, JsonElement data);


    void onFailure(String requestTag, String message, String errorcode);

}
