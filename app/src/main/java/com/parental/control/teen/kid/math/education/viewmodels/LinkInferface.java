package com.parental.control.teen.kid.math.education.viewmodels;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface LinkInferface {
    public void validatedAll(boolean allcorrect,String pin,String message);
}
