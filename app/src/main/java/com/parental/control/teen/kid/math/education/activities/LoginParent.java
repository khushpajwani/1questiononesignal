package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityParentloginBinding;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.models.ForgotPinModel;
import com.parental.control.teen.kid.math.education.models.HoursModel;
import com.parental.control.teen.kid.math.education.models.SavePauseModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.viewmodels.LoginInterface;
import com.parental.control.teen.kid.math.education.viewmodels.LoginViewModel;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LoginParent extends AppCompatActivity implements ServiceCallBack, ClickHandler, LoginInterface {


    Context context;
    boolean for_pause = false, for_settings = false;
    ActivityParentloginBinding binding;
    HoursModel.Data selectedHour;
    SimpleDateFormat sdf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(LoginParent.this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_parentlogin);
        LoginViewModel viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding.setViewmodel(viewModel);
        binding.getViewmodel().loginInterface = this;

        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);

        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);

        context = LoginParent.this;
        initData();
//        binding.getViewmodel().setPincode("1234");
        binding.txtForgot.setPaintFlags(binding.txtForgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


    }

    /**
     * check from which screen, reset error on text change for pin, api call
     */
    private void initData() {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for_pause = getIntent().getBooleanExtra(AppConstants.IntentKeys.FOR_PAUSE, false);
        for_settings = getIntent().getBooleanExtra(AppConstants.IntentKeys.FOR_SETTINGS, false);
        if (for_pause) {
            RxApiRequestHandler.getHoursList(context, LoginParent.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID), false);
        }
        binding.pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }


            @Override
            public void afterTextChanged(Editable s) {
                binding.pin.setLineColor(Color.parseColor("#C1C6D7"));
                binding.txtForgot.setVisibility(View.GONE);
                binding.layoutError.setVisibility(View.GONE);

            }
        });

    }


    /**
     * back pressed from screen
     */
    @Override
    public void onBackPressed() {
        if (for_pause || for_settings) {
            super.onBackPressed();
        } else {
            Utils.exitApp(context);
        }
    }


    /**
     * any view clicked from layout
     *
     * @param view
     */
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.txtForgot:
                RxApiRequestHandler.forogtApiNew(context, LoginParent.this
                        , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));
                break;
        }
    }

    /**
     * 200 success response freom api call
     *
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.PARENT_LOGIN)) {
            try {
                if (for_pause) {
                    if (selectedHour != null) {
                        Calendar futureCal = Calendar.getInstance();
                        long futureCOffset = -1;
//                        futureCOffset = System.currentTimeMillis() + ((1000 * 60) * 30);
                        futureCal.add(Calendar.MINUTE, 30);
                        futureCOffset = futureCal.getTimeInMillis();
                        futureCal.setTimeInMillis(futureCOffset);
                        Calendar currentCal = Calendar.getInstance();

                        RxApiRequestHandler.setPauseSettings(context, LoginParent.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID), selectedHour.getId(), sdf.format(currentCal.getTime())
                                , sdf.format(futureCal.getTime()), TimeZone.getDefault().getID());
                    }

                } else if (for_settings) {
                    startActivity(new Intent(context, SettingsActivity.class));
                    LoginParent.this.finish();

                } else {
                    startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.FORGOT_NEW)) {
            try {
                ForgotPinModel forgotPinModel = new Gson().fromJson(data, ForgotPinModel.class);
                DialogUtils.simpleDialog(context, forgotPinModel.getMessage(), context.getResources().getString(R.string.ok)
                        , AppConstants.PopupType.FORGOT_ALERT, new SimpleDialogListener() {
                            @Override
                            public void buttonPressed(int popupType) {
                                binding.pin.setLineColor(Color.parseColor("#C1C6D7"));
                                binding.txtForgot.setVisibility(View.GONE);
                                binding.layoutError.setVisibility(View.GONE);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_HOURS_LIST)) {
            try {
                HoursModel hoursModel = new Gson().fromJson(data, HoursModel.class);
                for (int i = 0; i < hoursModel.getData().size(); i++) {
                    if (hoursModel.getData().get(i).getValue().equalsIgnoreCase("0.5")) {
                        selectedHour = hoursModel.getData().get(i);
                        break;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SET_PAUSE)) {
            try {
                SavePauseModel savePauseModel = new Gson().fromJson(data, SavePauseModel.class);
                SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, true);
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                Utils.setToast(context, savePauseModel.getMessage());
                Utils.exitApp(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * fail response from api
     *
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.PARENT_LOGIN)) {
            binding.pin.setLineColor(ResourcesCompat.getColor(getResources(), R.color.nd_red, getTheme()));
            binding.layoutError.setVisibility(View.VISIBLE);
            binding.txtError.setText(message);
            binding.txtForgot.setVisibility(View.VISIBLE);

        }
    }


    /**
     * check all before api call
     *
     * @param allcorrect
     * @param pincode
     * @param message
     */

    @Override
    public void validateall(boolean allcorrect, String pincode, String message) {
        if (allcorrect) {
                RxApiRequestHandler.checkParentLogin(context, LoginParent.this
                    , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                    , pincode
                    , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PARENT_CODE));

            Log.d("geterrormessage", "this are param" + SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID) + "\n" + pincode + "\n" + SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PARENT_CODE));

        } else {
            binding.pin.setLineColor(ResourcesCompat.getColor(getResources(), R.color.nd_red, getTheme()));
            binding.layoutError.setVisibility(View.VISIBLE);
            binding.txtError.setText(message);
            Log.d("geterrormessage", "this is message" + message);
            binding.txtForgot.setVisibility(View.VISIBLE);
        }
    }
}