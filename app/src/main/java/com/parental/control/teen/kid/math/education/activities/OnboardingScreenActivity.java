package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.databinding.ActivityIntroBinding;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.Utils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class OnboardingScreenActivity extends AppCompatActivity implements ClickHandler {


    Context context;
    ActivityIntroBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityIntroBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false,R.color.nd_background);
        context= OnboardingScreenActivity.this;
        setUpViewPager();


    }

    /**
     * setup view pager
     */
    private void setUpViewPager() {

        binding.viewpagerPer.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        binding.indicatorPer.setViewPager(binding.viewpagerPer);
    }

    /**
     * skill all onboarding screen
     */
    public void skipall() {
        startActivity(new Intent(context, PermissionActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

    }

    @Override
    public void onViewClicked(View view) {

    }


    /**
     * adapter for slide
     */
    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return new SlideOneFragment();
                case 1: return new SlideTwoFragment();
                case 2: return new SlideThreeFragment();
                case 3: return new SlideFourFragment();
                default:return new SlideOneFragment();
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    /**
     * move to next slide
     */
    public void movetonext() {
        if(binding.viewpagerPer.getCurrentItem()+1!=4) {
            binding.viewpagerPer.setCurrentItem(binding.viewpagerPer.getCurrentItem() + 1);
        }
    }


}