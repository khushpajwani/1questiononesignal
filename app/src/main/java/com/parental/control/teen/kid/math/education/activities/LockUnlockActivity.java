package com.parental.control.teen.kid.math.education.activities;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.adapters.AppListAdapter;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityLockunlockBinding;
import com.parental.control.teen.kid.math.education.listeners.PermissionDialogListener;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.receivers.AdminReceiver;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LockUnlockActivity extends AppCompatActivity implements SimpleDialogListener, PermissionDialogListener, ServiceCallBack, ClickHandler {

    Context context;
    AppListAdapter appListAdapter;
    ArrayList<AppLockNewModel.Data> originalappDataArrayList;
    DevicePolicyManager dpm;
    ComponentName componentName;
    PowerManager powerManager;
    ActivityLockunlockBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(LockUnlockActivity.this);
        super.onCreate(savedInstanceState);
        binding = ActivityLockunlockBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        View view = binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparent(this, false, false);

        context = LockUnlockActivity.this;
        binding.headerBase.layoutHeaderBase.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

        init();
        searchinit();
    }


    /**
     * search logic from app list
     */
    private void searchinit() {
        binding.edtPSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (StringUtils.isNotEmpty(Utils.gettext(binding.edtPSearch))) {
                    binding.imgPClose.setVisibility(View.VISIBLE);
                } else {
                    binding.imgPClose.setVisibility(View.GONE);
                }
                if (originalappDataArrayList.size() > 0) {

                    appListAdapter.setfilterText(binding.edtPSearch.getText().toString());
                    appListAdapter.notifyDataSetChanged();
                    noData(false, true);
                } else {
                    noData(true, true);
                }

            }
        });

       /* edtPSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    getNearbyPlayers();
                }
                return false;
            }
        });*/
    }


    /**
     * get api call, adapter
     */
    private void init() {
        binding.headerBase.txtTitle.setText(getResources().getString(R.string.lockunlock));

        originalappDataArrayList = new ArrayList<>();
        appListAdapter = new AppListAdapter(context, originalappDataArrayList);
        binding.recAppList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        binding.recAppList.setAdapter(appListAdapter);
        dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        componentName = AdminReceiver.getComponentName(LockUnlockActivity.this);
        powerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);


        RxApiRequestHandler.getAppList(context, LockUnlockActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));


        binding.btnUpdate.setBackgroundResource(R.drawable.btn_new_disable);
        binding.btnUpdate.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        binding.btnUpdate.setEnabled(false);

    }


    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btnUpdate:

                RxApiRequestHandler.updateAppList(context, LockUnlockActivity.this
                        , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                        , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID)
                        , originalappDataArrayList);

                break;
            case R.id.imgPClose:
                if (originalappDataArrayList.size() > 0) {
                    binding.edtPSearch.setText("");
                    binding.edtPSearch.clearFocus();
                }
                break;
        }
    }


    /**
     * check if any app available or not
     * @param nodata
     * @param forsearch
     */
    private void noData(boolean nodata, boolean forsearch) {
        if (forsearch) {
            binding.layouNoData.setText(context.getResources().getString(R.string.no_search_found));
        } else {
            binding.layouNoData.setText(context.getResources().getString(R.string.no_app_found));
            if (nodata) {
                binding.edtPSearch.setEnabled(false);
            }
        }
        if (nodata) {
            binding.recAppList.setVisibility(View.GONE);
            binding.layouNoData.setVisibility(View.VISIBLE);
        } else {
            binding.layouNoData.setVisibility(View.GONE);
            binding.recAppList.setVisibility(View.VISIBLE);
        }
    }

    /**
     * enable button after app selected or unselected
     * @param isStart
     */
    public void updatedSelection(boolean isStart) {
        binding.btnUpdate.setBackgroundResource(R.drawable.btn_new_normal);
        binding.btnUpdate.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        binding.btnUpdate.setEnabled(true);

    }

    /**
     * dialog general button pressed
     * @param popupType
     */
    @Override
    public void buttonPressed(int popupType) {
        if (popupType == AppConstants.PopupType.QUESTION_ALREADY_DONE) {
            if (Utils.checkAllPermissionsGranted(context, dpm, componentName, powerManager)) {
                onBackPressed();
            } else {
                DialogUtils.permissionDialog(context, context.getResources().getString(R.string.permission_alert), context.getResources().getString(R.string.allow_permissions)
                        , AppConstants.PopupType.PERMISSIONS_MISSING, LockUnlockActivity.this);
            }
        } else if (popupType == AppConstants.PopupType.NOAPPS_SELECTED || popupType == AppConstants.PopupType.LOCK_SET) {
            onBackPressed();
        }
    }


    /**
     * positive button clicked from dialog
     * @param popupType
     */
    @Override
    public void positiveButtonPressed(int popupType) {
        startActivity(new Intent(context, PermissionActivity.class));
    }

    /**
     * negative button clicked from dialog
     * @param popupType
     */
    @Override
    public void negativeButtonPressed(int popupType) {
        onBackPressed();
    }

    /**
     * 200 success api call response
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_APP_LIST)) {
            try {
                AppLockNewModel appLockNewModel = new Gson().fromJson(data, AppLockNewModel.class);
                if (appLockNewModel != null && appLockNewModel.getData() != null && appLockNewModel.getData().size() > 0) {
                    SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(appLockNewModel.getData()));
                    noData(false, false);
                    originalappDataArrayList.addAll(new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
                    }.getType()));
                    appListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
                noData(true, false);
            }


        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.UPLOAD_APP_LIST)) {
            try {
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(originalappDataArrayList));
                boolean haveSelected = false;
                if (originalappDataArrayList.size() > 0) {
                    for (int i = 0; i < originalappDataArrayList.size(); i++) {
                        if (originalappDataArrayList.get(i).getStatus().getIsLocked()) {
                            haveSelected = true;
                            break;
                        }
                    }
                }
                if (!haveSelected) {
                    DialogUtils.twoDialogwithButton(context, context.getResources().getString(R.string.unlockall), context.getResources().getString(R.string.ok), context.getResources().getString(R.string.select_apps)
                            , AppConstants.PopupType.NOAPPS_SELECTED, LockUnlockActivity.this);
                } else {
                    DialogUtils.simpleDialog(context, context.getResources().getString(R.string.app_updated_dialog), context.getResources().getString(R.string.close)
                            , AppConstants.PopupType.LOCK_SET, LockUnlockActivity.this);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * fail of api response
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_APP_LIST)) {
            noData(true, false);
        }
        Utils.setToast(context, message);
    }


}
