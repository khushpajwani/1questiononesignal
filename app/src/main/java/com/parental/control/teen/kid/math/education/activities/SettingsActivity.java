package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivitySettingsBinding;
import com.parental.control.teen.kid.math.education.listeners.PermissionDialogListener;
import com.parental.control.teen.kid.math.education.models.LogoutModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import okhttp3.internal.Util;


/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class SettingsActivity extends AppCompatActivity implements ClickHandler, ServiceCallBack {


    Context context;
    ActivitySettingsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(SettingsActivity.this);
        super.onCreate(savedInstanceState);
        binding = ActivitySettingsBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        View view = binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = SettingsActivity.this;


    }

    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.layoutLockUnloack:
                startActivity(new Intent(SettingsActivity.this, LockUnlockActivity.class));
                break;
            case R.id.layoutPause:
                startActivity(new Intent(SettingsActivity.this, PauseActivity.class));
                break;
            case R.id.layoutTc:
                startActivity(new Intent(SettingsActivity.this, TermsActivity.class));
                break;
            case R.id.layoutContact:
                startActivity(new Intent(SettingsActivity.this, FaqActivity.class));
                break;
            case R.id.layoutNotification:
//                startActivity(new Intent(SettingsActivity.this, NotificationsActivity.class));
                Utils.setToast(context,"comming soon...");
                break;
            case R.id.layoutLogout:
                DialogUtils.logoutDialog(context, -1, new PermissionDialogListener() {
                    @Override
                    public void positiveButtonPressed(int popupType) {
                        RxApiRequestHandler.logoutApi(context, SettingsActivity.this
                                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID));
                    }

                    @Override
                    public void negativeButtonPressed(int popupType) {

                    }
                });
                break;
        }
    }

    /**
     * 200 success api response
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.LOGOUT)) {
            try {
                LogoutModel logoutModel = new Gson().fromJson(data, LogoutModel.class);
                Utils.setToast(context, logoutModel.getMessage());
                String token = SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.DEVICE_ID);
                SharedPreferenceUtils.clearPreference();
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.DEVICE_ID, token);
                startActivity(new Intent(context, GetStartedActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * fail response from api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        Utils.setToast(context, message);
    }
}