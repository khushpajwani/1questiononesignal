package com.parental.control.teen.kid.math.education.activities;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.adapters.AchievementsAdapter;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityDashboardBinding;
import com.parental.control.teen.kid.math.education.listeners.PermissionDialogListener;
import com.parental.control.teen.kid.math.education.listeners.SelectedAvatarListener;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.models.AvatarModel;
import com.parental.control.teen.kid.math.education.models.DashboardModel;
import com.parental.control.teen.kid.math.education.models.SubscriptionModel;
import com.parental.control.teen.kid.math.education.models.UpdateAvatarModel;
import com.parental.control.teen.kid.math.education.receivers.AdminReceiver;
import com.parental.control.teen.kid.math.education.receivers.AppBroadcastReceiver;
import com.parental.control.teen.kid.math.education.services.LoadAppListService;
import com.parental.control.teen.kid.math.education.services.LockService;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class DashboardActivity extends AppCompatActivity implements ServiceCallBack, ClickHandler {



    ArrayList<DashboardModel.Achievements> achievementsArrayList;
    ArrayList<AvatarModel.Data> avatarModelArrayList;
    AchievementsAdapter avatarAdapter;

    DevicePolicyManager dpm;
    ComponentName componentName;
    PowerManager powerManager;
    Context context;
    AvatarModel.Data selected_avatarModel;

    ActivityDashboardBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(DashboardActivity.this);
        super.onCreate(savedInstanceState);
        binding=ActivityDashboardBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = DashboardActivity.this;


        dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        componentName = AdminReceiver.getComponentName(DashboardActivity.this);
        powerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);

        binding.txtName.setText(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_NAME));
        Glide.with(context).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(binding.imgAvatar);

        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_SHOW_WELCOME, true)) {
            DialogUtils.welcomeDialog(context, context.getResources().getString(R.string.nice_work) + " " + SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_NAME), context.getResources().getString(R.string.get_started)
                    , -1, new SimpleDialogListener() {
                        @Override
                        public void buttonPressed(int popupType) {
                            SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_SHOW_WELCOME, false);
                            initData();

                        }
                    });
        } else {
            initData();
        }


    }

    /**
     * initialize adapter , controls and permission check
     */

    private void initData() {
        if (!Utils.checkAllPermissionsGranted(DashboardActivity.this, dpm, componentName, powerManager)) {
            DialogUtils.permissionDialog(context, context.getResources().getString(R.string.permission_alert), context.getResources().getString(R.string.allow_permissions)
                    , -1, new PermissionDialogListener() {
                        @Override
                        public void positiveButtonPressed(int popupType) {
                            startActivity(new Intent(context, PermissionActivity.class)
                                    .putExtra(AppConstants.IntentKeys.FORM_PERMISSION_DIALOG, true));
                        }

                        @Override
                        public void negativeButtonPressed(int popupType) {
                        }
                    });
        }
        avatarModelArrayList = new ArrayList<>();
        achievementsArrayList = new ArrayList<>();
        RxApiRequestHandler.getAppList(context, DashboardActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));
        RxApiRequestHandler.getAvatarList(context, DashboardActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID),false);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        RxApiRequestHandler.getDashboard(context, DashboardActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID), formattedDate);


        avatarAdapter = new AchievementsAdapter(context, achievementsArrayList);
        binding.recAchivements.setLayoutManager(new GridLayoutManager(context, 4));
        binding.recAchivements.setAdapter(avatarAdapter);

    }

    /**
     * for back button pressed
     */
    @Override
    public void onBackPressed() {
        Utils.exitApp(DashboardActivity.this);
    }

    /**
     * for any view click form layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEditAvatar:
                DialogUtils.avatarDialog(context, -1, avatarModelArrayList, new SelectedAvatarListener() {
                    @Override
                    public void madeselection(int popupType, AvatarModel.Data avatarModel) {
                        selected_avatarModel = avatarModel;
                        RxApiRequestHandler.updateAvatar(context, DashboardActivity.this
                                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                                , avatarModel.getId());
                    }

                    @Override
                    public void cancelselection(int popupType) {

                    }
                });
                break;
            case R.id.imgSettings:
                startActivity(new Intent(DashboardActivity.this, LoginParent.class)
                .putExtra(AppConstants.IntentKeys.FOR_SETTINGS,true));

                break;
        }
    }


    /**
     * Start service
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
//        RxApiRequestHandler.getCurrentSubStatus(context, DashboardActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
//                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID));
        Utils.startService(context, LockService.class, AppBroadcastReceiver.class, AppConstants.ServiceTypes.LOCK_REQ);

    }


    /**
     * 200 response of api
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_APP_LIST)) {
            try {
                AppLockNewModel appLockNewModel = new Gson().fromJson(data, AppLockNewModel.class);
                if (appLockNewModel != null && appLockNewModel.getData() != null && appLockNewModel.getData().size() > 0) {
                    SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(appLockNewModel.getData()));
                }
                startService(new Intent(DashboardActivity.this, LoadAppListService.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_AVATAR_LIST)) {
            try {
                AvatarModel avatarModel = new Gson().fromJson(data, AvatarModel.class);
                avatarModelArrayList.addAll(avatarModel.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.UPDATE_AVATAR)) {
            try {
                UpdateAvatarModel updateAvatarModel = new Gson().fromJson(data, UpdateAvatarModel.class);
                Utils.setToast(context, updateAvatarModel.getMessage());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.AVATAR_URL, selected_avatarModel.getName());
                Glide.with(context).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(binding.imgAvatar);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_DASHBOARD)) {
            try {
                DashboardModel dashboardModel = new Gson().fromJson(data, DashboardModel.class);
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.C_NAME, dashboardModel.getData().getChildname());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.AVATAR_URL, dashboardModel.getData().getAvatar());
                binding.txtName.setText(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_NAME));
                Glide.with(context).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(binding.imgAvatar);
                binding.txtYear.setText(dashboardModel.getData().getGrade());
                binding.txtYear.setTextColor(Color.parseColor(dashboardModel.getData().getGrade_color()));
                Drawable mDrawable = ContextCompat.getDrawable(context, R.drawable.light_green_draw).mutate();
                mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(dashboardModel.getData().getGrade_background_color()), PorterDuff.Mode.SRC_ATOP));
                binding.txtYear.setBackground(mDrawable);
                if (dashboardModel.getData().getDevice_link().equalsIgnoreCase("1")) {
                    binding.txtLink.setText(context.getResources().getString(R.string.device_liked));
                    binding.txtLink.setTextColor(ContextCompat.getColor(context, R.color.nd_btn));
                    Glide.with(context).load(R.drawable.green_tick_link).into(binding.imgLink);
                } else {
                    binding.txtLink.setText(context.getResources().getString(R.string.device_not_liked));
                    binding.txtLink.setTextColor(ContextCompat.getColor(context, R.color.nd_red));
                    Glide.with(context).load(R.drawable.warningred).into(binding.imgLink);
                }

                if(StringUtils.isNoneEmpty(dashboardModel.getData().getLast_answer())) {
                    binding.txtAgo.setText(context.getResources().getString(R.string.last_answered) + " " + dashboardModel.getData().getLast_answer());
                }else {
                    binding.txtAgo.setText(context.getResources().getString(R.string.last_answered) + " " +context.getResources().getString(R.string.hasnt_ans_que));
                }

                binding.txtAnswered.setText(dashboardModel.getData().getTotal_question());
                binding.txtCorrect.setText(dashboardModel.getData().getCorrect_answer() + "%");
                binding.txtTime.setText(dashboardModel.getData().getTotal_minutes());

                if (StringUtils.isNotEmpty(dashboardModel.getData().getAverage_speed())) {
                    String speedtext = dashboardModel.getData().getAverage_speed().replaceAll("[^A-Za-z]", "");
                    String speedtime = dashboardModel.getData().getAverage_speed().replaceAll("[^0-9]", "");
                    binding.txtSpeed1.setText(speedtime);
                    binding.txtSpeed2.setText(speedtext);
                }
                /*if (dashboardModel.getData().getAchievements() != null && dashboardModel.getData().getAchievements().size() > 0) {
                    achievementsArrayList.addAll(dashboardModel.getData().getAchievements());
                    avatarAdapter.notifyDataSetChanged();
                    binding.layoutNoData.setVisibility(View.GONE);
                    binding.txtViewAll.setVisibility(View.VISIBLE);
                } else {
                    binding.layoutNoData.setVisibility(View.VISIBLE);
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_CUR_SUB)) {
            try {
                SubscriptionModel subscriptionModel=new Gson().fromJson(data, SubscriptionModel.class);
                SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_ACTIVE_SUB,subscriptionModel.getData().getIs_active());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * failure of api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_APP_LIST)) {
            try {
                startService(new Intent(DashboardActivity.this, LoadAppListService.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_CUR_SUB)) {

        }else {
                Utils.setToast(context, message);

        }

    }
}
