package com.parental.control.teen.kid.math.education.viewmodels;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface LoginInterface {
    public void validateall(boolean allcorrect, String pincode, String message);
}
