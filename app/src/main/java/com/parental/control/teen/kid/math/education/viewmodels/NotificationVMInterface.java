package com.parental.control.teen.kid.math.education.viewmodels;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface NotificationVMInterface {
    public void getSettingValue(boolean inapp, boolean pn, boolean email);

}
