package com.parental.control.teen.kid.math.education.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.databinding.ItemsAchievementsBinding;
import com.parental.control.teen.kid.math.education.models.DashboardModel;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class AchievementsAdapter extends RecyclerView.Adapter<AchievementsAdapter.RewardViewHolder> {
    private Context context;
    private ArrayList<DashboardModel.Achievements> achievementsArrayList;

    public AchievementsAdapter(Context context, ArrayList<DashboardModel.Achievements> achievementsArrayList) {
        this.context = context;
        this.achievementsArrayList = achievementsArrayList;

    }

    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new RewardViewHolder(ItemsAchievementsBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RewardViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(context).load(achievementsArrayList.get(position).getImage()).placeholder(R.drawable.placeholder_avatar).into(holder.binding.imgAchievement);
        holder.binding.txtName.setText(achievementsArrayList.get(position).getName());
        holder.binding.txtName.setSelected(true);

    }


    @Override
    public int getItemCount() {
        return achievementsArrayList.size();
    }


    class RewardViewHolder extends RecyclerView.ViewHolder {

        ItemsAchievementsBinding binding;

        RewardViewHolder(@NonNull ItemsAchievementsBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;

        }
    }
}
