package com.parental.control.teen.kid.math.education.utility;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.application.AppConfig;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class CommLockInfoManager /*implements ServiceCallBack*/ {

    private PackageManager mPackageManager;
    private Context mContext;


    public CommLockInfoManager(Context mContext) {
        this.mContext = mContext;
        mPackageManager = mContext.getPackageManager();
    }

    public synchronized List<AppLockNewModel.Data> getAllCommLockInfos() {
        try {
            List<AppLockNewModel.Data> list = new ArrayList<>();
            list.addAll((Collection<? extends AppLockNewModel.Data>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
            }.getType()));
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized void deleteCommLockInfoTable(@NonNull List<AppLockNewModel.Data> appLockInfos) {

        List<AppLockNewModel.Data> baselist = new ArrayList<>();
        baselist.addAll((Collection<? extends AppLockNewModel.Data>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
        }.getType()));

        List<AppLockNewModel.Data> templist = new ArrayList<>();

        boolean match = false;
        for (int i = 0; i < appLockInfos.size(); i++) {
            for (int j = 0; j < baselist.size(); j++) {
                if (appLockInfos.get(i).getApp_id().equalsIgnoreCase(baselist.get(j).getApp_id())) {
                    match = true;
                    break;
                }
            }
            if (!match) {
                match = false;
                templist.add(appLockInfos.get(i));
            }
        }

        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(templist));

    }

    public synchronized void instanceCommLockInfoTable(@NonNull List<ResolveInfo> resolveInfos) throws PackageManager.NameNotFoundException {
        List<AppLockNewModel.Data> list = new ArrayList<>();

        List<String> preLockAppInfos = new ArrayList<>();
        try {
            preLockAppInfos.addAll((Collection<? extends String>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PREAPPLIST), new TypeToken<List<String>>() {
            }.getType()));
        } catch (Exception e) {
            e.printStackTrace();
        }


        for (ResolveInfo resolveInfo : resolveInfos) {
//            AppLockNewModel.Data appLockInfo = new AppLockNewModel.Data(resolveInfo.activityInfo.packageName, false);
            AppLockNewModel.Data appLockInfo = new AppLockNewModel.Data();

            ApplicationInfo appInfo = mPackageManager.getApplicationInfo(resolveInfo.activityInfo.packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            String appName = mPackageManager.getApplicationLabel(appInfo).toString();

            AppLockNewModel.Status status = new AppLockNewModel.Status();
            if (resolveInfo.activityInfo.packageName.equalsIgnoreCase(AppConfig.getContext().getPackageName())
                    || resolveInfo.activityInfo.packageName.equalsIgnoreCase("com.android.contacts")
                    || resolveInfo.activityInfo.packageName.equalsIgnoreCase("com.google.android.contacts")
                    || resolveInfo.activityInfo.packageName.equalsIgnoreCase("com.google.android.dialer")
                    || resolveInfo.activityInfo.packageName.equalsIgnoreCase("com.android.dialer")
                    || resolveInfo.activityInfo.packageName.equalsIgnoreCase("com.android.settings")
                    || resolveInfo.activityInfo.packageName.equalsIgnoreCase("com.google.android.apps.messaging")
                    || resolveInfo.activityInfo.packageName.contains("com.samsung.android.messaging")
                    || resolveInfo.activityInfo.packageName.contains("com.parental.control")) {

            } else if (appName.toLowerCase().contains("phone") || appName.toLowerCase().contains("contacts")
                    || appName.toLowerCase().contains("1question")
                    || appName.toLowerCase().contains("com.samsung.android.messaging")) {
            } else/*
            if (!(resolveInfo.activityInfo.packageName.equalsIgnoreCase(AppConfig.getContext().getPackageName()))
            )*/ {
                if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                    status.setIsSysApp(true);
                } else {
                    status.setIsSysApp(false);
                }
                if (preLockAppInfos.contains(resolveInfo.activityInfo.packageName)) {
                    status.setIsLocked(true);
                } else {
                    status.setIsLocked(false);
                }
//                status.setIsRequested(false);
                appLockInfo.setApp_id(resolveInfo.activityInfo.packageName);
                appLockInfo.setStatus(status);
                appLockInfo.setImage_url("");
                appLockInfo.setName(appName);
                appLockInfo.setDevice_type("android");

               /* try {
                    Drawable icon = mContext.getPackageManager().getApplicationIcon(resolveInfo.activityInfo.packageName);
                    Bitmap bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    icon.draw(canvas);
                    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteStream);
                    byte[] byteArray = byteStream.toByteArray();
                    String baseString = Base64.encodeToString(byteArray,Base64.DEFAULT);
                    appLockInfo.setImage_url(baseString);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                list.add(appLockInfo);
            }
        }
        list = clearRepeatCommLockInfo(list);

        Collections.sort(list, comparator);

//        Log.e("TAG", "applist: " + new Gson().toJson(list));
        if(StringUtils.isNotEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST))){
            List<AppLockNewModel.Data> templist = new ArrayList<>();
            templist.addAll((Collection<? extends AppLockNewModel.Data>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
            }.getType()));
            for (int i = 0; i <list.size() ; i++) {
                for (int j = 0; j <templist.size() ; j++) {
                    if(list.get(i).getApp_id().equalsIgnoreCase(templist.get(j).getApp_id())){
                        list.get(i).getStatus().setIsLocked(templist.get(j).getStatus().getIsLocked());
                        break;
                    }
                }
            }
        }
        List<AppLockNewModel.Data> finalList = list;
        RxApiRequestHandler.updateAppList(mContext, new ServiceCallBack() {
                    @Override
                    public void onSuccess(String requestTag, JsonElement data) {
                        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(finalList));
                    }

                    @Override
                    public void onFailure(String requestTag, String message, String errorcode) {

                    }
                }
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID)
                , list);

    }

    public static Comparator<AppLockNewModel.Data> comparator = new Comparator<AppLockNewModel.Data>() {

        public int compare(AppLockNewModel.Data s1, AppLockNewModel.Data s2) {
            String StudentName1 = s1.getName().replaceAll(" ", "").replaceAll("-", "").replaceAll(",", "").replaceAll("_", "").toLowerCase();
            String StudentName2 = s2.getName().replaceAll(" ", "").replaceAll("-", "").replaceAll(",", "").replaceAll("_", "").toLowerCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };

   /* @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.UPLOAD_APP)) {
            Log.e("TAG", "onSuccess: applist");
        }else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_LOCKED_APP)) {
            try {
                AppLockNewModel appLockNewModel = new Gson().fromJson(data, AppLockNewModel.class);
                if (appLockNewModel != null && appLockNewModel.getData() != null && appLockNewModel.getData().size() > 0) {
                    SharedPreferenceUtils.preferencePutString(AppConstants.StoredJsonDataKeys.APPLIST, new Gson().toJson(appLockNewModel.getData()));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        Log.e("TAG", "onError: applist");

    }*/

    public static List<AppLockNewModel.Data> clearRepeatCommLockInfo(List<AppLockNewModel.Data> lockInfos) {
        HashMap<String, AppLockNewModel.Data> hashMap = new HashMap<>();
        for (AppLockNewModel.Data lockInfo : lockInfos) {
            if (!hashMap.containsKey(lockInfo.getApp_id())) {
                hashMap.put(lockInfo.getApp_id(), lockInfo);
            }
        }
        List<AppLockNewModel.Data> appLockInfos = new ArrayList<>();
        for (HashMap.Entry<String, AppLockNewModel.Data> entry : hashMap.entrySet()) {
            appLockInfos.add(entry.getValue());
        }
        return appLockInfos;
    }


    public void lockApplication(String packageName) {
        updateLockStatus(packageName, true);
    }

    public void unlockApplication(String packageName) {
        updateLockStatus(packageName, false);
    }

    private void updateLockStatus(String packageName, boolean isLock) {
        List<AppLockNewModel.Data> baselist = new ArrayList<>();
        baselist.addAll((Collection<? extends AppLockNewModel.Data>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
        }.getType()));

        for (int i = 0; i < baselist.size(); i++) {
            if (baselist.get(i).getApp_id().equalsIgnoreCase(packageName)) {
                baselist.get(i).getStatus().setIsLocked(isLock);
                ;
                break;
            }
        }
        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.APPLIST, new Gson().toJson(baselist));
    }


    public boolean isLockedPackageName(String packageName) {
        List<AppLockNewModel.Data> baselist = new ArrayList<>();
        try {
            baselist.addAll((Collection<? extends AppLockNewModel.Data>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
            }.getType()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isLocked = false;
        for (int i = 0; i < baselist.size(); i++) {
            if (baselist.get(i).getApp_id().equalsIgnoreCase(packageName) && baselist.get(i).getStatus().getIsLocked()) {
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.OVERLAY_INFO, new Gson().toJson(baselist.get(i)));
                isLocked = true;
                break;
            }
        }
        return isLocked;
    }


}
