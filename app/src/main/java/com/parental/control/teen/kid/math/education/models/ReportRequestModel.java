package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class ReportRequestModel {


    @SerializedName("answer")
    private Answer answer;

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public static class Answer {
        @SerializedName("time_duration")
        private String time_duration;
        @SerializedName("id")
        private String id;
        @SerializedName("index")
        private String index;

        public String getTime_duration() {
            return time_duration;
        }

        public void setTime_duration(String time_duration) {
            this.time_duration = time_duration;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }
    }
}
