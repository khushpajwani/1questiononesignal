package com.parental.control.teen.kid.math.education.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import androidx.annotation.Nullable;

import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.CommLockInfoManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parental.control.teen.kid.math.education.utility.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LoadAppListService extends IntentService {

    private PackageManager mPackageManager;
    @Nullable
    private CommLockInfoManager mLockInfoManager;

    public LoadAppListService() {
        super("LoadAppListService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPackageManager = getPackageManager();
        mLockInfoManager = new CommLockInfoManager(this);
    }

    @Override
    protected void onHandleIntent(Intent handleIntent) {

        initFavoriteApps();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = mPackageManager.queryIntentActivities(intent, 0);
        try {
            mLockInfoManager.instanceCommLockInfoTable(resolveInfos);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLockInfoManager = null;
    }


    public void initFavoriteApps() {
        List<String> preLockAppInfos = new ArrayList<>();

        if(StringUtils.isNoneEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST))){
            List<AppLockNewModel.Data> baselist = new ArrayList<>();
            try {
                baselist.addAll((Collection<? extends AppLockNewModel.Data>) new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.APPLIST), new TypeToken<List<AppLockNewModel.Data>>() {
                }.getType()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(baselist.size()>0){
                for (int i = 0; i <baselist.size() ; i++) {
                    if(baselist.get(i).getStatus().getIsLocked()) {
                        preLockAppInfos.add(baselist.get(i).getApp_id());
                    }
                }
            }
        }


        /*preLockAppInfos.add("com.android.contacts");
        preLockAppInfos.add("com.android.mms");
        preLockAppInfos.add("com.android.email");
        preLockAppInfos.add("com.android.dialer");
        preLockAppInfos.add("com.sec.android.app.myfiles");
        preLockAppInfos.add("com.android.settings");
        preLockAppInfos.add("com.google.android.apps.maps");
        preLockAppInfos.add("com.spotify.music");
        preLockAppInfos.addAll(preLockAppInfos);*/


        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.PREAPPLIST, new Gson().toJson(preLockAppInfos));

    }
}
