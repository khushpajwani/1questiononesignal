package com.parental.control.teen.kid.math.education.webservice;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.OPTIONS;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface RXInterface {

    @POST("{url}")
    Observable<JsonElement> makePostRequest(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url, @Body JsonObject data);

    @POST("{url}")
    Observable<JsonElement> makePostRequest_Query(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url, @QueryMap Map<String, String> options, @Body JsonObject data);


    @Multipart
    @POST("{url}")
    Observable<JsonElement> makePostRequest_media(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url, @PartMap Map<String, RequestBody> map, @Part MultipartBody.Part filePart);

    @Multipart
    @PUT("{url}")
    Observable<JsonElement> makePutRequest_media(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url, @Part MultipartBody.Part filePart);

    @PUT("{url}")
    Observable<JsonElement> makePutRequest(@HeaderMap Map<String, String> headers, @Path("url") String url, @Body JsonObject data);

    @PATCH("{url}")
    Observable<JsonElement> makePatchRequest(@HeaderMap Map<String, String> headers, @Path("url") String url, @Body JsonObject data);

    @OPTIONS("{url}")
    Observable<JsonElement> makeOptionsRequest(@HeaderMap Map<String, String> headers, @Path("url") String url, @Body JsonObject data);

    @GET("{url}")
    Observable<JsonElement> makeGetRequest(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url);

    @GET("{url}")
    Observable<JsonElement> makeGetRequest_Query(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url, @QueryMap Map<String, String> options);

    @DELETE("{url}")
    Observable<JsonElement> makeDeleteRequest(@HeaderMap Map<String, String> headers, @Path(value = "url", encoded = true) String url);



}
