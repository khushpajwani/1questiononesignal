package com.parental.control.teen.kid.math.education.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.activities.LockUnlockActivity;
import com.parental.control.teen.kid.math.education.databinding.ItemsApplistBinding;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.qtalk.recyclerviewfastscroller.RecyclerViewFastScroller;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.RewardViewHolder> implements RecyclerViewFastScroller.OnPopupTextUpdate {
    private Context context;
    private ArrayList<AppLockNewModel.Data> appDataArrayList;
    String filtername;

    public AppListAdapter(Context context, ArrayList<AppLockNewModel.Data> appDataArrayList) {
        this.context = context;
        this.appDataArrayList = appDataArrayList;
    }

    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new RewardViewHolder(ItemsApplistBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RewardViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if(StringUtils.isNoneEmpty(filtername)) {
            if (appDataArrayList.get(position).getName().toLowerCase().trim().contains(filtername)) {
                holder.binding.layoutChildItem.setVisibility(View.VISIBLE);
            } else {
                holder.binding.layoutChildItem.setVisibility(View.GONE);
            }
        }else {
            holder.binding.layoutChildItem.setVisibility(View.VISIBLE);
        }

        if(holder.binding.layoutChildItem.getVisibility()==View.VISIBLE) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.binding.layoutChildItem.getLayoutParams();
            if (Utils.isTablet(context)) {
                if (position == 0) {
                    layoutParams.setMargins((int) context.getResources().getDimension(R.dimen._10sdp), (int) context.getResources().getDimension(R.dimen._10sdp), (int) context.getResources().getDimension(R.dimen._15sdp), (int) context.getResources().getDimension(R.dimen._2sdp));
                } else if (position + 1 == appDataArrayList.size()) {
                    layoutParams.setMargins((int) context.getResources().getDimension(R.dimen._10sdp), (int) context.getResources().getDimension(R.dimen._2sdp), (int) context.getResources().getDimension(R.dimen._15sdp), (int) context.getResources().getDimension(R.dimen._10sdp));
                } else {
                    layoutParams.setMargins((int) context.getResources().getDimension(R.dimen._10sdp), (int) context.getResources().getDimension(R.dimen._2sdp), (int) context.getResources().getDimension(R.dimen._15sdp), (int) context.getResources().getDimension(R.dimen._2sdp));
                }
            } else {
                if (position == 0) {
                    layoutParams.setMargins((int) context.getResources().getDimension(R.dimen._20sdp), (int) context.getResources().getDimension(R.dimen._7sdp), (int) context.getResources().getDimension(R.dimen._20sdp), (int) context.getResources().getDimension(R.dimen._5sdp));
                } else if (position + 1 == appDataArrayList.size()) {
                    layoutParams.setMargins((int) context.getResources().getDimension(R.dimen._20sdp), (int) context.getResources().getDimension(R.dimen._7sdp), (int) context.getResources().getDimension(R.dimen._20sdp), (int) context.getResources().getDimension(R.dimen._10sdp));
                } else {
                    layoutParams.setMargins((int) context.getResources().getDimension(R.dimen._20sdp), (int) context.getResources().getDimension(R.dimen._7sdp), (int) context.getResources().getDimension(R.dimen._20sdp), (int) context.getResources().getDimension(R.dimen._7sdp));
                }
            }
            holder.binding.layoutChildItem.requestLayout();

            holder.binding.txtAppName.setText(appDataArrayList.get(position).getName());

            /*if (StringUtils.isNoneEmpty(appDataArrayList.get(position).getImage_url())) {
                byte[] decodedString = Base64.decode(appDataArrayList.get(position).getImage_url(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                Glide.with(context).load(decodedByte).placeholder(R.drawable.andro).into(holder.binding.imgAppIcon);
            }*/

            try {
                Drawable icon = context.getPackageManager().getApplicationIcon(appDataArrayList.get(position).getApp_id());
                Glide.with(context).load(icon).placeholder(R.drawable.andro).into(holder.binding.imgAppIcon);
            } catch (Exception e) {
                Glide.with(context).load(R.drawable.andro).into(holder.binding.imgAppIcon);
            }


            setSelectColor(appDataArrayList.get(position).getStatus().getIsLocked(), holder.binding.txtAppName, holder.binding.imgLock);

            holder.binding.layoutChildItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (appDataArrayList.get(position).getStatus().getIsLocked()) {
                        appDataArrayList.get(position).getStatus().setIsLocked(false);
                    } else {
                        appDataArrayList.get(position).getStatus().setIsLocked(true);
                        LinkedHashMap<String, String> paramHashMap = new LinkedHashMap<>();
                        paramHashMap.put("app_name", appDataArrayList.get(position).getName());
                        Utils.logEvent("apps_lock", paramHashMap);
                    }

                    notifyItemChanged(position);
                    ((LockUnlockActivity) context).updatedSelection(false);


                }
            });
        }
    }

    public void setfilterText(String filtername){
        this.filtername=filtername.toLowerCase().trim();

    }

    private void setSelectColor(boolean isLocked, TextView txtRowTitle, ImageView imgLock) {
        if (isLocked) {
            imgLock.setImageResource(R.drawable.switch_on);
        } else {
            imgLock.setImageResource(R.drawable.switch_off);
        }
    }


    @Override
    public int getItemCount() {
        return appDataArrayList.size();
    }

    @NotNull
    @Override
    public CharSequence onChange(int i) {
        try {
            return appDataArrayList.get(i).getName().substring(0, 1).toUpperCase();
        } catch (Exception e) {
            return "";
        }

    }


    class RewardViewHolder extends RecyclerView.ViewHolder {

        ItemsApplistBinding binding;

        RewardViewHolder(@NonNull ItemsApplistBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;


        }
    }
}
