package com.parental.control.teen.kid.math.education.webservice;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import com.parental.control.teen.kid.math.education.BuildConfig;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.activities.GetStartedActivity;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.models.ErrorModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import retrofit2.http.Part;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class RXRequestHandler {

    private static final String TAG = "RxRequestHandler";
    private static Dialog dialogView;
    public static Disposable disposable = null;


    public void makeApiRequest(final Context context, String url, final String requestTag,
                               final ServiceCallBack listener, JsonObject requestObject, Map<String, String> query,
                               boolean isProgressNeeded, String apiType, @Part MultipartBody.Part filePart,
                               Map<String, RequestBody> requestObjectPart) {

        if (Utils.isConnectingToInternet(context)) {
//            if(disposable!=null  && requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_OVERLAY_QUESTIONS)){
//                disposable.dispose();
//                disposable=null;
//            }

            RXInterface apiService = RxApiClient.getClient().create(RXInterface.class);
            Observable<JsonElement> observableResponse = null;

            if (isProgressNeeded) {
                Utils.showProgressDialog(context);
            }

            String authorization = "";
            if (StringUtils.isNotEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AUTH_TOKEN))) {
                authorization = "Bearer " + SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AUTH_TOKEN);
            }

            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Authorization",authorization);
            headers.put("Version", ""+ BuildConfig.VERSION_CODE+"("+BuildConfig.VERSION_NAME+")");

            if (apiType.equalsIgnoreCase(AppConstants.ApiType.POST)) {
                observableResponse = apiService.makePostRequest(headers, url, requestObject)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.POST_QUERY)) {

                observableResponse = apiService.makePostRequest_Query(headers, url, query, requestObject)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.POST_MEDIA)) {
                observableResponse = apiService.makePostRequest_media(headers, url, requestObjectPart, filePart)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.PUT_MEDIA)) {
                observableResponse = apiService.makePutRequest_media(headers, url, filePart)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.PATCH)) {

                observableResponse = apiService.makePatchRequest(headers, url, requestObject)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.PUT)) {

                observableResponse = apiService.makePutRequest(headers, url, requestObject)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.GET)) {

                observableResponse = apiService.makeGetRequest(headers, url)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.GET_QUERY)) {

                observableResponse = apiService.makeGetRequest_Query(headers, url, query)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.OPTIONS)) {
                observableResponse = apiService.makeOptionsRequest(headers, url, requestObject)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if (apiType.equalsIgnoreCase(AppConstants.ApiType.DELETE)) {
                observableResponse = apiService.makeDeleteRequest(headers, url)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
            }

            if (observableResponse != null) {
                observableResponse.subscribe(new Observer<JsonElement>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(@NonNull JsonElement jsonElement) {
                        Utils.hideProgressDialog(context);
                        if (listener != null) {
                            listener.onSuccess(requestTag, jsonElement);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Utils.hideProgressDialog(context);
                        if (listener != null) {
                            readerror(context, e, context.getResources().getString(R.string.serverError), listener, requestTag);
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });


            }
        }
    }


    private void readerror(Context context, Throwable e, String errorstring, ServiceCallBack listener, String requestTag) {
        String errorcode = "";


        try {
            if (e instanceof HttpException) {
                HttpException error = (HttpException) e;

                //General error
                String errorBody = error.response().errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(errorBody, ErrorModel.class);
                if (errorModel != null && StringUtils.isNoneEmpty(errorModel.getMessage())) {
                    errorstring = errorModel.getMessage();
                    errorcode = errorModel.getStatusCode();
                    if(StringUtils.isNotEmpty(errorcode) && errorcode.equalsIgnoreCase("403")){
                        Utils.setToast(context,context.getResources().getString(R.string.unauthorized));
                        String token = SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.DEVICE_ID);
                        SharedPreferenceUtils.clearPreference();
                        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.DEVICE_ID, token);
                        context.startActivity(new Intent(context, GetStartedActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        return;
                    }
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if (listener != null) {
            listener.onFailure(requestTag, errorstring, errorcode);
        }
    }


}
