package com.parental.control.teen.kid.math.education.viewmodels;

import android.view.View;

import androidx.lifecycle.ViewModel;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.AppConfig;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LoginViewModel  extends ViewModel {
    public String pincode=null;
    public LoginInterface loginInterface=null;

    public String getPincode() {
        return this.pincode;
    }

    public void setPincode(final String pincode) {
        this.pincode = pincode;
    }

    public void loginbuttonclick(View view) {
        if (StringUtils.isNotEmpty(pincode) && pincode.length() >= 4) {
            loginInterface.validateall(true, pincode, "");

        }else {
            loginInterface.validateall(false, pincode, AppConfig.getAppInstance().getResources().getString(R.string.error_code2));
        }
    }
}
