package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class AppLockNewModel {


    @SerializedName("data")
    private List<Data> data;
    @SerializedName("message")
    private String message;
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @SerializedName("status")
        private Status status;
        @SerializedName("device_type")
        private String device_type;
        @SerializedName("name")
        private String name;
        @SerializedName("app_id")
        private String app_id;
        @SerializedName("image_url")
        private String image_url;


        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }
    }

    public static class Status {
//        @SerializedName("isRequested")
//        private String isRequested;
        @SerializedName("isSysApp")
        private String isSysApp;
        @SerializedName("isLocked")
        private String isLocked;

       /* public boolean getIsRequested() {
            return Boolean.parseBoolean(isRequested);
        }

        public void setIsRequested(boolean isRequested) {
            this.isRequested = String.valueOf(isRequested);
        }*/

        public boolean getIsSysApp() {
            return Boolean.parseBoolean(isSysApp);
        }

        public void setIsSysApp(boolean isSysApp) {
            this.isSysApp = String.valueOf(isSysApp);
        }

        public boolean getIsLocked() {
            return Boolean.parseBoolean(isLocked);
        }

        public void setIsLocked(boolean isLocked) {
            this.isLocked = String.valueOf(isLocked);
        }
    }
}
