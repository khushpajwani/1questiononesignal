package com.parental.control.teen.kid.math.education.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.utility.AppConstants;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class AppPauseReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

       /* if (intent.getIntExtra("type", 0) == AppConstants.ServiceTypes.LOCK_REQ) {
            Utils.startService(context, LockService.class, AppPauseReceiver.class, AppConstants.ServiceTypes.LOCK_REQ);
        } else if (intent.getIntExtra("type", 0) == AppConstants.ServiceTypes.PAUSE_REQ) {*/
            SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, false);
            SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
            context.sendBroadcast(new Intent("UNPAUSE_DONE"));
            context.sendBroadcast(new Intent("UNPAUSE_DONE_TOBASE"));


        /* }*/


    }

}