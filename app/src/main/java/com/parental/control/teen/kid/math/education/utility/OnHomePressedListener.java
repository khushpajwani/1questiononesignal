package com.parental.control.teen.kid.math.education.utility;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface OnHomePressedListener {
    void onHomePressed();
    void onHomeLongPressed();
}
