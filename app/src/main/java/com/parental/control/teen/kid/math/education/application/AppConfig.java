package com.parental.control.teen.kid.math.education.application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.TaskStackBuilder;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.onesignal.OSMutableNotification;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenedResult;
import com.onesignal.OSNotificationReceivedEvent;
import com.onesignal.OneSignal;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.activities.DashboardActivity;
import com.parental.control.teen.kid.math.education.activities.LoginParent;
import com.parental.control.teen.kid.math.education.activities.SettingsActivity;
import com.parental.control.teen.kid.math.education.activities.SplashActivity;
import com.parental.control.teen.kid.math.education.receivers.AppBroadcastReceiver;
import com.parental.control.teen.kid.math.education.services.LockService;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.HomeWatcher;
import com.parental.control.teen.kid.math.education.utility.OnHomePressedListener;
import com.parental.control.teen.kid.math.education.utility.Utils;

import org.json.JSONObject;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

@SuppressLint("CommitPrefEdits")
public class AppConfig extends MultiDexApplication implements AppLifeCycleHandler.AppLifeCycleCallback, OneSignal.OSNotificationOpenedHandler {
    //////////////////////////////////One Signal///////////////////////////////////////////////
    private static final String ONESIGNAL_APP_ID = "38fe0a28-0fdc-48d4-a387-03b7f91dffbb";
    /////////////////////////////////////////////////////////////////////////////////
    private static AppConfig appInstance;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor sharedPreferencesEditor;
    private static Context mContext;
    public static Activity currentActivity;

    public static FirebaseAnalytics mFirebaseAnalytics;


    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context mctx) {
        mContext = mctx;
    }

    public static AppConfig getAppInstance() {
        if (appInstance == null)
            throw new IllegalStateException("The application is not created yet!");
        return appInstance;
    }

    public static SharedPreferences.Editor getApplicationPreferenceEditor() {
        return sharedPreferencesEditor;
    }

    public static SharedPreferences getApplicationPreference() {
        return sharedPreferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;

        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
        OneSignal.setNotificationOpenedHandler(new AppConfig());

        Log.d("getappid", "ONESIGNAL_APP_ID");


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

//        if (isTablet(this)) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }


//        FirebaseCrashlytics.getInstance().setCustomKey("current_server", "production");
        FirebaseCrashlytics.getInstance().setCustomKey("current_server", "development");
//        FirebaseCrashlytics.getInstance().setCustomKey("current_server", "staging1");


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferencesEditor = sharedPreferences.edit();
        setContext(getApplicationContext());


        AppLifeCycleHandler appLifeCycleHandler = new AppLifeCycleHandler(this);
        registerActivityLifecycleCallbacks(appLifeCycleHandler);
        registerComponentCallbacks(appLifeCycleHandler);

        try {
            HomeWatcher mHomeWatcher = new HomeWatcher(AppConfig.this);
            mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
                @Override
                public void onHomePressed() {
                    try {

                        if (!currentActivity.getClass().getSimpleName().equalsIgnoreCase("OverlayActivity")) {
                            Intent homeIntent = new Intent(AppConfig.this, SplashActivity.class);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            homeIntent.putExtra("killapp", true);
                            AppConfig.this.startActivity(homeIntent);

                            Utils.startService(AppConfig.this, LockService.class, AppBroadcastReceiver.class, AppConstants.ServiceTypes.LOCK_REQ);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onHomeLongPressed() {
                }
            });
            mHomeWatcher.startWatch();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onAppBackground() {
    }

    @Override
    public void onAppForeground() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        currentActivity = activity;
        if (!activity.getClass().getSimpleName().equalsIgnoreCase("OverlayActivity")) {

            setOrientation2(activity);
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }

    public static void setOrientation(Context context) {

    }

    public static void setOrientation2(Context context) {

        if (isTablet(context)) {
//            ((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }


    @Override
    public void notificationOpened(OSNotificationOpenedResult result) {
        Log.i("OSNotification", "result.notification.toJSONObject(): " + result.getNotification().toJSONObject());
        Log.i("OSNotification", "result.notification.toJSONObject(): " + result.getNotification().getAdditionalData());


        JSONObject data = result.getNotification().getAdditionalData();
        if (data != null) {
            String customKey = data.optString("customkey", null);
            if (customKey != null) {
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
            }
        }
        OSNotificationAction.ActionType actionType = result.getAction().getType();
        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
            Log.i("OneSignalExample", "result.notification.toJSONObject(): " + result.getNotification().getAdditionalData());

            Log.i("OneSignalExample", "Button pressed with id: " + result.getAction().getActionId());
            Log.i("OneSignalExample", "Button pressed with id: " + currentActivity.getClass().getSimpleName().equalsIgnoreCase("OverlayActivity"));

            if (result.getAction().getActionId().equalsIgnoreCase("ok_id")) {
                Toast.makeText(mContext, "Okay button clicked", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(AppConfig.getContext(), LoginParent.class);
                intent.putExtra(AppConstants.IntentKeys.FOR_SETTINGS, true);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                AppConfig.getContext().startActivity(intent);
            } else {
                Intent intent = new Intent(AppConfig.getContext(), SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                AppConfig.getContext().startActivity(intent);
                Toast.makeText(mContext, "Cancel button clicked", Toast.LENGTH_SHORT).show();
            }
        } else if (actionType == OSNotificationAction.ActionType.Opened) {

            Intent intent = new Intent(AppConfig.getContext(), DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            AppConfig.getContext().startActivity(intent);
        }
    }

}