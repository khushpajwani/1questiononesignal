package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class NotificationModel {

    @SerializedName("user_type")
    private String user_type;
    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("notification_type")
    private String notification_type;

    public NotificationModel(String user_type, String title, String body, String notification_type) {
        this.user_type = user_type;
        this.title = title;
        this.body = body;
        this.notification_type = notification_type;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }
}
