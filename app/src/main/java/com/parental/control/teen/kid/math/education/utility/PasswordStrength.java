package com.parental.control.teen.kid.math.education.utility;

import android.graphics.Color;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.AppConfig;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public enum PasswordStrength {

    // we use some color in green tint =>
    //more secure is the password, more darker is the color associated
    WEAK(AppConfig.getContext().getString(R.string.weakpassword), Color.parseColor("#61ad85"),0),
    MEDIUM(AppConfig.getContext().getString(R.string.mediumpassword), Color.parseColor("#4d8a6a"),1),
    STRONG(AppConfig.getContext().getString(R.string.strongpassword), Color.parseColor("#3a674f"),2),
    VERY_STRONG(AppConfig.getContext().getString(R.string.verystrongpassword), Color.parseColor("#264535"),3);

    public String msg;
    public int color;
    public int strengthid;

    private static int MIN_LENGTH = 8;
    private static int MAX_LENGTH = 15;

    PasswordStrength(String msg, int color, int strengthid) {
        this.msg = msg;
        this.color = color;
        this.strengthid = strengthid;

    }

    public static PasswordStrength calculate(String password) {
        int score = 0;
        // boolean indicating if password has an upper case
        boolean upper = false;
        // boolean indicating if password has a lower case
        boolean lower = false;
        // boolean indicating if password has at least one digit
        boolean digit = false;
        // boolean indicating if password has a leat one special char
        boolean specialChar = false;

        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);

            if (!specialChar  &&  !Character.isLetterOrDigit(c)) {
                score++;
                specialChar = true;
            } else {
                if (!digit  &&  Character.isDigit(c)) {
                    score++;
                    digit = true;
                } else {
                    if (!upper || !lower) {
                        if (Character.isUpperCase(c)) {
                            upper = true;
                        } else {
                            lower = true;
                        }

                        if (upper && lower) {
                            score++;
                        }
                    }
                }
            }
        }

        int length = password.length();

        if (length > MAX_LENGTH) {
            score++;
        } else if (length < MIN_LENGTH) {
            score = 0;
        }

        // return enum following the score
        switch(score) {
            case 0 : return WEAK;
            case 1 : return MEDIUM;
            case 2 : return STRONG;
            case 3 : return VERY_STRONG;
            default:
        }

        return VERY_STRONG;
    }
}