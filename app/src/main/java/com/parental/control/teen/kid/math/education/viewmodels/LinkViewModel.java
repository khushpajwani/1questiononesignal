package com.parental.control.teen.kid.math.education.viewmodels;

import android.view.View;

import androidx.lifecycle.ViewModel;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.AppConfig;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LinkViewModel extends ViewModel {

    public String pincode=null;
    public LinkInferface linkInferface=null;

    public String getPincode() {
        return this.pincode;
    }

    public void setPincode(final String pincode) {
        this.pincode = pincode;
    }

    public void loginButtonClick(View view){
        if(StringUtils.isNoneEmpty(pincode) && pincode.length() >= 6){
            linkInferface.validatedAll(true,pincode,"");
        }else {
            linkInferface.validatedAll(false,pincode, AppConfig.getAppInstance().getResources().getString(R.string.error_code2));
        }
    }
}
