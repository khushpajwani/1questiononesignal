package com.parental.control.teen.kid.math.education.application;


/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */


public class SharedPreferenceUtils {

    public static void preferencePutInteger(String key, int value) {
        AppConfig.getApplicationPreferenceEditor().putInt(key, value);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static int preferenceGetInteger(String key, int defaultValue) {
        return AppConfig.getApplicationPreference().getInt(key, defaultValue);
    }

    public static void preferencePutBoolean(String key, boolean value) {
        AppConfig.getApplicationPreferenceEditor().putBoolean(key, value);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static boolean preferenceGetBoolean(String key, boolean defaultValue) {
        return AppConfig.getApplicationPreference().getBoolean(key, defaultValue);
    }

    public static void preferencePutString(String key, String value) {
        AppConfig.getApplicationPreferenceEditor().putString(key, value);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static void removeKey(String key) {
        AppConfig.getApplicationPreferenceEditor().remove(key);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static String preferenceGetString(String key) {
        return AppConfig.getApplicationPreference().getString(key, "");
    }

    public static String preferenceGetString(String key, String value) {
        return AppConfig.getApplicationPreference().getString(key, value);
    }

    public static void preferencePutLong(String key, long value) {
        AppConfig.getApplicationPreferenceEditor().putLong(key, value);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static long preferenceGetLong(String key, long defaultValue) {
        return AppConfig.getApplicationPreference().getLong(key, defaultValue);
    }

    public static void preferencePutFloat(String key, float value) {
        AppConfig.getApplicationPreferenceEditor().putFloat(key, value);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static float preferenceGetFloat(String key, float defaultValue) {
        return AppConfig.getApplicationPreference().getFloat(key, defaultValue);
    }



    public static void preferenceRemoveKey(String key) {
        AppConfig.getApplicationPreferenceEditor().remove(key);
        AppConfig.getApplicationPreferenceEditor().commit();
    }

    public static void clearPreference() {
        AppConfig.getApplicationPreferenceEditor().clear();
        AppConfig.getApplicationPreferenceEditor().commit();
    }

}

