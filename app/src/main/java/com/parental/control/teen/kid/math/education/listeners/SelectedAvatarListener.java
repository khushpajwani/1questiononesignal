package com.parental.control.teen.kid.math.education.listeners;


import com.parental.control.teen.kid.math.education.models.AvatarModel;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface SelectedAvatarListener {
    public void madeselection(int popupType , AvatarModel.Data avatarModel);
    public void cancelselection(int popupType);

}
