package com.parental.control.teen.kid.math.education.listeners;


import com.parental.control.teen.kid.math.education.models.PopupModel;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface UnlimitedListener {
    public void selectedValue(int popupType, String value);

}
