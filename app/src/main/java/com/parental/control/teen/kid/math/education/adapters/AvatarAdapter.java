package com.parental.control.teen.kid.math.education.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ItemsAvatarBinding;
import com.parental.control.teen.kid.math.education.listeners.SelectedAvatarListener;
import com.parental.control.teen.kid.math.education.models.AvatarModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class AvatarAdapter extends RecyclerView.Adapter<AvatarAdapter.RewardViewHolder> {
    private Context context;
    private ArrayList<AvatarModel.Data> avatarModelArrayList;
    private int selectedposition = -1;
    SelectedAvatarListener selectedAvatarListener;

    public AvatarAdapter(Context context, ArrayList<AvatarModel.Data> avatarModels, SelectedAvatarListener selectedAvatarListener) {
        this.context = context;
        this.avatarModelArrayList = avatarModels;
        this.selectedAvatarListener = selectedAvatarListener;

    }

    @Override
    public RewardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new RewardViewHolder(ItemsAvatarBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RewardViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(context).load(avatarModelArrayList.get(position).getName()).placeholder(R.drawable.placeholder_avatar).into(holder.binding.imgAvatar);

        if (position == selectedposition) {
            holder.binding.imgSelected.setVisibility(View.VISIBLE);
        } else {
            holder.binding.imgSelected.setVisibility(View.INVISIBLE);
        }



        holder.binding.imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedposition = position;
                selectedAvatarListener.madeselection(-1, avatarModelArrayList.get(position));
                notifyDataSetChanged();


            }
        });
    }


    @Override
    public int getItemCount() {
        return avatarModelArrayList.size();
    }

    public void clearselection() {
        selectedposition = -1;
        notifyDataSetChanged();
    }

    public void setSelectedposition(int selectedposition) {
        this.selectedposition = selectedposition;
        notifyDataSetChanged();
    }

    class RewardViewHolder extends RecyclerView.ViewHolder {

        ItemsAvatarBinding binding;

        RewardViewHolder(ItemsAvatarBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

        }
    }
}
