
package com.parental.control.teen.kid.math.education.utility;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class AppConstants {
    public static final String DATE_FORMATE = "yyyy-MM-dd";


    public static int PASSWORD_LENGTH = 8;

    public class ApiURL {
//        public static final String API_URL = "https://web.1question.app/api/";//Live
               public static final String API_URL = "https://dev.web.1question.app/api/";
//        public static final String API_URL = "https://staging1.web.1question.app/api/";//stagging

    }

    //       public static final String QUESTIONS_URL = "http://questionapp.project-demo.info:8081/api/questions.json";


    public class ApiType {

        public static final String POST = "POST";
        public static final String POST_QUERY = "POST_QUERY";
        public static final String GET = "GET";
        public static final String GET_QUERY = "GET_QUERY";
        public static final String PUT = "PUT";
        public static final String PATCH = "PATCH";
        public static final String OPTIONS = "OPTIONS";
        public static final String DELETE = "DELETE";
        public static final String POST_WITHOUT_BODY = "POST_BODY";
        public static final String PUT_WITHOUT_BODY = "PUT_BODY";
        public static final String POST_MEDIA = "POST_MEDIA";
        public static final String PUT_MEDIA = "PUT_MEDIA";
    }

    public static class ApiTags {
        public static final String FORGOT = "FORGOT";
        public static final String REGISTER = "REGISTER";
        public static final String LOGIN = "LOGIN";

        public static final String UPDATE_PARENT = "UPDATE_PARENT";
        public static final String METER = "METER";
        public static final String SEND_REPORT = "SEND_REPORT";

        public static final String GET_QUESTIONS = "GET_QUESTIONS";
        public static final String GET_QUESTION_SETTINGS = "GET_QUESTION_SETTINGS";
        public static final String GET_GRADES = "GET_GRADES";
        public static final String GET_SUBJECTS = "GET_SUBJECTS";
        public static final String GET_TOPICS = "GET_TOPICS";
        public static final String SAVE_QUESTION_SETTINGS = "SAVE_QUESTION_SETTINGS";
        public static final String GET_OVERLAY_QUESTIONS = "GET_OVERLAY_QUESTIONS";
        public static final String SAVE_QUESTION_SETTINGS_L = "SAVE_QUESTION_SETTINGS_L";
        public static final String SAVE_QUESTION_SETTINGS_AL = "SAVE_QUESTION_SETTINGS_AL";
        public static final String ADD_CHILD = "ADD_CHILD";
        public static final String GET_CHILDS = "GET_CHILDS";

        public static final String LINK_CHILD = "LINK_CHILD";
        public static final String VERIFY_PASSWORD = "VERIFY_PASSWORD";
        public static final String PARENT_PROFILE = "PARENT_PROFILE";
        public static final String SEND_FCM_TOKEN = "SEND_FCM_TOKEN";

        public static final String LOGIN_UNIQUE_CODE = "LOGIN_UNIQUE_CODE";
        public static final String GET_AVATAR_LIST = "GET_AVATAR_LIST";
        public static final String UPDATE_AVATAR = "UPDATE_AVATAR";
        public static final String GET_APP_LIST = "GET_APP_LIST";
        public static final String UPLOAD_APP_LIST = "UPLOAD_APP_LIST";
        public static final String PARENT_LOGIN = "PARENT_LOGIN";
        public static final String GET_DASHBOARD = "GET_DASHBOARD";
        public static final String GET_HOURS_LIST = "GET_HOURS_LIST";
        public static final String GET_CURRENT_PAUSE = "GET_CURRENT_PAUSE";
        public static final String SET_PAUSE = "SET_PAUSE";
        public static final String CANCEL_PAUSE = "CANCEL_PAUSE";
        public static final String GET_FAQ = "GET_FAQ";
        public static final String GET_CUR_SUB = "GET_CUR_SUB";
        public static final String FORGOT_NEW = "FORGOT_NEW";
        public static final String GET_NOTI_SETTING = "GET_NOTI_SETTING";
        public static final String SAVE_NOTI_SETTING = "SAVE_NOTI_SETTING";
        public static final String LOGOUT = "LOGOUT";


    }

    public static class SharedPreferenceKeys {

        public static final String AUTH_TOKEN = "AUTH_TOKEN";
        public static final String ISLOGGEDIN = "ISLOGGEDIN";
        public static final String P_NAME = "P_NAME";
        public static final String P_ID = "P_ID";
        public static final String C_NAME = "C_NAME";
        public static final String C_ID = "C_ID";
        public static final String EMAIL = "EMAIL";
        public static final String PIN = "PIN";
        public static final String FIRSTTIME_USER = "FIRSTTIME_USER";
        public static final String PARENT_CODE = "PARENT_CODE";
        public static final String APPLIST = "applist";
        public static final String PREAPPLIST = "preapplist";
        public static final String OVERLAY_INFO = "overlay_info";
        public static final String TEMP_UNLOCK = "temp_unlock";
        public static final String IS_PAUSE = "is_pause";
        public static final String UNPAUSE_TIME = "unpause_time";
        public static final String RATING_DATE = "rating_date";
        public static final String IS_GODEVICE = "is_godevice";
        public static final String Q_SETTINGS_DONE = "q_settings_done";
        public static final String NOT_NOW_ADMIN = "not_now_Admin";
        public static final String GO_TO_HOME = "go_to_home";
        public static final String IS_SHOW_WELCOME = "is_show_welcome";
        public static final String DEVICE_ID = "device_id";
        public static final String AVATAR_URL = "AVATAR_URL";
        public static final String UNIQUE_CODE = "UNIQUE_CODE";
        public static final String IS_ACTIVE_SUB = "IS_ACTIVE_SUB";


    }

    public class ValidationTypes {
        public static final String EMAIL = "email";
        public static final String PASSWORD_NORMAL = "password_normal";
        public static final String LOGIN_PIN = "login_pin";
        public static final String CONFIRM_PASSWORD = "confirm_password";
        public static final String NAME = "name";
        public static final String STUDENT_NAME = "student_name";
        public static final String DOB = "dob";

    }


    public static class PopupType {
        public static final int GRADE = 1;
        public static final int SUBJECT = 2;
        public static final int TOPIC = 3;
        public static final int QUE = 4;
        public static final int PAUSE = 5;
        public static final int DO_QUESTION_SETTING = 6;
        public static final int QUESTION_ALREADY_DONE = 7;
        public static final int DO_LOCK = 8;
        public static final int LOCK_ALREADY_SETUP = 9;
        public static final int PIN_CHANGED = 10;
        public static final int ACTIVE_AGAIN = 11;
        public static final int RATING = 12;
        public static final int NOAPPS_SELECTED = 13;
        public static final int BATTERY_SETTINGS = 14;
        public static final int NOT_NOW_USAGE_ACCESS = 15;
        public static final int NOT_NOW_DISPLAY_OVER = 16;
        public static final int NOT_NOW_BACKGROUND = 17;
        public static final int PERMISSIONS_MISSING = 18;
        public static final int MAX = 19;
        public static final int DO_UPDATE = 20;
        public static final int POPUP_ADD_CHILD = 21;
        public static final int ALREADY_CHILD = 22;
        public static final int SHARE_APP = 23;
        public static final int PAUSE_AGAIN = 24;


        public static final int LINK_SUCCESS = 25;
        public static final int LOCK_SET = 26;
        public static final int PAUSE_SET = 27;
        public static final int FORGOT_ALERT = 28;



    }

    public static class IntentPass {
        public static final int SELECTION_POPUP = 1001;

    }

    public class IntentKeys {
        public static final String URL = "URL";
        public static final String SCREEN_TITLE = "SCREEN_TITLE";
        public static final String IS_PARENT = "IS_PARENT";
        public static final String APP_ARRAY = "APP_ARRAY";
        public static final String SELECT_PARENT = "SELECT_PARENT";
        public static final String FROM_SHARE_DIALOG = "FROM_SHARE_DIALOG";

        public static final String FROM_ACCOUNTS = "FROM_ACCOUNTS";
        public static final String CHILD_LIST_DATA = "CHILD_LIST_DATA";
        public static final String FORM_PERMISSION_DIALOG = "FORM_PERMISSION_DIALOG";
        public static final String FOR_PAUSE = "FOR_PAUSE";
        public static final String FOR_SETTINGS = "FOR_SETTINGS";


    }

    public class ActivityResultKeys {
        public static final int FROM_PURCHASE = 101;
    }

    public class ServiceTypes {
        public static final int LOCK_REQ = 2222;
        public static final int PAUSE_REQ = 3333;

    }

}
