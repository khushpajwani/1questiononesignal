package com.parental.control.teen.kid.math.education.viewmodels;

import android.view.View;

import androidx.lifecycle.ViewModel;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class NotificationViewModel extends ViewModel {
    public boolean inapp = false;
    public boolean pn = false;
    public boolean email = false;
    public NotificationVMInterface notificationVMInterface = null;

    public void changeinApp(View view) {
        inapp = !inapp;
        notificationVMInterface.getSettingValue(inapp, pn, email);
    }

    public void changepn(View view) {
        pn = !pn;
        notificationVMInterface.getSettingValue(inapp, pn, email);
    }

    public void changeemail(View view) {
        email = !email;
        notificationVMInterface.getSettingValue(inapp, pn, email);
    }
}
