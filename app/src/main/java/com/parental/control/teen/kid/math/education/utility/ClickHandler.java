package com.parental.control.teen.kid.math.education.utility;

import android.view.View;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface ClickHandler {
    public void onViewClicked(View view);

}
