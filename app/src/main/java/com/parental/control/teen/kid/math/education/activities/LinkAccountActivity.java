package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityLinkAccountBinding;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.models.UniqueCodeModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.viewmodels.LinkInferface;
import com.parental.control.teen.kid.math.education.viewmodels.LinkViewModel;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LinkAccountActivity extends AppCompatActivity implements ServiceCallBack, SimpleDialogListener, ClickHandler, LinkInferface {


    Context context;
    ActivityLinkAccountBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(LinkAccountActivity.this);
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_link_account);
        LinkViewModel viewModel= ViewModelProviders.of(this).get(LinkViewModel.class);
        binding.setViewmodel(viewModel);
        binding.getViewmodel().linkInferface=this;
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = LinkAccountActivity.this;
        initData();
        binding.getViewmodel().setPincode("249318");
    }


    /**
     * text change of pin and hide error
     */
    private void initData() {
        binding.pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }


            @Override
            public void afterTextChanged(Editable s) {
                binding.pin.setLineColor(Color.parseColor("#C1C6D7"));
                binding.layoutError.setVisibility(View.GONE);
            }
        });

    }

    /**
     * any view clicked form layout
     * @param view
     */

    @Override
    public void onViewClicked(View view) {
        Utils.hideKeypad(context, view);
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.txtLearnMore:
                Utils.setToast(context,"Coming Soon....");
                break;
        }
    }

    /**
     * 200 success response from api
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.LOGIN_UNIQUE_CODE)) {
            try {
                UniqueCodeModel uniqueCodeModel = new Gson().fromJson(data, UniqueCodeModel.class);
                SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.ISLOGGEDIN, true);
                SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_SHOW_WELCOME, true);
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.C_ID, uniqueCodeModel.getData().getChild_id());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.P_ID, uniqueCodeModel.getData().getParent_id());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.PARENT_CODE, uniqueCodeModel.getData().getParent_code());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.C_NAME, uniqueCodeModel.getData().getChildname());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.AVATAR_URL, uniqueCodeModel.getData().getAvatar());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.UNIQUE_CODE, uniqueCodeModel.getData().getChild_link_code());
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.AUTH_TOKEN, uniqueCodeModel.getData().getRemember_token());

//                Utils.setToast(context, uniqueCodeModel.getMessage());
                Utils.logEvent("login", null);
                DialogUtils.simpleDialog(context, context.getResources().getString(R.string.linked_success), context.getResources().getString(R.string.get_started)
                        , AppConstants.PopupType.LINK_SUCCESS, LinkAccountActivity.this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * failure response from api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.LOGIN_UNIQUE_CODE)) {
            binding.pin.setLineColor(ResourcesCompat.getColor(getResources(), R.color.nd_red, getTheme()));
            binding.layoutError.setVisibility(View.VISIBLE);
            binding.txtError.setText(message);
        }

    }

    /**
     * dialog button pressed
     * @param popupType
     */
    @Override
    public void buttonPressed(int popupType) {
        if (popupType == AppConstants.PopupType.LINK_SUCCESS) {
            startActivity(new Intent(context, OnboardingScreenActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }
    }


    /**
     * calidation before api call
     * @param allcorrect
     * @param pin
     * @param message
     */
    @Override
    public void validatedAll(boolean allcorrect, String pin, String message) {
        if (allcorrect) {
            RxApiRequestHandler.loginuniquecode(context, LinkAccountActivity.this, pin, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.DEVICE_ID));
        } else {
            binding.pin.setLineColor(ResourcesCompat.getColor(getResources(), R.color.nd_red, getTheme()));
            binding.layoutError.setVisibility(View.VISIBLE);
            binding.txtError.setText(message);

        }

    }
}