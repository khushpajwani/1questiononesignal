package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public  class CurrentPauseModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("statusCode")
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("value")
        private String value;
        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private String id;
        @SerializedName("pause_timezone")
        private String pause_timezone;
        @SerializedName("pause_future_time")
        private String pause_future_time;
        @SerializedName("pause_current_time")
        private String pause_current_time;
        @SerializedName("currentpausestatus")
        private boolean currentpausestatus;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPause_timezone() {
            return pause_timezone;
        }

        public void setPause_timezone(String pause_timezone) {
            this.pause_timezone = pause_timezone;
        }

        public String getPause_future_time() {
            return pause_future_time;
        }

        public void setPause_future_time(String pause_future_time) {
            this.pause_future_time = pause_future_time;
        }

        public String getPause_current_time() {
            return pause_current_time;
        }

        public void setPause_current_time(String pause_current_time) {
            this.pause_current_time = pause_current_time;
        }

        public boolean getCurrentpausestatus() {
            return currentpausestatus;
        }

        public void setCurrentpausestatus(boolean currentpausestatus) {
            this.currentpausestatus = currentpausestatus;
        }
    }
}
