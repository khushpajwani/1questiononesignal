package com.parental.control.teen.kid.math.education.utility;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Process;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.FloatRange;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.activities.WebActivity;
import com.parental.control.teen.kid.math.education.application.AppConfig;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.receivers.AppPauseReceiver;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.parental.control.teen.kid.math.education.services.LoadAppListService;

import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.security.Provider;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class Utils {

    public static Toast toast;
    private static TransparentProgressDialog mProgressDialog;
    static boolean doubleBackToExitPressedOnce = false;

    /**
     * Set toast message as string
     */
    public static void setToast(Context context, String msg) {
        try {
            if (toast != null) {
                toast.cancel();
            }
            toast = SToast.makeText(context, msg.trim(), SToast.LENGTH_LONG, 0, true);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, (int) context.getResources().getDimension(R.dimen._40sdp));
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * cancel toast message as string
     */
    public static void cancelToast() {
        try {
            if (toast != null) {
                toast.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <T extends TextView> String gettext(T t) {
        return t.getText().toString().trim();
    }


    public static void exitApp(Context context) {
        ((Activity) context).finishAndRemoveTask();

        /*if (doubleBackToExitPressedOnce) {
            ((Activity) context).finishAndRemoveTask();
            return;
        }
        doubleBackToExitPressedOnce = true;
        Utils.setToast(context, context.getResources().getString(R.string.exit));
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);*/
    }

    /**
     * Check for internet connectivity
     */

    public static boolean isConnectingToInternet(Context context) {
        // Also detect scenario of, connected to wifi but wifi is not having internet
        boolean isOnline = false;
        try {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkCapabilities capabilities = manager.getNetworkCapabilities(manager.getActiveNetwork());  // ACCESS_NETWORK_STATE permission
            isOnline = capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isOnline) {
            setToast(context, context.getResources().getString(R.string.noInternet));
        }
        return isOnline;
    }
    /*public static boolean isConnectingToInternet(Context context) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            setToast(context, context.getResources().getString(R.string.noInternet));
            return false;
            adb devices
            adb shell
            pm disable-user --user 0 com.miui.cloudservice
        }
    }*/

    /**
     * Show loader on complete task or background process
     */
    public static void showProgressDialog(final Context ctx) {

        try {

            if(!(ctx instanceof LoadAppListService)) {
                if (mProgressDialog == null) {
                    mProgressDialog = new TransparentProgressDialog(ctx);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();

                } else {

                    mProgressDialog.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Hide loader on complete task or background process
     */
    public static void hideProgressDialog(final Context ctx) {

        try {
            if(!(ctx instanceof LoadAppListService)) {

                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkAllPermissionsGranted(Context context, DevicePolicyManager dpm, ComponentName componentName, PowerManager powerManager) {


        if (!isAccessibilitySettingsOn(context)) {
            return false;
        }
        if (!Settings.canDrawOverlays(context)) {
            return false;
        }
       /* if (!hasNotificationAccess(context)) {
            return false;
        }*/
        /*if (!dpm.isAdminActive(componentName)) {
            return false;
        }*/

//        if (!SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
        if (!powerManager.isIgnoringBatteryOptimizations(context.getPackageName())) {
            return false;
        }
//        }

        return true;
    }


    public static void startService(Context context, Class<?> serviceClass, Class<?> reciverClass, int type) {

        try {
            if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.ISLOGGEDIN, false)) {

                Log.e("TAG", "startService:" + type);
                try {
                    if (serviceClass != null && !isServiceRunning(context, serviceClass)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            context.startForegroundService(new Intent(context, serviceClass));
                        } else {
                            context.startService(new Intent(context, serviceClass));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(context, reciverClass);
                intent.putExtra("type", type);
                Calendar cal = Calendar.getInstance();
                if (type == AppConstants.ServiceTypes.LOCK_REQ) {
                    //2 second
                    intent.putExtra("time", "every 4 seconds");
                    cal.setTimeInMillis(System.currentTimeMillis() + 1000 * 4);
                } else {
                    //15 second
                    intent.putExtra("time", "every 15 seconds");
                    cal.setTimeInMillis(System.currentTimeMillis() + 1000 * 15);
                }
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, type, intent, 0);
//                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
                    alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(), pendingIntent), pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setPause(Context context, int time) {
        if (time != 0) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(context, AppPauseReceiver.class);
            intent.putExtra("type", AppConstants.ServiceTypes.PAUSE_REQ);
            Calendar cal = Calendar.getInstance();
            intent.putExtra("time", "at 1 minute");
            long unpause_time = -1;
            if (time == 30) {
                //30 minutes
                LinkedHashMap<String, String> paramHashMap = new LinkedHashMap<>();
                paramHashMap.put("time",time+" minutes");
                Utils.logEvent("pause_questions", paramHashMap);
                unpause_time = System.currentTimeMillis() + ((1000 * 60) * 30);
                cal.setTimeInMillis(unpause_time);
            } else {
                //1 hr
                LinkedHashMap<String, String> paramHashMap = new LinkedHashMap<>();
                if(time>1){
                    paramHashMap.put("time",time+ " hrs");
                }else {
                    paramHashMap.put("time",time+ " hr");
                }
                Utils.logEvent("pause_questions", paramHashMap);
                unpause_time = System.currentTimeMillis() + (((1000 * 60) * 60) * time);
                cal.setTimeInMillis(unpause_time);
            }
//            cal.setTimeInMillis(System.currentTimeMillis() + (1000 * 15));

            SharedPreferenceUtils.preferencePutLong(AppConstants.SharedPreferenceKeys.UNPAUSE_TIME, unpause_time);
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AppConstants.ServiceTypes.PAUSE_REQ, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AppConstants.ServiceTypes.PAUSE_REQ, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
                alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(), pendingIntent), pendingIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            }
        } else {
            LinkedHashMap<String, String> paramHashMap = new LinkedHashMap<>();
            paramHashMap.put("time","Until I turn it back on");
            Utils.logEvent("pause_questions", paramHashMap);
            SharedPreferenceUtils.preferencePutLong(AppConstants.SharedPreferenceKeys.UNPAUSE_TIME, -1);
        }
        SharedPreferenceUtils.preferencePutBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, true);
        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");

    }


    public static void cancelAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, AppPauseReceiver.class);
        intent.putExtra("type", AppConstants.ServiceTypes.PAUSE_REQ);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis() + 1000);
//        SharedPreferenceUtils.preferencePutLong(AppConstants.SharedPreferenceKeys.UNPAUSE_TIME, cal.getTimeInMillis());
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AppConstants.ServiceTypes.PAUSE_REQ, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AppConstants.ServiceTypes.PAUSE_REQ, intent, PendingIntent.FLAG_UPDATE_CURRENT);

//        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
            alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(), pendingIntent), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }
    }

    public static String setTwoDigit(String val) {
        if (val.length() == 1)
            return "0" + val;
        return val;
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAccessibilitySettingsOn(Context context) {
        try {
            AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            final int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, Process.myUid(), context.getPackageName());
            boolean usage = false;
            usage = mode == AppOpsManager.MODE_DEFAULT ?
                    (context.checkCallingOrSelfPermission(Manifest.permission.PACKAGE_USAGE_STATS) == PackageManager.PERMISSION_GRANTED)
                    : (mode == AppOpsManager.MODE_ALLOWED);
            return usage;
        } catch (Exception e) {
            return false;
        }
    }

    /*public static boolean hasNotificationAccess(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = context.getPackageName();
        return !(enabledNotificationListeners == null || !enabledNotificationListeners.contains(packageName));
    }*/

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }

    public static void setOrientation(Context context) {

//        if (isTablet(context)) {
//            ((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        } else {
//            ((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        }
    }

    public static void statusbarTransparent(Activity activity, boolean transparentStatus, boolean haveBgImage) {

        try {
            try {
                if (activity.getClass() != WebActivity.class) {
                    LinkedHashMap<String, String> paramHashMap = new LinkedHashMap<>();
                    paramHashMap.put("screen_name", activity.getClass().getSimpleName().replace("Activity", "Screen"));
                    Utils.logEvent("screen_view", paramHashMap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Window w = activity.getWindow();
            View view = w.getDecorView();
            if (haveBgImage) {
                w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                w.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int flags = view.getSystemUiVisibility();
                    flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    view.setSystemUiVisibility(flags);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int Nflags = view.getSystemUiVisibility();
                        Nflags &= ~View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                        view.setSystemUiVisibility(Nflags);
                    }
                }
                keyboardTheme(activity);
            } else {
                ((ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content)).getChildAt(0).setFitsSystemWindows(true);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.white));
                w.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.white));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int flags = view.getSystemUiVisibility();
                    flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    view.setSystemUiVisibility(flags);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int Nflags = view.getSystemUiVisibility();
                        Nflags |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                        view.setSystemUiVisibility(Nflags);
                    }
                }
            }
            if (transparentStatus) {
                w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.setStatusBarColor(Color.TRANSPARENT);
                view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void keyboardTheme(Activity activity) {
        Window w = activity.getWindow();
        View view = w.getDecorView();
        KeyboardUtils.addKeyboardToggleListener(activity, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                if (isVisible) {
                    w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                    w.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.white));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        int flags = view.getSystemUiVisibility();
                        flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                        view.setSystemUiVisibility(flags);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            int Nflags = view.getSystemUiVisibility();
                            Nflags |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                            view.setSystemUiVisibility(Nflags);
                        }
                    }
                } else {
                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                    w.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        int flags = view.getSystemUiVisibility();
                        flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                        view.setSystemUiVisibility(flags);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            int Nflags = view.getSystemUiVisibility();
                            Nflags &= ~View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                            view.setSystemUiVisibility(Nflags);
                        }
                    }
                }
            }
        });
    }

    public static void statusbarTransparentNew(Activity activity, boolean transparentStatus, boolean haveBgImage,int color) {

        try {
            try {
                if (activity.getClass() != WebActivity.class) {
                    LinkedHashMap<String, String> paramHashMap = new LinkedHashMap<>();
                    paramHashMap.put("screen_name", activity.getClass().getSimpleName().replace("Activity", "Screen"));
                    Utils.logEvent("screen_view", paramHashMap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Window w = activity.getWindow();
            View view = w.getDecorView();
            if (haveBgImage) {
                w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                w.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int flags = view.getSystemUiVisibility();
                    flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    view.setSystemUiVisibility(flags);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int Nflags = view.getSystemUiVisibility();
                        Nflags &= ~View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                        view.setSystemUiVisibility(Nflags);
                    }
                }
                keyboardThemeNew(activity,color);
            } else {
                ((ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content)).getChildAt(0).setFitsSystemWindows(true);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setStatusBarColor(ContextCompat.getColor(activity, color));
                w.setNavigationBarColor(ContextCompat.getColor(activity, color));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int flags = view.getSystemUiVisibility();
                    flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    view.setSystemUiVisibility(flags);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int Nflags = view.getSystemUiVisibility();
                        Nflags |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                        view.setSystemUiVisibility(Nflags);
                    }
                }
            }
            if (transparentStatus) {
                w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.setStatusBarColor(Color.TRANSPARENT);
                view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void keyboardThemeNew(Activity activity,int color) {
        Window w = activity.getWindow();
        View view = w.getDecorView();
        KeyboardUtils.addKeyboardToggleListener(activity, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                if (isVisible) {
                    w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                    w.setNavigationBarColor(ContextCompat.getColor(activity, color));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        int flags = view.getSystemUiVisibility();
                        flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                        view.setSystemUiVisibility(flags);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            int Nflags = view.getSystemUiVisibility();
                            Nflags |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                            view.setSystemUiVisibility(Nflags);
                        }
                    }
                } else {
                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                    w.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        int flags = view.getSystemUiVisibility();
                        flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                        view.setSystemUiVisibility(flags);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            int Nflags = view.getSystemUiVisibility();
                            Nflags &= ~View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                            view.setSystemUiVisibility(Nflags);
                        }
                    }
                }
            }
        });
    }

    public static boolean checkValidationError(Context context, TextInputEditText edt, TextInputLayout textInputLayout, String type, TextInputEditText... oldedt) {
        if (type.equalsIgnoreCase(AppConstants.ValidationTypes.EMAIL)) {
            if (StringUtils.isEmpty(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_email));
                return true;
            } else if (StringUtils.isNotEmpty(gettext(edt)) && !isValidEmail(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_email));
                return true;
            } else {
                setError(textInputLayout, false, null);
            }
        } else if (type.equalsIgnoreCase(AppConstants.ValidationTypes.PASSWORD_NORMAL)) {
            if (StringUtils.isEmpty(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_password));
                return true;
            } else if (gettext(edt).length() < 4) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_valid_password));
                return true;
            } else {
                setError(textInputLayout, false, null);
            }
        } else if (type.equalsIgnoreCase(AppConstants.ValidationTypes.CONFIRM_PASSWORD)) {
            if (StringUtils.isEmpty(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_cpass));
                return true;
            } else if (StringUtils.isNotEmpty(gettext(edt)) && !gettext(edt).equalsIgnoreCase(gettext(oldedt[0]))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_cpass));
                return true;
            } else {
                setError(textInputLayout, false, null);
            }
        } else if (type.equalsIgnoreCase(AppConstants.ValidationTypes.LOGIN_PIN)) {
            if (StringUtils.isEmpty(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_password));
                return true;
            } else if (gettext(edt).length() < 4) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_valid_password));
                return true;
            } else if (!gettext(edt).equalsIgnoreCase(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PIN))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_wrong_pin));
                return true;
            } else {
                setError(textInputLayout, false, null);
            }
        } else if (type.equalsIgnoreCase(AppConstants.ValidationTypes.NAME)) {
            if (StringUtils.isEmpty(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_name));
                return true;
            } else {
                setError(textInputLayout, false, null);
            }
        } else if (type.equalsIgnoreCase(AppConstants.ValidationTypes.STUDENT_NAME)) {
            if (StringUtils.isEmpty(gettext(edt))) {
                setError(textInputLayout, true, context.getResources().getString(R.string.err_enter_studentname));
                return true;
            } else {
                setError(textInputLayout, false, null);
            }
        }
        return false;
    }


    private static void setError(TextInputLayout textInputLayout, boolean hasError, String errorText) {
        if (hasError) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorText);
            textInputLayout.startAnimation(Utils.shakeError());

        } else {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
        }
    }

    private static boolean isValidEmail(CharSequence target) {
        return (Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 7, 0, 0);
        shake.setDuration(400);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    public static void hideKeypad(Context context, View view) {
        try {
            new Handler().post(() -> {
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    view.clearFocus();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setColorFilter(Context context, int colorid, ImageView imageView) {
        if (imageView != null) {

            imageView.setColorFilter(ContextCompat.getColor(context, colorid), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public static String loadJSONFromAsset(Context context, String filename) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static void setpasswordBigDot(Context context, TextInputLayout txtInpPin, EditText edtPin) {
        edtPin.setTransformationMethod(new MyPasswordTransformationMethod());
        if (txtInpPin != null) {
            txtInpPin.setEndIconOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (edtPin.getTransformationMethod() != null) {
                                edtPin.setTransformationMethod(null);
                            } else {
                                edtPin.setTransformationMethod(new MyPasswordTransformationMethod());
                            }
                            edtPin.setSelection(Utils.gettext(edtPin).length());
                        }
                    });
                }
            });
        }
    }

    public static void setBold(Context context, ViewGroup ll) {
        Typeface font_yekan = Typeface.createFromAsset(context.getAssets(), "fonts/poppins_regular.ttf");
        final int childCount = ll.getChildCount();
        for (int i = 0; i < childCount; i++) {
            try {
                if (ll.getChildAt(i) instanceof TextInputLayout) {
                    ((TextInputLayout) ll.getChildAt(i)).setTypeface(font_yekan);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void resetNewDay() {
       /* Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String todayDate = df.format(c);
        if (StringUtils.isEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.COUNT_DATE))) {
            SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.COUNT_DATE, todayDate);
            SharedPreferenceUtils.preferencePutInteger(AppConstants.SharedPreferenceKeys.TODAY_COUNT, 0);
        } else if (!SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.COUNT_DATE).equalsIgnoreCase(todayDate)) {
            SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.COUNT_DATE, todayDate);
            SharedPreferenceUtils.preferencePutInteger(AppConstants.SharedPreferenceKeys.TODAY_COUNT, 0);
        }*/
    }


    /*public static boolean isAndroidGoEdition(Context context) {
        final String GMAIL_GO = "com.google.android.gm.lite";
        final String YOUTUBE_GO = "com.google.android.apps.youtube.mango";
        final String GOOGLE_GO = "com.google.android.apps.searchlite";
        final String ASSISTANT_GO = "com.google.android.apps.assistant";

        boolean isGmailGoPreInstalled = isPreInstalledApp(context, GMAIL_GO);
        boolean isYoutubeGoPreInstalled = isPreInstalledApp(context, YOUTUBE_GO);
        boolean isGoogleGoPreInstalled = isPreInstalledApp(context, GOOGLE_GO);
        boolean isAssistantGoPreInstalled = isPreInstalledApp(context, ASSISTANT_GO);

        if (isGoogleGoPreInstalled | isAssistantGoPreInstalled) {
            return true;
        }

        if (isGmailGoPreInstalled && isYoutubeGoPreInstalled) {
            return true;
        }

        return false;
    }*/

    private static boolean isPreInstalledApp(Context context, String packageName) {
        try {
            PackageManager pacMan = context.getPackageManager();
            PackageInfo packageInfo = pacMan.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            if (packageInfo != null) {
                //Check if comes with the image OS
                int mask = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;
                return (packageInfo.applicationInfo.flags & mask) != 0;
            }
        } catch (PackageManager.NameNotFoundException e) {
            //The app isn't installed
        }
        return false;
    }

    public static void logEvent(String key, LinkedHashMap<String, String> paramHashMap) {

        Bundle params = new Bundle();
        if (paramHashMap != null && paramHashMap.size() > 0) {
            for (Map.Entry<String, String> e : paramHashMap.entrySet()) {
                params.putString(e.getKey(), e.getValue());
                Log.e("TAG", "LogEvent: " + e.getKey() + "|" + e.getValue());
            }
        }
        AppConfig.mFirebaseAnalytics.logEvent(key, params);
    }

    public static void dimBehind(PopupWindow popupWindow) {
        View container;
        if (popupWindow.getBackground() == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                container = (View) popupWindow.getContentView().getParent();
            } else {
                container = popupWindow.getContentView();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                container = (View) popupWindow.getContentView().getParent().getParent();
            } else {
                container = (View) popupWindow.getContentView().getParent();
            }
        }
        Context context = popupWindow.getContentView().getContext();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.5f;
        wm.updateViewLayout(container, p);
    }

    public static TextView createLink(TextView targetTextView, String completeString,
                                      String partToClick, ClickableSpan clickableAction) {

        SpannableString spannableString = new SpannableString(completeString);

        // make sure the String is exist, if it doesn't exist
        // it will throw IndexOutOfBoundException
        int startPosition = completeString.indexOf(partToClick);
        int endPosition = completeString.lastIndexOf(partToClick) + partToClick.length();

        spannableString.setSpan(clickableAction, startPosition, endPosition,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        targetTextView.setText(spannableString);
        targetTextView.setMovementMethod(LinkMovementMethod.getInstance());

        return targetTextView;
    }




}
