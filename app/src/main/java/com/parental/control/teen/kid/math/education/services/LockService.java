package com.parental.control.teen.kid.math.education.services;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.activities.OverlayActivity;
import com.parental.control.teen.kid.math.education.activities.SplashActivity;
import com.parental.control.teen.kid.math.education.application.AppConfig;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.CommLockInfoManager;

import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Timer;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class LockService extends Service {

    //    public static final String UNLOCK_ACTION = "UNLOCK_ACTION";
//    public static final String LOCK_SERVICE_LASTTIME = "LOCK_SERVICE_LASTTIME";
//    public static final String LOCK_SERVICE_LASTAPP = "LOCK_SERVICE_LASTAPP";
    private static final String TAG = "LockService";
    public boolean threadIsTerminate = false;
    UsageStatsManager sUsageStatsManager;
    Timer timer = new Timer();
    //    private boolean lockState;
    private CommLockInfoManager mLockInfoManager;

    AlarmManager alarmManager;
    Calendar cal;
    PendingIntent pendingIntent;

    @Nullable
    private ActivityManager activityManager;

//    public LockService() {
//        super("LockService");
//    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // do your jobs here
        Log.e(TAG, "onStartCommand: onStartCommand");
//        startForeground();
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
        startForeground();
//        }else {
//            startForeground(1, new Notification());
//        }
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;

    }

    private void startForeground() {
        Intent notificationIntent = new Intent(this, SplashActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Random rnd = new Random();
        int notificationID = 100000 + rnd.nextInt(900000);
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        final String CHANNEL_ID = this.getResources().getString(R.string.app_name) + "_ID";
        if (notificationManager != null && Build.VERSION.SDK_INT >= 26) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, this.getResources().getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);
            mChannel.setSound(null, null);
            mChannel.enableLights(false);
            mChannel.enableVibration(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        int icon = R.drawable.ic_noti;
        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
            icon = R.drawable.ic_noti_go;
        }
        startForeground(notificationID, new NotificationCompat.Builder(this, CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(icon)
                .setColor(ContextCompat.getColor(LockService.this, R.color.selected))
                .setContentTitle(getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_LOW) // Set priority to PRIORITY_LOW to mute notification sound
                .setContentText(this.getResources().getString(R.string.app_name) + " is running in background")
                .setContentIntent(pendingIntent)
                .build());
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_square))

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onStartCommand: onCreate");

//        lockState = SharedPreferenceUtils.preferenceGetBoolean(LockAppConstants.LOCK_STATE,false);
        mLockInfoManager = new CommLockInfoManager(this);
        activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

//        IntentFilter filter = new IntentFilter();
//        filter.addAction(Intent.ACTION_SCREEN_ON);
//        filter.addAction(Intent.ACTION_SCREEN_OFF);
//        filter.addAction(UNLOCK_ACTION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sUsageStatsManager = (UsageStatsManager) this.getSystemService(Context.USAGE_STATS_SERVICE);
        }

        threadIsTerminate = true;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationUtil.createNotification(this, "Sagar lock", "Lock running in background");
//        }

        alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), this.getClass());
        intent.putExtra("type", AppConstants.ServiceTypes.LOCK_REQ);
        intent.putExtra("time", "every 4 seconds");
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), AppConstants.ServiceTypes.LOCK_REQ, intent, PendingIntent.FLAG_ONE_SHOT);


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                runForever();
            }
        });


    }

//    @Override
//    protected void onHandleIntent(Intent intent) {
//        runForever();
//    }

    private void runForever() {

        while (threadIsTerminate) {
            String packageName = getLauncherTopApp(LockService.this, activityManager);
//            Log.e(TAG, "runForever: " + packageName);
            if (/*lockState &&*/ StringUtils.isNoneEmpty(packageName) && !inWhiteList(packageName)) {
                if (mLockInfoManager.isLockedPackageName(packageName)) {

                    //is not pause


                    try {
                        if (!SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_PAUSE, false)) {
                            if (StringUtils.isNoneEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK))
                                    && packageName.equalsIgnoreCase(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK))) {

                            }/* else if (SharedPreferenceUtils.preferenceGetInteger(AppConstants.SharedPreferenceKeys.MAX_LIMIT, 0) > 0
                                    && SharedPreferenceUtils.preferenceGetInteger(AppConstants.SharedPreferenceKeys.TODAY_COUNT, 0)
                                    >= SharedPreferenceUtils.preferenceGetInteger(AppConstants.SharedPreferenceKeys.MAX_LIMIT, 0)) {

                            }*/else if(!Settings.canDrawOverlays(LockService.this)){

                            } else {

                                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                                passwordLock(packageName);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    passwordLock(packageName);

                    continue;
                } else {
                    SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, "");
                }
            }
            try {
                Thread.sleep(210);
            } catch (Exception ignore) {
            }
        }
    }

    private boolean inWhiteList(String packageName) {
        return packageName.equals(LockService.this.getPackageName());
    }

    public String getLauncherTopApp(@NonNull Context context, @NonNull ActivityManager activityManager) {
        //isLockTypeAccessibility = SharedPreferenceUtils.getBoolean(LockAppConstants.LOCK_TYPE, false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            List<ActivityManager.RunningTaskInfo> appTasks = activityManager.getRunningTasks(1);
            if (null != appTasks && !appTasks.isEmpty()) {
                return appTasks.get(0).topActivity.getPackageName();
            }
        } else {
            long endTime = System.currentTimeMillis();
            long beginTime = endTime - (1000 * 10);
            String result = "";
            UsageEvents.Event event = new UsageEvents.Event();
            UsageEvents usageEvents = sUsageStatsManager.queryEvents(beginTime, endTime);
            while (usageEvents.hasNextEvent()) {
                usageEvents.getNextEvent(event);
                if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                    result = event.getPackageName();
                }
            }
            if (!TextUtils.isEmpty(result)) {
                return result;
            }
        }
        return "";
    }


    private void passwordLock(String packageName) {
        try {
//            Log.e(TAG, "passwordLock: "+SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.OVERLAY_INFO) );

            boolean allSetupDone = true;

           /* ArrayList<QuestionsModels.Question> quesionsArray = new ArrayList<>();
            try {
                if (StringUtils.isNoneEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.QUESTIONS_LIST))) {
                    quesionsArray.addAll(new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.QUESTIONS_LIST), new TypeToken<ArrayList<QuestionsModels.Question>>() {
                    }.getType()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (quesionsArray.size() <= 0) {
                allSetupDone = false;
            }
            if (StringUtils.isEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.SELECTED_QUE_NUM))) {
                allSetupDone = false;
            }*/

            if (allSetupDone) {

                Log.e(TAG, "passwordLock: call");
                Intent intent = new Intent(LockService.this, OverlayActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*@Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: " );
        threadIsTerminate = false;
        timer.cancel();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationUtil.cancelNotification(this);
//        }
//        lockState = SharedPreferenceUtils.preferenceGetBoolean(LockAppConstants.LOCK_STATE,false);
//        if (lockState) {
            Intent intent = new Intent(this, LockRestarterBroadcastReceiver.class);
            intent.putExtra("type", "lockservice");
            sendBroadcast(intent);
//        }
    }*/


    @Override
    public void onDestroy() {
        Log.e(TAG, "onStartCommand: onDestroy");
        stopForeground(true);
        stopSelf();

        cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis() + 1000 * 4);
//        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
            alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(), pendingIntent), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }
        super.onDestroy();
        threadIsTerminate = false;
        timer.cancel();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "onStartCommand: onTaskRemoved");
        stopForeground(true);
        stopSelf();

        cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis() + 1000 * 4);
//        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        if (SharedPreferenceUtils.preferenceGetBoolean(AppConstants.SharedPreferenceKeys.IS_GODEVICE, false)) {
            alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(), pendingIntent), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
        }
        super.onTaskRemoved(rootIntent);
        threadIsTerminate = false;
        timer.cancel();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationUtil.cancelNotification(this);
//        }
//        lockState = SharedPreferenceUtils.preferenceGetBoolean(LockAppConstants.LOCK_STATE,false);
//        if (lockState) {
//            Intent restartServiceTask = new Intent(getApplicationContext(), this.getClass());
//            restartServiceTask.setPackage(getPackageName());
//            PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), 1495, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
//            AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//            myAlarmService.set(
//                    AlarmManager.ELAPSED_REALTIME,
//                    SystemClock.elapsedRealtime() + 1500,
//                    restartPendingIntent);
//        }

/*
        Intent restartServiceTask = new Intent(getApplicationContext(), this.getClass());
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), AppConstants.ServiceTypes.LOCK_REQ, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        assert myAlarmService != null;
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1500,
                restartPendingIntent);
*/


    }


}
