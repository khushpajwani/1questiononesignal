package com.parental.control.teen.kid.math.education.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class WebActivity extends AppCompatActivity {

    public ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;

//    @BindView(R.id.web_privacy)
//    ObservableWebView wv;

    @BindView(R.id.web_privacy)
    WebView wv;

    String url = "",title="";
    @BindView(R.id.layoutWeb)
    RelativeLayout layoutWeb;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.layoutHeaderBase)
    RelativeLayout layoutHeaderBase;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(WebActivity.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        Utils.statusbarTransparent(this, false, false);
        ButterKnife.bind(this);

        url = getIntent().getStringExtra(AppConstants.IntentKeys.URL);
        title = getIntent().getStringExtra(AppConstants.IntentKeys.SCREEN_TITLE);

        txtTitle.setText(title);
        wv.setWebChromeClient(new WebChromeClient());
        wv.getSettings().setJavaScriptEnabled(true);
//        wv.setOnScrollChangedCallback(new ObservableWebView.OnScrollChangedCallback() {
//            public void onScroll(int l, int t, int oldl, int oldt) {
//                if (t > 0) {
//                    if(headerShadow.getVisibility()== View.GONE) {
//                        headerShadow.setVisibility(View.VISIBLE);
//                    }
//                } else {
//                    if(headerShadow.getVisibility()== View.VISIBLE) {
//                        headerShadow.setVisibility(View.GONE);
//                    }
//                }
//            }
//        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        wv.getSettings().setSupportZoom(true);
        wv.setScrollBarStyle(ScrollView.SCROLLBARS_OUTSIDE_OVERLAY);
//        wv.setWebViewClient(new WebViewClient() {});

        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setAllowContentAccess(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAllowFileAccessFromFileURLs(true);
        wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
        wv.setClickable(true);

        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setDisplayZoomControls(false);
//        if (url.equalsIgnoreCase(AppConstants.ApiURL.OSPREY)) {
//            wv.getSettings().setLoadWithOverviewMode(true);
//            wv.getSettings().setUseWideViewPort(true);
//        }

        wv.setWebChromeClient(new WebChromeClient() {
            // For 3.0+ Devices (Start)
            // onActivityResult attached before constructor
            protected void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                try {
                    mUploadMessage = uploadMsg;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType("image/*");
                    startActivityForResult(Intent.createChooser(i, getString(R.string.filebrowser)), FILECHOOSER_RESULTCODE);
                } catch (Exception e) {
                    e.printStackTrace();
//                    Toast.makeText(WebActivity.this, getString(R.string.something), Toast.LENGTH_LONG).show();
                }
            }


            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;
                try {
                    Intent intent = fileChooserParams.createIntent();

                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (Exception e) {
                    uploadMessage = null;
//                    Toast.makeText(WebActivity.this, getString(R.string.something), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                try {
                    mUploadMessage = uploadMsg;
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.filebrowser)), FILECHOOSER_RESULTCODE);
                } catch (Exception e) {
                    e.printStackTrace();
//                    Toast.makeText(ContactUsActivity.this, getString(R.string.something), Toast.LENGTH_LONG).show();
                }
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                try {
                    mUploadMessage = uploadMsg;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType("image/*");
                    startActivityForResult(Intent.createChooser(i, getString(R.string.filebrowser)), FILECHOOSER_RESULTCODE);
                } catch (Exception e) {
                    e.printStackTrace();
//                    Toast.makeText(ContactUsActivity.this, getString(R.string.something), Toast.LENGTH_LONG).show();
                }
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            wv.setWebViewClient(new WebViewClient() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(request.getUrl().toString());
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        Utils.showProgressDialog(WebActivity.this);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    Utils.hideProgressDialog(WebActivity.this);
                }

                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                               SslError error) {
                    Utils.hideProgressDialog(WebActivity.this);
                    showsslerror(handler, error);
                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                    Utils.hideProgressDialog(WebActivity.this);
                }
            });
        } else {
            wv.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        Utils.showProgressDialog(WebActivity.this);

                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    Utils.hideProgressDialog(WebActivity.this);
                }

                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                               SslError error) {
                    Utils.hideProgressDialog(WebActivity.this);
                    showsslerror(handler, error);
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Utils.hideProgressDialog(WebActivity.this);
                }
            });
        }

        wv.loadUrl(url);


    }

    /**
     * Confirmation for back press
     */
    @Override
    public void onBackPressed() {
        if (wv.canGoBack()) {
            wv.goBack();
        } else {
            WebActivity.super.onBackPressed();

        }
    }



    /**
     * SSl alert
     *
     * @param handler
     * @param error
     */
    private void showsslerror(final SslErrorHandler handler, SslError error) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(WebActivity.this);
        String[] ssl = getResources().getStringArray(R.array.ssl);
        String message = ssl[0];
        switch (error.getPrimaryError()) {
            case SslError.SSL_UNTRUSTED:
                message = ssl[1];
                break;
            case SslError.SSL_EXPIRED:
                message = ssl[2];
                break;
            case SslError.SSL_IDMISMATCH:
                message = ssl[3];
                break;
            case SslError.SSL_NOTYETVALID:
                message = ssl[4];
                break;
        }
        message += ssl[5];

        builder.setTitle(ssl[0]);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.continue_ahead), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.proceed();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                handler.cancel();
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * File upload result
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
            // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
            // Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = intent == null || resultCode != WebActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else {
//            Toast.makeText(WebActivity.this, getString(R.string.something), Toast.LENGTH_LONG).show();
        }
    }

}
