package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityOverlayBinding;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.models.QuestionsNewModel;
import com.parental.control.teen.kid.math.education.models.ReportRequestModel;
import com.parental.control.teen.kid.math.education.receivers.AppBroadcastReceiver;
import com.parental.control.teen.kid.math.education.services.LockService;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.DialogUtils;
import com.parental.control.teen.kid.math.education.utility.MathView;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class OverlayActivity extends AppCompatActivity implements ServiceCallBack, ClickHandler {

    AppLockNewModel.Data appdata;
    Context context;
    int currentQueNumber = 0;
    boolean firtAttemp = true;
    String reported_QID = "";
    QuestionsNewModel questionsNewModel;
    QuestionsNewModel.Data currentQue;
    long oldtimemillis = 0, timeDifference = 0;

    ActivityOverlayBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityOverlayBinding.inflate(getLayoutInflater());
        binding.setClickHandler(this);
        View view=binding.getRoot();
        context = OverlayActivity.this;
        try {
            Settings.System.putInt(getContentResolver(), "multi_window_enabled", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* if (isTablet(context)) {
//            ((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_overlay_l);
//            ((TextView)findViewById(R.id.txtTopic)).setText("testT");
        } else {*/
//        ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(view);
//            ((TextView)findViewById(R.id.txtTopic)).setText("testP");
        /*   }*/

        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        overridePendingTransition(0, 0);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
            binding.layoutBase.setVisibility(View.VISIBLE);
        }
        init();

    }


    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }


    private void init() {
        Utils.setpasswordBigDot(context, null, binding.edtPin);

        Utils.resetNewDay();

        appdata = new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.OVERLAY_INFO), AppLockNewModel.Data.class);
//        Log.e("TAG", "runoverlay0: " );

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!OverlayActivity.this.isFinishing()) {
//                    Log.e("TAG", "runoverlay: " );
                    try {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String formattedDate = df.format(c.getTime());
                        RxApiRequestHandler.getOverlayQuestionsApi(context, OverlayActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID), formattedDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 400);

        binding.edtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (StringUtils.isNoneEmpty(Utils.gettext(binding.edtPin)) && Utils.gettext(binding.edtPin).length() >= 4) {
                    if (checkValidation()) {
                        Utils.hideKeypad(context, binding.edtPin);
                        Utils.setPause(context, 30);
                        finish();
//                        finishAndRemoveTask();
                        overridePendingTransition(0, 0);
                    }

                }
            }
        });

    }


    private boolean checkValidation() {
        if (!Utils.gettext(binding.edtPin).equalsIgnoreCase(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PIN))) {
            Utils.setToast(context, context.getResources().getString(R.string.err_wrong_pin));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {

        try {
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            this.startActivity(homeIntent);
            this.finish();
//            this.finishAndRemoveTask();
            overridePendingTransition(0, 0);
            Utils.startService(OverlayActivity.this, LockService.class, AppBroadcastReceiver.class, AppConstants.ServiceTypes.LOCK_REQ);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

   /* @OnClick({R.id.txtRequest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtRequest:
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK,data.getApp_id());
                finish();
                break;
        }
    }*/


    @Override
    public void onViewClicked(View view) {
        try {
            Utils.hideKeypad(context, view);
//            clearButtonsTheme();
            if (binding.edtPin.hasFocus()) {
                binding.edtPin.setText("");
                binding.edtPin.clearFocus();
            }
        } catch (Exception e) {
        }
        switch (view.getId()) {
            case R.id.cardOne:
                checkAnswer(currentQue.getAnswer().get(0).getIs_correct(), binding.cardOne, binding.txtOne, currentQue.getAnswer().get(0));
                break;
            case R.id.cardTwo:
                checkAnswer(currentQue.getAnswer().get(1).getIs_correct(), binding.cardTwo, binding.txtTwo, currentQue.getAnswer().get(1));
                break;
            case R.id.cardThree:
                checkAnswer(currentQue.getAnswer().get(2).getIs_correct(), binding.cardThree, binding.txtThree, currentQue.getAnswer().get(2));
                break;
            case R.id.cardFour:
                checkAnswer(currentQue.getAnswer().get(3).getIs_correct(), binding.cardFour, binding.txtFour, currentQue.getAnswer().get(3));
                break;
            case R.id.layoutHint:
                try {
                    View mView = LayoutInflater.from(OverlayActivity.this).inflate(R.layout.popup_tooltip, null, false);
                    PopupWindow popUp = new PopupWindow(mView, (int) context.getResources().getDimension(R.dimen._180sdp), LinearLayout.LayoutParams.WRAP_CONTENT, false);
                    popUp.setTouchable(true);
                    popUp.setFocusable(true);
                    popUp.setOutsideTouchable(true);
                    int width=(int) context.getResources().getDimension(R.dimen._minus60sdp)+(int) context.getResources().getDimension(R.dimen._minus40sdp);
                    int height=(int) context.getResources().getDimension(R.dimen._minus60sdp)+(int) context.getResources().getDimension(R.dimen._minus55sdp);
                    popUp.showAsDropDown(binding.layoutHint,width,height, Gravity.TOP);
                    Utils.dimBehind(popUp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnPin:
                startActivity(new Intent(context, LoginParent.class).putExtra(AppConstants.IntentKeys.FOR_PAUSE,true));

                break;
        }
    }



    private void setTime() {
        //Calculate time difference ( last action to current action)
        timeDifference = System.currentTimeMillis() - oldtimemillis;
        Log.e("TAG", "Time till last action: " + timeDifference);
        //Reset timer
        oldtimemillis = System.currentTimeMillis();
    }

    private void checkAnswer(String isCorrect, RelativeLayout buttonView, MathView txt, QuestionsNewModel.Answer answer) {
        setTime();
        buttonView.setBackground(ContextCompat.getDrawable(context, R.drawable.overlay_btn_press));
        txt.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        if (isCorrect.equalsIgnoreCase("1")) {
            reportApi(true, answer);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
            DialogUtils.excellentDialog(context, -1, new SimpleDialogListener() {
                @Override
                public void buttonPressed(int popupType) {
                    if (currentQueNumber + 1 == questionsNewModel.getData().size()) {
                        nodata();
                    } else {
                        currentQueNumber++;
                        setQuestion(true);
                    }
                }
            }, true);
//                }
//            }, 500);
        } else {
//            firtAttemp = false;
            reportApi(false, answer);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
            firtAttemp = false;
            DialogUtils.excellentDialog(context, -1, new SimpleDialogListener() {
                @Override
                public void buttonPressed(int popupType) {
                    setQuestion(false);
                }
            }, false);
//                }
//            }, 500);
        }
    }

    private void reportApi(boolean iscorrect, QuestionsNewModel.Answer answer) {
        try {
//            if (!reported_QID.equalsIgnoreCase(currentQue.getFq_id())) {
//                reported_QID = currentQue.getFq_id();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c.getTime());

//                RxApiRequestHandler.sendReportApi(context, OverlayActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID)
//                        , firtAttemp, iscorrect, formattedDate, currentQue.getFq_id(), SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));

            ReportRequestModel.Answer answerModel = new ReportRequestModel.Answer();
            answerModel.setIndex(answer.getIndex());
            answerModel.setId(answer.getId());
            answerModel.setTime_duration(String.valueOf(timeDifference));
//            Toast.makeText(context, "firtAttemp:"+firtAttemp+"\n"+"text:"+answer.getName()+"\n"
//                    +"index:"+answer.getIndex()+"\n"+"id:"+answer.getId()+"\n"
//                    +"diff:"+String.valueOf(timeDifference)+"\n", Toast.LENGTH_LONG).show();

            RxApiRequestHandler.sendReportApi2(context, OverlayActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID)
                    , firtAttemp, iscorrect, formattedDate, currentQue.getFq_id(), SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID), answerModel);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSuccess(String requestTag, JsonElement data1) {
        if (!OverlayActivity.this.isFinishing()) {
            if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SEND_REPORT)) {
                Log.e("TAG", "onSuccess: report");
            } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_OVERLAY_QUESTIONS)) {
                if (!OverlayActivity.this.isFinishing()) {
                    try {
                        questionsNewModel = new Gson().fromJson(data1, QuestionsNewModel.class);
                        if (questionsNewModel != null && questionsNewModel.getData() != null && questionsNewModel.getData().size() > 0) {
                            /*Utils.statusbarTransparent(OverlayActivity.this, false, false);
                            if (isTablet(context)) {
                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int width = displayMetrics.widthPixels;
                                int height = displayMetrics.heightPixels;
                                int newwidth = 0, newheight = 0;
                                if (height > width) {
                                    newwidth = height;
                                    newheight = width;
                                } else {
                                    newwidth = width;
                                    newheight = height;
                                }
                                newheight = newheight - (int) context.getResources().getDimension(R.dimen._10sdp);
                                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(newwidth, newheight);
                                layoutBase.setLayoutParams(param);
                            }*/
                            binding.layoutBase.setVisibility(View.VISIBLE);
                            setQuestion(true);
                        } else {
                            nodata();
                        }

                    } catch (Exception e) {
                        nodata();
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void clearButtonsTheme() {
        binding.cardOne.setBackground(ContextCompat.getDrawable(context, R.drawable.overlay_btn));
        binding.cardTwo.setBackground(ContextCompat.getDrawable(context, R.drawable.overlay_btn));
        binding.cardThree.setBackground(ContextCompat.getDrawable(context, R.drawable.overlay_btn));
        binding.cardFour.setBackground(ContextCompat.getDrawable(context, R.drawable.overlay_btn));
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height_landscape", "dimen", "android");
        if (resourceId <= 0) {
            resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        }
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getNavBarHeight() {

        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height_landscape", "dimen", "android");
        if (resourceId <= 0) {
            resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        }
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private void nodata() {
        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, appdata.getApp_id());
        finish();
//        finishAndRemoveTask();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onFailure(String requestTag, String message, String errorcode) {

        if(StringUtils.isNoneEmpty(message) && message.contains("subscription")){
            Utils.setToast(context,message);
        }
        if (!OverlayActivity.this.isFinishing()) {
            if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SEND_REPORT)) {
                Log.e("TAG", "onSuccess: report");
            } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_OVERLAY_QUESTIONS)) {
                nodata();
            }
        }
    }

    private void setQuestion(boolean newquestion) {
        /*currentQue=new QuestionsNewModel.Data();
        List<QuestionsNewModel.Answer> arrayans=new ArrayList<>();
        arrayans.add(new Gson().fromJson("{index:1,id:3917,name:41,is_correct:0}",QuestionsNewModel.Answer.class));
        arrayans.add(new Gson().fromJson("{\"index\": 2,\"id\": \"3917\",\"name\": \"42\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
        arrayans.add(new Gson().fromJson("{\"index\": 3,\"id\": \"3917\",\"name\": \"43\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
        arrayans.add(new Gson().fromJson("{\"index\": 4,\"id\": \"3917\",\"name\": \"44\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
        arrayans.add(new Gson().fromJson("{\"index\": 5,\"id\": \"3917\",\"name\": \"45\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
        arrayans.add(new Gson().fromJson("{\"index\": 6,\"id\": \"3917\",\"name\": \"46\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
        currentQue.setAnswer(arrayans);
        currentQue.setTopic_name("dsadd");
        currentQue.setQuestion_name("dsadd");*/


        currentQue = questionsNewModel.getData().get(currentQueNumber);
        String topicname = currentQue.getTopic_name().toLowerCase();
        try {

            if (topicname.contains("6") || topicname.contains("7") || topicname.contains("8") || topicname.contains("9")
                    || topicname.contains("10")|| topicname.contains("11")) {
                binding.txtQuestion.setVisibility(View.INVISIBLE);
                binding.txtOne.setVisibility(View.INVISIBLE);
                binding.txtTwo.setVisibility(View.INVISIBLE);
                binding.txtThree.setVisibility(View.INVISIBLE);
                binding.txtFour.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        oldtimemillis = System.currentTimeMillis();
        if (newquestion) {
            firtAttemp = true;
        }
        clearButtonsTheme();
        binding.txtTopic.setText(currentQue.getTopic_name());
//        Collections.shuffle(currentQue.getAnswer(),new Random());

        if (!newquestion) {
            try {
         /*   ArrayList<Integer> intarrat = new ArrayList<>();
            final int min = 0;
            final int max = 3;
            while (intarrat.size() < 4) {
                int random = new Random().nextInt((max - min) + 1) + min;
                if (!intarrat.contains(random)) {
                }
                intarrat.add(random);
            }*/


                ArrayList<Integer> content = new ArrayList<Integer>();

                for (int i = 0; i < currentQue.getAnswer().size(); i++) {
                    content.add(i);
                }

                ArrayList<Integer> intarrat = new ArrayList<Integer>();

                Random random = new Random();
                while (content.size() > 0) {
                    int randomIndex = random.nextInt(content.size());
                    intarrat.add(content.remove(randomIndex));
                }


                ArrayList<QuestionsNewModel.Answer> answers = new ArrayList<>();
                for (int i = 0; i < intarrat.size(); i++) {
                    Log.e("TAG", "setQuestion: " + intarrat.get(i));
                    try {
                        answers.add(currentQue.getAnswer().get(intarrat.get(i)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                currentQue.getAnswer().clear();

                currentQue.setAnswer(answers);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        Collections.shuffle(currentQue.getAnswer());


        try {
//            binding.commingSoon.setVisibility(View.VISIBLE);
            binding.txtOne.setDisplayText(currentQue.getAnswer().get(0).getName(), ContextCompat.getColor(context, android.R.color.black));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    binding.txtOne.setVisibility(View.VISIBLE);
                }
            }, 800);
            binding.cardOne.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            binding.cardOne.setVisibility(View.GONE);
        }
        try {
            binding.txtTwo.setDisplayText(currentQue.getAnswer().get(1).getName(), ContextCompat.getColor(context, android.R.color.black));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    binding.txtTwo.setVisibility(View.VISIBLE);
                }
            }, 800);
            binding.cardTwo.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            binding.cardTwo.setVisibility(View.GONE);
        }
        try {
            binding.txtThree.setDisplayText(currentQue.getAnswer().get(2).getName(), ContextCompat.getColor(context, android.R.color.black));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    binding.txtThree.setVisibility(View.VISIBLE);
                }
            }, 800);
            binding.cardThree.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            binding.cardThree.setVisibility(View.GONE);
        }
        try {
            binding.txtFour.setDisplayText(currentQue.getAnswer().get(3).getName(), ContextCompat.getColor(context, android.R.color.black));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    binding.txtFour.setVisibility(View.VISIBLE);
                }
            }, 800);
            binding.cardFour.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            binding.cardFour.setVisibility(View.GONE);
        }

        binding.txtQuestion.setDisplayText(currentQue.getQuestion_name());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.txtQuestion.setVisibility(View.VISIBLE);

            }
        }, 800);
        binding.txtTopic.setText(currentQue.getTopic_name());




//        binding.txtQuestion.setDisplayText("<html><head><style>/ This style sets the width of all images to 100%: /img {width: 100%;}</style></head><body> <img src=\"https://www.w3schools.com/images/w3schools_green.jpg\" alt=\"W3Schools.com\"></body></html>");
//        binding.txtQuestion.setDisplayText("Solve by factoring test test: <br> $$\\textnormal{$x ^ {2} $ + 14x + 49 = 0}$$");
//        txtQuestion.setDisplayText("$$\\textnormal{Find the vertex this is test: g(x) = 2$(x + 1) ^ {2} $ + 1}$$");
//        txtQuestion.setDisplayText("Find the vertex this is test:<br>$$ \\textnormal{ g(x) = 2$(x + 1) ^ {2} $ + 1}$$");

    }

}
