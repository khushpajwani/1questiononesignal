package com.parental.control.teen.kid.math.education.listeners;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface PermissionDialogListener {
    public void positiveButtonPressed(int popupType);
    public void negativeButtonPressed(int popupType);

}
