package com.parental.control.teen.kid.math.education.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.parental.control.teen.kid.math.education.activities.DashboardActivity;
import com.parental.control.teen.kid.math.education.activities.SplashActivity;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.models.NotificationModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class NotificationReceiver extends BroadcastReceiver {

    Context context;
    Intent intent;
    int notiID=0;
    NotificationModel notificationModel;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        this.intent = intent;

        try {
            notiID=intent.getIntExtra("notificationID",0);
            notificationModel=new Gson().fromJson(intent.getStringExtra("data"),NotificationModel.class);
            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase("poitive")) {

            } else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase("negative")) {

            } else {
                context.startActivity(new Intent(context, DashboardActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }

            cancelNotification(context, notiID);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        assert nMgr != null;
        nMgr.cancel(notifyId);
    }

}