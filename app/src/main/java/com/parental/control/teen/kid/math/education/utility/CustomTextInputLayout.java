package com.parental.control.teen.kid.math.education.utility;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.parental.control.teen.kid.math.education.R;
import com.google.android.material.textfield.TextInputLayout;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class CustomTextInputLayout  extends TextInputLayout {
    private Object collapsingTextHelper;
    private Rect bounds;
    private Method recalculateMethod;
    private Context context;
    public View paddingView;


    public CustomTextInputLayout(Context context) {
        this(context, null);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;

        init();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        adjustBounds();
    }

    private void init() {
        try {
            Field cthField = null;
            try {
                cthField = TextInputLayout.class.getDeclaredField("mCollapsingTextHelper");
            } catch (NoSuchFieldException e) {
                cthField = TextInputLayout.class.getDeclaredField("collapsingTextHelper");
            }
            cthField.setAccessible(true);
            collapsingTextHelper = cthField.get(this);

            Field boundsField = null;
            try {
                boundsField = collapsingTextHelper.getClass().getDeclaredField("mCollapsedBounds");
            } catch (NoSuchFieldException e) {
                boundsField = collapsingTextHelper.getClass().getDeclaredField("collapsedBounds");
            }
            boundsField.setAccessible(true);
            bounds = (Rect) boundsField.get(collapsingTextHelper);

            recalculateMethod = collapsingTextHelper.getClass().getDeclaredMethod("recalculate");
        }
        catch (Exception e) {
            collapsingTextHelper = null;
            bounds = null;
            recalculateMethod = null;
            e.printStackTrace();
        }
    }

    private void adjustBounds() {
        if (collapsingTextHelper == null) {
            return;
        }

        try {
            bounds.left = getEditText().getLeft() + getEditText().getPaddingLeft()+(int)context.getResources().getDimension(R.dimen._30sdp);
            recalculateMethod.invoke(collapsingTextHelper);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        refreshPaddingView();
    }

    public void addPaddingView(){
        paddingView = new View(getContext());
        int height = (int) getContext().getResources().getDimension(R.dimen._20sdp);
        ViewGroup.LayoutParams paddingParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                height);
        super.addView(paddingView, 0, paddingParams);
    }

    public void refreshPaddingView(){
        if (paddingView != null) {
            removeView(paddingView);
            paddingView = null;
        }
        addPaddingView();
    }*/
}