package com.parental.control.teen.kid.math.education.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.databinding.FragmentSlideThreeBinding;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class SlideThreeFragment extends Fragment implements ClickHandler {



    Context context;
    FragmentSlideThreeBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding=FragmentSlideThreeBinding.inflate(inflater,container,false);
        binding.setClickHandler(this);
        View view=binding.getRoot();
        context=getActivity();
        return view;
    }

    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if(context instanceof OnboardingScreenActivity){
                    ((OnboardingScreenActivity)context).movetonext();
                }
                break;
            case R.id.txtSkip:
                if(context instanceof OnboardingScreenActivity) {
                    ((OnboardingScreenActivity) context).skipall();
                }

                break;
        }
    }



}