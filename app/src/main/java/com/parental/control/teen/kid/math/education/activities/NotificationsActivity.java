package com.parental.control.teen.kid.math.education.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.databinding.ActivityNotificationsBinding;
import com.parental.control.teen.kid.math.education.models.GetNotiModel;
import com.parental.control.teen.kid.math.education.models.SaveNotiModel;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.ClickHandler;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.viewmodels.NotificationVMInterface;
import com.parental.control.teen.kid.math.education.viewmodels.NotificationViewModel;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class NotificationsActivity extends AppCompatActivity implements ClickHandler, NotificationVMInterface, ServiceCallBack {

    ActivityNotificationsBinding binding;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notifications);
        NotificationViewModel viewmodel = ViewModelProviders.of(this).get(NotificationViewModel.class);
        binding.setViewmodel(viewmodel);
        binding.getViewmodel().notificationVMInterface = this;
        binding.setClickHandler(this);
        binding.headerBase.setClickHandler(this);
        View view=binding.getRoot();
        setContentView(view);
        Utils.statusbarTransparentNew(this, false, false, R.color.nd_background);
        context = this;

        binding.headerBase.txtTitle.setText(getResources().getString(R.string.notification));
        binding.headerBase.layoutHeaderBase.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));


        getsettingdata();

    }

    /**
     * api call for current notification setting
     */
    private void getsettingdata() {
        RxApiRequestHandler.getnotificationSettings(context, NotificationsActivity.this
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PARENT_CODE));
    }

    /**
     * any view clicked from layout
     * @param view
     */
    @Override
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }


    /**
     * api call to get settings
     * @param inapp
     * @param pn
     * @param email
     */
    @Override
    public void getSettingValue(boolean inapp, boolean pn, boolean email) {
        RxApiRequestHandler.savenotificationSettings(context, NotificationsActivity.this
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PARENT_CODE), inapp ? "1" : "0", pn ? "1" : "0", email ? "1" : "0");
    }

    /**
     * 200 success api call response
     * @param requestTag
     * @param data
     */
    @Override
    public void onSuccess(String requestTag, JsonElement data) {
        if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_NOTI_SETTING)) {
            try {
                GetNotiModel getNotiModel = new Gson().fromJson(data, GetNotiModel.class);

                binding.getViewmodel().inapp = getNotiModel.getData().getIn_app_notification().equalsIgnoreCase("1")?true:false;
                binding.getViewmodel().pn = getNotiModel.getData().getPush_notification().equalsIgnoreCase("1")?true:false;
                binding.getViewmodel().email = getNotiModel.getData().getEmail().equalsIgnoreCase("1")?true:false;

                binding.chInApp.setChecked(binding.getViewmodel().inapp);
                binding.chPushNoti.setChecked(binding.getViewmodel().pn);
                binding.chEmail.setChecked(binding.getViewmodel().email);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SAVE_NOTI_SETTING)) {
            try {
                SaveNotiModel saveNotiModel=new Gson().fromJson(data,SaveNotiModel.class);
                Utils.setToast(context,saveNotiModel.getMessage());
                getsettingdata();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * fail response from api
     * @param requestTag
     * @param message
     * @param errorcode
     */
    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        Utils.setToast(context, message);
    }
}