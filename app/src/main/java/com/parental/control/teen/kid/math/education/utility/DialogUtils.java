package com.parental.control.teen.kid.math.education.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.activities.DashboardActivity;
import com.parental.control.teen.kid.math.education.adapters.AvatarAdapter;
import com.parental.control.teen.kid.math.education.adapters.PopupMultipleSelectionClassAdapter;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.listeners.DialogInternalListener;
import com.parental.control.teen.kid.math.education.listeners.DialogSelectionListener;
import com.parental.control.teen.kid.math.education.listeners.PermissionDialogListener;
import com.parental.control.teen.kid.math.education.listeners.RatingDialogListener;
import com.parental.control.teen.kid.math.education.listeners.SelectedAvatarListener;
import com.parental.control.teen.kid.math.education.listeners.SimpleDialogListener;
import com.parental.control.teen.kid.math.education.listeners.UnlimitedListener;
import com.parental.control.teen.kid.math.education.models.AvatarModel;
import com.parental.control.teen.kid.math.education.models.PopupModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class DialogUtils {

    private static Dialog activeAgainDialog;
    private static boolean isGlobalUnlimited = true;


    /**
     * popup Dialog
     */
    public static void popupDialogWithListener(Context appCompatActivity, String title,
                                               ArrayList<PopupModel> originalalPopupText, int popupType,
                                               boolean isMultiSelection, boolean needSearch, DialogSelectionListener dialogSelectionListener
            , String screentitle) {
        try {
            ArrayList<PopupModel> alPopupText = new ArrayList<>();
            //performance improvement
//            alPopupText.addAll(new Gson().fromJson(new Gson().toJson(originalalPopupText), new TypeToken<ArrayList<PopupModel>>() {}.getType()));
//            dismissAlertDialog();
            Dialog dialog = new Dialog(appCompatActivity, R.style.AppTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.popup_multi_selection);

            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                View view = w.getDecorView();
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setStatusBarColor(ContextCompat.getColor(appCompatActivity, android.R.color.white));
                w.setNavigationBarColor(ContextCompat.getColor(appCompatActivity, android.R.color.white));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int flags = view.getSystemUiVisibility();
                    flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    view.setSystemUiVisibility(flags);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int Nflags = view.getSystemUiVisibility();
                        Nflags |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                        view.setSystemUiVisibility(Nflags);
                    }
                }
            }

            if (StringUtils.isNoneEmpty(title)) {
                ((TextView) dialog.findViewById(R.id.txtPopupTitle)).setText(title);
            } else {
                ((TextView) dialog.findViewById(R.id.txtPopupTitle)).setPadding(0, 0, 0, 0);
                ((TextView) dialog.findViewById(R.id.txtPopupTitle)).setVisibility(View.INVISIBLE);
            }
            AppCompatCheckBox chkSelectAll = dialog.findViewById(R.id.chkSelectAll);
            TextView txtSelectAll = dialog.findViewById(R.id.txtSelectAll);
            ImageView imgBack = dialog.findViewById(R.id.imgBack);
            TextView txtMultiOK = dialog.findViewById(R.id.txtMultiOK);
            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
            CardView cardAll = dialog.findViewById(R.id.cardAll);
            ImageView imgLogoAll = dialog.findViewById(R.id.imgLogoAll);
            TextView txtAll = dialog.findViewById(R.id.txtAll);
            TextView txtTip = dialog.findViewById(R.id.txtTip);
            if (popupType == AppConstants.PopupType.QUE) {
                txtTip.setVisibility(View.VISIBLE);
            }
            cardAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chkSelectAll.performClick();
                }
            });
            txtTitle.setText(screentitle);


            RecyclerView recyclerMultiSection = dialog.findViewById(R.id.recyclerMultiSection);
            recyclerMultiSection.setLayoutManager(new LinearLayoutManager(appCompatActivity, LinearLayoutManager.VERTICAL, false));
            PopupMultipleSelectionClassAdapter popupMultipleSelectionClassAdapter = new PopupMultipleSelectionClassAdapter(appCompatActivity, alPopupText,
                    chkSelectAll, isMultiSelection, popupType, new DialogInternalListener() {
                @Override
                public void pressok() {

                    if (isMultiSelection) {
                        checkButtonEnable(alPopupText, btnSubmit);
                    } else {
                        dialog.dismiss();
                        originalalPopupText.clear();
                        originalalPopupText.addAll(new Gson().fromJson(new Gson().toJson(alPopupText), new TypeToken<ArrayList<PopupModel>>() {
                        }.getType()));
                        dialogSelectionListener.doneSelectedChoiceFromPopupList(alPopupText, popupType);
                    }
                }

                @Override
                public void selectAll(boolean isselected) {
                    setSelectColor(appCompatActivity, isselected, imgLogoAll, txtAll, cardAll);
                    checkButtonEnable(alPopupText, btnSubmit);

                }
            });

            checkButtonEnable(alPopupText, btnSubmit);
            recyclerMultiSection.setAdapter(popupMultipleSelectionClassAdapter);

            if (isMultiSelection) {
                btnSubmit.setVisibility(View.VISIBLE);
                chkSelectAll.setVisibility(View.VISIBLE);
                cardAll.setVisibility(View.VISIBLE);
                txtSelectAll.setVisibility(View.VISIBLE);

                chkSelectAll.setOnClickListener(v -> {
                    new Handler().post(() -> {

                        setSelectColor(appCompatActivity, !chkSelectAll.isSelected(), imgLogoAll, txtAll, cardAll);

                        boolean selectAll = !chkSelectAll.isSelected();
                        chkSelectAll.setSelected(selectAll);
                        chkSelectAll.setChecked(selectAll);

                        for (int i = 0; i < alPopupText.size(); i++) {
                            PopupModel data = alPopupText.get(i);
                            data.setChecked(selectAll);
                            alPopupText.set(i, data);
                        }
                        popupMultipleSelectionClassAdapter.notifyDataSet(selectAll);
                    });
                });

                txtSelectAll.setOnClickListener(v -> {
                    chkSelectAll.performClick();
                });

            } else {
                chkSelectAll.setVisibility(View.GONE);
                cardAll.setVisibility(View.GONE);
                txtSelectAll.setVisibility(View.GONE);
            }

            dialog.findViewById(R.id.txtMultiCancel).setOnClickListener(v -> {
//                alPopupText.clear();
//                alPopupText.addAll(new Gson().fromJson(new Gson().toJson(tempalPopupText), new TypeToken<ArrayList<SkillModel.Success_data>>() {}.getType()));
                dialog.dismiss();

            });
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });


            txtMultiOK.setOnClickListener(v -> {
                dialog.dismiss();
                originalalPopupText.clear();
                originalalPopupText.addAll(new Gson().fromJson(new Gson().toJson(alPopupText), new TypeToken<ArrayList<PopupModel>>() {
                }.getType()));
                dialogSelectionListener.doneSelectedChoiceFromPopupList(alPopupText, popupType);
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtMultiOK.performClick();
                }
            });

            if (needSearch) {
                ((EditText) dialog.findViewById(R.id.edtSearch)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        ArrayList<PopupModel> temp = new ArrayList<>();
                        for (PopupModel d : alPopupText) {
                            if (d.getName().toLowerCase().trim().contains(s.toString().toLowerCase().trim())) {
                                temp.add(d);
                            }
                        }
                        popupMultipleSelectionClassAdapter.updateList(temp);
                    }
                });
            } else {
                View llSearch = dialog.findViewById(R.id.llSearch);
                llSearch.setVisibility(View.GONE);
            }
            dialog.show();
            for (int i = 0; i < originalalPopupText.size(); i++) {
                alPopupText.add(new Gson().fromJson(new Gson().toJson(originalalPopupText.get(i)), PopupModel.class));
                popupMultipleSelectionClassAdapter.notifyItemInserted(i);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Popup dialog for topic
    public static void popupDialogWithListenerForTopic(Context appCompatActivity, String title,
                                                       ArrayList<PopupModel> originalalPopupText, int popupType,
                                                       boolean isMultiSelection, boolean needSearch, DialogSelectionListener dialogSelectionListener
            , String screentitle, PopupModel timeTable, ArrayList<PopupModel> topicArray) {
        try {
            ArrayList<PopupModel> alPopupText = new ArrayList<>();

            //performance improvement
//            alPopupText.addAll(new Gson().fromJson(new Gson().toJson(originalalPopupText), new TypeToken<ArrayList<PopupModel>>() {}.getType()));
//            dismissAlertDialog();
            Dialog dialog = new Dialog(appCompatActivity, R.style.AppTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.popup_multi_selection_topic);

            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                View view = w.getDecorView();
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                w.setStatusBarColor(ContextCompat.getColor(appCompatActivity, android.R.color.white));
                w.setNavigationBarColor(ContextCompat.getColor(appCompatActivity, android.R.color.white));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int flags = view.getSystemUiVisibility();
                    flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    view.setSystemUiVisibility(flags);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int Nflags = view.getSystemUiVisibility();
                        Nflags |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                        view.setSystemUiVisibility(Nflags);
                    }
                }
            }

            if (StringUtils.isNoneEmpty(title)) {
                ((TextView) dialog.findViewById(R.id.txtPopupTitle)).setText(title);
            } else {
                ((TextView) dialog.findViewById(R.id.txtPopupTitle)).setPadding(0, 0, 0, 0);
                ((TextView) dialog.findViewById(R.id.txtPopupTitle)).setVisibility(View.INVISIBLE);
            }
            AppCompatCheckBox chkSelectAll = dialog.findViewById(R.id.chkSelectAll);
            TextView txtSelectAll = dialog.findViewById(R.id.txtSelectAll);
            ImageView imgBack = dialog.findViewById(R.id.imgBack);
            TextView txtMultiOK = dialog.findViewById(R.id.txtMultiOK);
            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
            CardView cardAll = dialog.findViewById(R.id.cardAll);
            ImageView imgLogoAll = dialog.findViewById(R.id.imgLogoAll);
            TextView txtAll = dialog.findViewById(R.id.txtAll);
            TextView txtTip = dialog.findViewById(R.id.txtTip);

            AppCompatCheckBox chkTables = dialog.findViewById(R.id.chkTables);
            CardView cardTables = dialog.findViewById(R.id.cardTables);
            ImageView imgTables = dialog.findViewById(R.id.imgTables);
            TextView txtTables = dialog.findViewById(R.id.txtTables);
            LinearLayout linearTables = dialog.findViewById(R.id.linearTimeTable);


            txtTables.setText(timeTable.getName());


            if (popupType == AppConstants.PopupType.QUE) {
                txtTip.setVisibility(View.VISIBLE);
            }
            cardAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chkSelectAll.performClick();
                }
            });
            txtTitle.setText(screentitle);


            RecyclerView recyclerMultiSection = dialog.findViewById(R.id.recyclerMultiSection);
            recyclerMultiSection.setLayoutManager(new LinearLayoutManager(appCompatActivity, LinearLayoutManager.VERTICAL, false));
            PopupMultipleSelectionClassAdapter popupMultipleSelectionClassAdapter = new PopupMultipleSelectionClassAdapter(appCompatActivity, alPopupText,
                    chkSelectAll, isMultiSelection, popupType, new DialogInternalListener() {
                @Override
                public void pressok() {

                    if (isMultiSelection) {
                        checkButtonEnable(alPopupText, btnSubmit, chkTables, imgTables, txtTables, cardTables, appCompatActivity, timeTable);
                    } else {
                        dialog.dismiss();
                        originalalPopupText.clear();
                        originalalPopupText.addAll(new Gson().fromJson(new Gson().toJson(alPopupText), new TypeToken<ArrayList<PopupModel>>() {
                        }.getType()));
                        dialogSelectionListener.doneSelectedChoiceFromPopupList(alPopupText, popupType);
                    }
                }

                @Override
                public void selectAll(boolean isselected) {
                    setSelectColor(appCompatActivity, isselected, imgLogoAll, txtAll, cardAll);
                    checkButtonEnable(alPopupText, btnSubmit, chkTables, imgTables, txtTables, cardTables, appCompatActivity, timeTable);

                }
            });


            chkTables.setOnClickListener(v -> {

                new Handler().post(() -> {

                    setSelectColor(appCompatActivity, chkTables.isSelected(), imgLogoAll, txtAll, cardAll);


                    boolean selectAll = chkTables.isSelected();
                    chkSelectAll.setSelected(selectAll);
                    chkSelectAll.setChecked(selectAll);

                    for (int i = 0; i < alPopupText.size(); i++) {
                        PopupModel data = alPopupText.get(i);
                        data.setChecked(selectAll);
                        alPopupText.set(i, data);
                    }
                    popupMultipleSelectionClassAdapter.notifyDataSet(selectAll);

                    setSelectColor(appCompatActivity, !chkTables.isSelected(), imgTables, txtTables, cardTables);
                    timeTable.setChecked(!chkTables.isSelected());
                    chkTables.setSelected(!chkTables.isSelected());
                    chkTables.setChecked(!chkTables.isSelected());

                });

            });


            cardTables.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    chkTables.performClick();

                }
            });

            checkButtonEnable(alPopupText, btnSubmit, chkTables, imgTables, txtTables, cardTables, appCompatActivity, timeTable);
            recyclerMultiSection.setAdapter(popupMultipleSelectionClassAdapter);

            if (isMultiSelection) {
                btnSubmit.setVisibility(View.VISIBLE);
                chkSelectAll.setVisibility(View.VISIBLE);
                cardAll.setVisibility(View.VISIBLE);
                txtSelectAll.setVisibility(View.VISIBLE);

                chkSelectAll.setOnClickListener(v -> {
                    new Handler().post(() -> {


                        timeTable.setChecked(chkSelectAll.isSelected());
                        chkTables.setSelected(chkSelectAll.isSelected());
                        chkTables.setChecked(chkSelectAll.isSelected());
                        setSelectColor(appCompatActivity, chkSelectAll.isSelected(), imgTables, txtTables, cardTables);

                        boolean selectAll = !chkSelectAll.isSelected();
                        setSelectColor(appCompatActivity, selectAll, imgLogoAll, txtAll, cardAll);
                        chkSelectAll.setSelected(selectAll);
                        chkSelectAll.setChecked(selectAll);

                        for (int i = 0; i < alPopupText.size(); i++) {
                            PopupModel data = alPopupText.get(i);
                            data.setChecked(selectAll);
                            alPopupText.set(i, data);
                        }
                        popupMultipleSelectionClassAdapter.notifyDataSet(selectAll);
                    });
                });

                txtSelectAll.setOnClickListener(v -> {
                    chkSelectAll.performClick();
                });

            } else {
                chkSelectAll.setVisibility(View.GONE);
                cardAll.setVisibility(View.GONE);
                txtSelectAll.setVisibility(View.GONE);
            }

            dialog.findViewById(R.id.txtMultiCancel).setOnClickListener(v -> {
//                alPopupText.clear();
//                alPopupText.addAll(new Gson().fromJson(new Gson().toJson(tempalPopupText), new TypeToken<ArrayList<SkillModel.Success_data>>() {}.getType()));
                dialog.dismiss();

            });
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });


            txtMultiOK.setOnClickListener(v -> {
                dialog.dismiss();
                topicArray.clear();

                alPopupText.add(timeTable);
                topicArray.addAll(new Gson().fromJson(new Gson().toJson(alPopupText), new TypeToken<ArrayList<PopupModel>>() {
                }.getType()));
                dialogSelectionListener.doneSelectedChoiceFromPopupList(alPopupText, popupType);
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtMultiOK.performClick();
                }
            });

            if (needSearch) {
                ((EditText) dialog.findViewById(R.id.edtSearch)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        ArrayList<PopupModel> temp = new ArrayList<>();
                        for (PopupModel d : alPopupText) {
                            if (d.getName().toLowerCase().trim().contains(s.toString().toLowerCase().trim())) {
                                temp.add(d);
                            }
                        }
                        popupMultipleSelectionClassAdapter.updateList(temp);
                    }
                });
            } else {
                View llSearch = dialog.findViewById(R.id.llSearch);
                llSearch.setVisibility(View.GONE);
            }
            dialog.show();
            for (int i = 0; i < originalalPopupText.size(); i++) {
                alPopupText.add(new Gson().fromJson(new Gson().toJson(originalalPopupText.get(i)), PopupModel.class));
                popupMultipleSelectionClassAdapter.notifyItemInserted(i);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void setSelectColor(Context context, boolean isSelected, ImageView imgLogo, TextView txtAll, CardView cardItem) {
        if (isSelected) {
            txtAll.setTextColor(ContextCompat.getColor(context, R.color.selected));
            imgLogo.setImageResource(R.drawable.ic_check_box);
            imgLogo.setColorFilter(ContextCompat.getColor(context, R.color.selected), PorterDuff.Mode.SRC_ATOP);
            cardItem.setCardBackgroundColor(ContextCompat.getColor(context, R.color.unselected_bg));
            cardItem.setCardElevation(context.getResources().getDimension(R.dimen._3sdp));
            cardItem.setTranslationZ(context.getResources().getDimension(R.dimen._3sdp));
        } else {
            txtAll.setTextColor(ContextCompat.getColor(context, R.color.unselected_text));
            imgLogo.setImageResource(R.drawable.ic_check_un);
            imgLogo.setColorFilter(ContextCompat.getColor(context, R.color.unselected_text), PorterDuff.Mode.SRC_ATOP);
            cardItem.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
            cardItem.setCardElevation(context.getResources().getDimension(R.dimen._1sdp));
            cardItem.setTranslationZ(context.getResources().getDimension(R.dimen._1sdp));
        }
    }

    private static void checkButtonEnable(ArrayList<PopupModel> alPopupText, Button btnSubmit) {
        boolean ifAnyChecked = false;
        for (int i = 0; i < alPopupText.size(); i++) {
            if (alPopupText.get(i).isChecked()) {
                ifAnyChecked = true;
                break;
            }
        }
        if (ifAnyChecked) {
            btnSubmit.setEnabled(true);
            btnSubmit.setBackgroundResource(R.drawable.btn_selector);
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setBackgroundResource(R.drawable.btn_new_disable);
        }
    }

    private static void checkButtonEnable(ArrayList<PopupModel> alPopupText, Button btnSubmit, AppCompatCheckBox chkTimeTable, ImageView imgTables, TextView txtTables, CardView cardTables, Context appCompatActivity, PopupModel timeTable) {


        boolean ifAnyChecked = false;
        for (int i = 0; i < alPopupText.size(); i++) {
            if (alPopupText.get(i).isChecked()) {
                ifAnyChecked = true;
                break;
            }
        }

        if (ifAnyChecked) {
            chkTimeTable.setSelected(false);
            chkTimeTable.setChecked(false);
            setSelectColor(appCompatActivity, false, imgTables, txtTables, cardTables);
            timeTable.setChecked(false);
        } else {
            chkTimeTable.setSelected(true);
            chkTimeTable.setChecked(true);
            setSelectColor(appCompatActivity, true, imgTables, txtTables, cardTables);
            timeTable.setChecked(true);
        }

        if (!chkTimeTable.isSelected() && !ifAnyChecked) {
            btnSubmit.setEnabled(false);
            btnSubmit.setBackgroundResource(R.drawable.btn_new_disable);
        } else {
            btnSubmit.setEnabled(true);
            btnSubmit.setBackgroundResource(R.drawable.btn_selector);
        }

       /* if (ifAnyChecked) {
            btnSubmit.setEnabled(true);
            btnSubmit.setBackgroundResource(R.drawable.btn_selector);
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setBackgroundResource(R.drawable.btn_new_disable);
        }*/
    }


    public static String popupClickSelectAllListener(TextView textView, ArrayList<PopupModel> alClass, boolean needmulti) {
        try {
            StringBuilder content = new StringBuilder();
            StringBuilder id = new StringBuilder();
            for (int i = 0; i < alClass.size(); i++) {
                if (alClass.get(i).isChecked()) {
                    if (needmulti) {
                        if (content.length() == 0) {
                            content = new StringBuilder(alClass.get(i).getName());
//                            id = new StringBuilder(alClass.get(i).getId());
                            id = new StringBuilder(alClass.get(i).getName());

                        } else {
                            content.append(", ").append(alClass.get(i).getName());
//                            id.append(", ").append(alClass.get(i).getId());
                            id.append(", ").append(alClass.get(i).getName());

                        }
                    } else {
                        content = new StringBuilder(alClass.get(i).getName());
//                        id = new StringBuilder(alClass.get(i).getId());
                        id = new StringBuilder(alClass.get(i).getName());

                    }
                }
            }
            if (StringUtils.isNotEmpty(content.toString()) && textView != null) {
                textView.setText(content.toString());
            }
            return id.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String QSpopupClickSelectAllListener(TextView textView, ArrayList<PopupModel> alClass, boolean needmulti) {
        try {
            StringBuilder content = new StringBuilder();
            StringBuilder id = new StringBuilder();
            for (int i = 0; i < alClass.size(); i++) {
                if (alClass.get(i).isChecked()) {
                    if (needmulti) {
                        if (content.length() == 0) {
                            content = new StringBuilder(alClass.get(i).getName());
                            id = new StringBuilder(alClass.get(i).getId());

                        } else {
                            content.append(", ").append(alClass.get(i).getName());
                            id.append(",").append(alClass.get(i).getId());

                        }
                    } else {
                        content = new StringBuilder(alClass.get(i).getName());
                        id = new StringBuilder(alClass.get(i).getId());

                    }
                }
            }
            if (StringUtils.isNotEmpty(content.toString()) && textView != null) {
                textView.setText(content.toString());
            }
            return id.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static void simpleDialogwithButton(Context appCompatActivity, String title, String button, int popupType,
                                              SimpleDialogListener simpleDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_simple_button);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            TextView btnSubmit = dialog.findViewById(R.id.btnSubmit);
            txtTitle.setText(title);
            btnSubmit.setText(button);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (simpleDialogListener != null) {
                        simpleDialogListener.buttonPressed(popupType);
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void simpleDialogwithButtonLarge(Context appCompatActivity, String title, String button, int popupType,
                                                   SimpleDialogListener simpleDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_simple_buttonlarge);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            TextView btnSubmit = dialog.findViewById(R.id.btnSubmit);
            txtTitle.setText(title);
            btnSubmit.setText(button);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (simpleDialogListener != null) {
                        simpleDialogListener.buttonPressed(popupType);
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void twoDialogwithButton(Context appCompatActivity, String title, String leftbutton, String rightbutton, int popupType,
                                           SimpleDialogListener simpleDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_two_button);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            ImageView btnClose = dialog.findViewById(R.id.btnClose);
            TextView btnLeft = dialog.findViewById(R.id.btnLeft);
            TextView btnRight = dialog.findViewById(R.id.btnRight);

            txtTitle.setText(title);
            btnLeft.setText(leftbutton);
            btnRight.setText(rightbutton);

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (simpleDialogListener != null) {
                        simpleDialogListener.buttonPressed(popupType);
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static void ratingDialogwithButton(Context appCompatActivity, int popupType,
                                              RatingDialogListener ratingDialogListener) {
        try {

            Dialog dialog = new Dialog(appCompatActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_rating);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
            Button btnNotnow = dialog.findViewById(R.id.btnNotnow);
            ImageView btnClose = dialog.findViewById(R.id.btnClose);
            Button btnEmail = dialog.findViewById(R.id.btnEmail);

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (ratingDialogListener != null) {
                        ratingDialogListener.ratenow(popupType);
                    }
                }
            });

            btnNotnow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (ratingDialogListener != null) {
                        ratingDialogListener.notnow(popupType);
                    }
                }
            });

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (ratingDialogListener != null) {
                        ratingDialogListener.notnow(popupType);
                    }
                }
            });
            btnEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (ratingDialogListener != null) {
                        ratingDialogListener.emailus(popupType);
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void permissionDialog(Context appCompatActivity, String title, String button, int popupType,
                                        PermissionDialogListener permissionDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.popup_permission);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            Button btnAllow = dialog.findViewById(R.id.btnAllow);
            TextView txtNotNow = dialog.findViewById(R.id.txtNotNow);
            txtTitle.setText(title);
            btnAllow.setText(button);
            btnAllow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (permissionDialogListener != null) {
                        permissionDialogListener.positiveButtonPressed(popupType);
                    }
                }
            });
            txtNotNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (permissionDialogListener != null) {
                        permissionDialogListener.negativeButtonPressed(popupType);
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void simpleDialog(Context appCompatActivity, String message, String button, int popupType,
                                    SimpleDialogListener simpleDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity, R.style.DialogSlideAnim);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_message);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            TextView txtMessage = dialog.findViewById(R.id.txtMessage);
            Button btnCancel = dialog.findViewById(R.id.btnCancel);
            txtMessage.setText(message);
            btnCancel.setText(button);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (simpleDialogListener != null) {
                                simpleDialogListener.buttonPressed(popupType);
                            }
                        }
                    }, 200);

                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void welcomeDialog(Context appCompatActivity, String title, String button, int popupType,
                                     SimpleDialogListener simpleDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity, R.style.DialogSlideAnim);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.welcome_message);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }
            ImageView imgAvatar = dialog.findViewById(R.id.imgAvatar);
            TextView txtTitle = dialog.findViewById(R.id.txtTitle);
            Button start = dialog.findViewById(R.id.btnCancel);
            txtTitle.setText(title);
            start.setText(button);
            if (StringUtils.isNotEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL))) {
                Glide.with(appCompatActivity).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(imgAvatar);
            }
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (simpleDialogListener != null) {
                                simpleDialogListener.buttonPressed(popupType);
                            }
                        }
                    }, 200);

                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void avatarDialog(Context appCompatActivity, int popupType, ArrayList<AvatarModel.Data> avatarModelArrayList,
                                    SelectedAvatarListener selectedAvatarListener) {
        try {
            final AvatarModel.Data[] selected_avatarModel = {null};
            Dialog dialog = new Dialog(appCompatActivity, R.style.AvatarDialogSlideAnim);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_selectpic);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }


            TextView txtCancel = dialog.findViewById(R.id.txtCancel);
            TextView txtNext = dialog.findViewById(R.id.txtNext);
            RecyclerView recAvatar = dialog.findViewById(R.id.recAvatar);
            ImageView imgSelectedAvatar = dialog.findViewById(R.id.imgSelectedAvatar);
            ImageView imgClear = dialog.findViewById(R.id.imgClear);

            txtNext.setEnabled(false);

            if (appCompatActivity instanceof DashboardActivity) {
                txtNext.setText(appCompatActivity.getResources().getString(R.string.save));
            }
            if (StringUtils.isNotEmpty(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL))) {
                Glide.with(appCompatActivity).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(imgSelectedAvatar);
                imgClear.setVisibility(View.VISIBLE);


            }



            AvatarAdapter avatarAdapter = new AvatarAdapter(appCompatActivity, avatarModelArrayList, new SelectedAvatarListener() {
                @Override
                public void madeselection(int popupType, AvatarModel.Data avatarModel) {
                    selected_avatarModel[0] = avatarModel;
                    txtNext.setTextColor(ContextCompat.getColor(appCompatActivity, R.color.nd_dialogbtn));
                    txtNext.setEnabled(true);
                    Glide.with(appCompatActivity).load(selected_avatarModel[0].getName()).placeholder(R.drawable.placeholder_avatar).into(imgSelectedAvatar);
                    imgClear.setVisibility(View.VISIBLE);
                }

                @Override
                public void cancelselection(int popupType) {
                }
            });
            recAvatar.setLayoutManager(new GridLayoutManager(appCompatActivity, 4));
            recAvatar.setAdapter(avatarAdapter);

            for (int i = 0; i <avatarModelArrayList.size() ; i++) {
                if(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL).contains(avatarModelArrayList.get(i).getName())){
                    avatarAdapter.setSelectedposition(i);
                    break;
                }
            }

            imgClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    avatarAdapter.clearselection();
                    selected_avatarModel[0] = null;
                    txtNext.setTextColor(ContextCompat.getColor(appCompatActivity, R.color.nd_dialogbtn_disable));
                    txtNext.setEnabled(false);
                    Glide.with(appCompatActivity).load("").placeholder(R.drawable.placeholder_avatar).into(imgSelectedAvatar);
                    imgClear.setVisibility(View.GONE);

                }
            });

            txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (selectedAvatarListener != null) {
                                selectedAvatarListener.cancelselection(popupType);
                            }
                        }
                    }, 200);

                }
            });

            txtNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (selectedAvatarListener != null) {
                                selectedAvatarListener.madeselection(popupType, selected_avatarModel[0]);
                            }
                        }
                    }, 200);

                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void excellentDialog(Context appCompatActivity, int popupType, SimpleDialogListener simpleDialogListener, boolean isCorrect) {
        try {
            Window screenwindow = ((Activity)appCompatActivity).getWindow();
            Dialog dialog = new Dialog(appCompatActivity, R.style.AvatarDialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setGravity(Gravity.BOTTOM);



            dialog.setCancelable(false);
            dialog.setContentView(R.layout.excellent_selectpic);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            ImageView imgCorrect = dialog.findViewById(R.id.imgCorrect);
            ImageView imgWrong = dialog.findViewById(R.id.imgWrong);
            ImageView imgTryAgain = dialog.findViewById(R.id.imgTryAgain);
            TextView txtName = dialog.findViewById(R.id.txtName);

            final int min = 1;
            final int max = 4;
            final int random = new Random().nextInt((max - min) + 1) + min;
            if (isCorrect) {
                screenwindow.setNavigationBarColor(ContextCompat.getColor(appCompatActivity, R.color.correct_navigation));
                int res = 0;
                if (random == 1) {
                    res = R.drawable.correct_one;
                } else if (random == 2) {
                    res = R.drawable.correct_two;
                } else if (random == 3) {
                    res = R.drawable.correct_three;
                }else {
                    res = R.drawable.correct_four;
                }
                Glide.with(appCompatActivity).load(res).into(imgCorrect);
                imgCorrect.setVisibility(View.VISIBLE);
                txtName.setText(appCompatActivity.getResources().getString(R.string.correct_text).replaceAll("xxxx",SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_NAME)));
                txtName.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        simpleDialogListener.buttonPressed(popupType);
                        screenwindow.setNavigationBarColor(ContextCompat.getColor(appCompatActivity, R.color.nd_background));
                        dialog.dismiss();
                    }
                },4000);

            }else {
                screenwindow.setNavigationBarColor(ContextCompat.getColor(appCompatActivity, R.color.wrong_navigation));
                int res = 0;
                if (random == 1) {
                    res = R.drawable.wrong_one;
                } else if (random == 2) {
                    res = R.drawable.wrong_two;
                } else if (random == 3) {
                    res = R.drawable.wrong_three;
                }else {
                    res = R.drawable.wrong_four;
                }
                Glide.with(appCompatActivity).load(res).into(imgWrong);
                imgWrong.setVisibility(View.VISIBLE);
                imgTryAgain.setVisibility(View.VISIBLE);
                imgTryAgain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        simpleDialogListener.buttonPressed(popupType);
                        screenwindow.setNavigationBarColor(ContextCompat.getColor(appCompatActivity, R.color.nd_background));
                        dialog.dismiss();
                    }
                });
            }
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void logoutDialog(Context appCompatActivity, int popupType, PermissionDialogListener permissionDialogListener) {
        try {
            Dialog dialog = new Dialog(appCompatActivity, R.style.DialogSlideAnim);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.popup_logout);
            if (dialog.getWindow() != null) {
                Window w = dialog.getWindow();
                w.setBackgroundDrawableResource(android.R.color.transparent);
            }

            Button btnLogout = dialog.findViewById(R.id.btnLogout);
            Button btnCancel = dialog.findViewById(R.id.btnCancel);
            ImageView imgAvatar = dialog.findViewById(R.id.imgAvatar);

            Glide.with(appCompatActivity).load(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.AVATAR_URL)).placeholder(R.drawable.placeholder_avatar).into(imgAvatar);

            btnLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (permissionDialogListener != null) {
                                permissionDialogListener.positiveButtonPressed(popupType);
                            }
                        }
                    }, 200);

                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (permissionDialogListener != null) {
                                permissionDialogListener.negativeButtonPressed(popupType);
                            }
                        }
                    }, 200);

                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
