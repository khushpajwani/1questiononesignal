package com.parental.control.teen.kid.math.education.listeners;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public interface SimpleDialogListener {
    public void buttonPressed(int popupType);

}
