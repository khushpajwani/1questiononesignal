package com.parental.control.teen.kid.math.education.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.listeners.DialogInternalListener;
import com.parental.control.teen.kid.math.education.models.PopupModel;
import com.parental.control.teen.kid.math.education.utility.Utils;

import java.util.ArrayList;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */

public class PopupMultipleSelectionClassAdapter extends RecyclerView.Adapter<PopupMultipleSelectionClassAdapter.MyViewHolder> {

    private ArrayList<PopupModel> alProduct;
    private CheckBox chkSelectAll;
    private Context context;
    private int counter = 0;
    private boolean isMultiSelection, checked;
    PopupModel oldSelectedRadio = null;
    int popupType;
    DialogInternalListener dialogInternalListener;
    int oldSelectedPosition=-1;

    public PopupMultipleSelectionClassAdapter(Context context, ArrayList<PopupModel> alProduct,
                                              AppCompatCheckBox chkSelectAll, boolean isMultiSelection,
                                              int popupType, DialogInternalListener dialogInternalListener) {
        this.context = context;
        this.alProduct = alProduct;
        this.chkSelectAll = chkSelectAll;
        this.isMultiSelection = isMultiSelection;
        this.popupType = popupType;
        this.dialogInternalListener = dialogInternalListener;
        oldSelectedRadio = null;
        checked = false;
        oldSelectedPosition=-1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_popup_multiselection, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PopupModel popupModel = alProduct.get(position);
        boolean isSelected = popupModel.isChecked();


        holder.txtRowTitle.setText(popupModel.getName());
        holder.itemView.setOnClickListener(v -> {
            holder.chkSelect.performClick();
        });

        if (isMultiSelection) {
            holder.imgLogo.setVisibility(View.VISIBLE);
        }else {
            if(isSelected){
                holder.imgSingle.setVisibility(View.VISIBLE);
            }else {
                holder.imgSingle.setVisibility(View.GONE);
            }
        }

        if (isMultiSelection) {
//            holder.chkSelect.setVisibility(View.VISIBLE);
//            holder.rdoSelect.setVisibility(View.GONE);
            holder.chkSelect.setChecked(isSelected);
            holder.chkSelect.setSelected(isSelected);

            setSelectColor(isSelected, holder.txtRowTitle, holder.imgLogo, holder.cardItem,isMultiSelection,holder.imgSingle,position);
            if (isSelected) {
                if (counter < alProduct.size()) {
                    counter++;
                }
            } else {
                if (counter > 0) {
                    counter--;
                }
            }

            selectAll();



            holder.chkSelect.setOnClickListener(v -> {
                boolean select = !holder.chkSelect.isSelected();
                popupModel.setChecked(select);
                holder.chkSelect.setSelected(select);
                holder.chkSelect.setChecked(select);

                if (holder.chkSelect.isSelected() || holder.chkSelect.isChecked()) {
                    setSelectColor(true, holder.txtRowTitle, holder.imgLogo, holder.cardItem, isMultiSelection, holder.imgSingle,position);
                    counter++;
                } else {
                    setSelectColor(false, holder.txtRowTitle, holder.imgLogo, holder.cardItem, isMultiSelection, holder.imgSingle,position);
                    counter--;
                }

                selectAll();
                Utils.hideKeypad(context, holder.chkSelect);
                dialogInternalListener.pressok();

            });

        } else {
//            holder.chkSelect.setVisibility(View.GONE);
//            holder.rdoSelect.setVisibility(View.VISIBLE);

            setSelectColor(isSelected, holder.txtRowTitle, holder.imgLogo, holder.cardItem, isMultiSelection, holder.imgSingle,position);

            if (isSelected) {
                oldSelectedRadio = popupModel;
                holder.rdoSelect.setChecked(true);
                holder.rdoSelect.setSelected(true);
                popupModel.setChecked(true);
            } else {
                holder.rdoSelect.setChecked(false);
                holder.rdoSelect.setSelected(false);
                popupModel.setChecked(false);
            }

            holder.rdoSelect.setOnClickListener(v -> {
                Utils.hideKeypad(context, v);
                if (oldSelectedRadio != null) {
                    oldSelectedRadio.setChecked(false);
                }
                popupModel.setChecked(true);
                this.notifyItemChanged(oldSelectedPosition);
                this.notifyItemChanged(position);
//                notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialogInternalListener.pressok();
                    }
                }, 300);

            });

            holder.layoutBase.setOnClickListener(v -> {
                Utils.hideKeypad(context, v);
                if (oldSelectedRadio != null) {
                    oldSelectedRadio.setChecked(false);
                }
                popupModel.setChecked(true);
                this.notifyItemChanged(oldSelectedPosition);
                this.notifyItemChanged(position);
//                notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialogInternalListener.pressok();
                    }
                }, 300);
            });
        }
    }

    private void setSelectColor(boolean isSelected, TextView txtRowTitle, ImageView imgLogo, CardView cardItem, boolean isMultiSelection, ImageView imgSingle, int position) {
        if (isMultiSelection) {
            imgLogo.setVisibility(View.VISIBLE);
        }else {
            if(isSelected){
                oldSelectedPosition=position;
                imgSingle.setVisibility(View.VISIBLE);
            }else {
                imgSingle.setVisibility(View.GONE);
            }
        }
        if (isSelected) {
            txtRowTitle.setTextColor(ContextCompat.getColor(context, R.color.selected));
            imgLogo.setImageResource(R.drawable.ic_check_box);
            imgLogo.setColorFilter(ContextCompat.getColor(context, R.color.selected), PorterDuff.Mode.SRC_ATOP);
//            imgLogo.setImageResource(R.drawable.check_fill);
            cardItem.setCardBackgroundColor(ContextCompat.getColor(context, R.color.unselected_bg));

            if(Utils.isTablet(context)){
                cardItem.setCardElevation(context.getResources().getDimension(R.dimen._2sdp));
                cardItem.setTranslationZ(context.getResources().getDimension(R.dimen._2sdp));
            }else {
                cardItem.setCardElevation(context.getResources().getDimension(R.dimen._3sdp));
                cardItem.setTranslationZ(context.getResources().getDimension(R.dimen._3sdp));
            }

        } else {
            txtRowTitle.setTextColor(ContextCompat.getColor(context, R.color.unselected_text));
            imgLogo.setImageResource(R.drawable.ic_check_un);
            imgLogo.setColorFilter(ContextCompat.getColor(context, R.color.unselected_text), PorterDuff.Mode.SRC_ATOP);
//            imgLogo.setImageResource(R.drawable.check_empty);
            cardItem.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
            cardItem.setCardElevation(context.getResources().getDimension(R.dimen._1sdp));
            cardItem.setTranslationZ(context.getResources().getDimension(R.dimen._1sdp));
        }
    }

    @Override
    public int getItemCount() {
        return alProduct.size();
    }

    public void updateList(ArrayList<PopupModel> list) {
        alProduct = list;
        notifyDataSetChanged();
    }

    public void notifyDataSet(boolean value) {
        if (value)
            counter = alProduct.size();
        else
            counter = 0;

        notifyDataSetChanged();
    }

    private void selectAll() {
        if (counter == alProduct.size()) {
            dialogInternalListener.selectAll(true);
            chkSelectAll.setChecked(true);
            chkSelectAll.setSelected(true);
        } else {
            dialogInternalListener.selectAll(false);

            chkSelectAll.setChecked(false);
            chkSelectAll.setSelected(false);
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtRowTitle;
        CheckBox chkSelect;
        RadioButton rdoSelect;
        ImageView imgLogo;
        CardView cardItem;
        LinearLayout layoutBase;
        ImageView imgSingle;

        MyViewHolder(View view) {
            super(view);
            txtRowTitle = view.findViewById(R.id.txtRowTitle);
            chkSelect = view.findViewById(R.id.chkSelect);
            rdoSelect = view.findViewById(R.id.rdoSelect);
            imgLogo = view.findViewById(R.id.imgLogo);
            cardItem = view.findViewById(R.id.cardItem);
            layoutBase = view.findViewById(R.id.layoutBase);
            imgSingle = view.findViewById(R.id.imgSingle);

        }
    }
}

