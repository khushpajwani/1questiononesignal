package com.parental.control.teen.kid.math.education.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sagar Gadani on 2021.
 * IT Path Solutions
 * Ahmedabad, Gujarat, India
 */
public class DashboardModel {


    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;
    @SerializedName("statusCode")
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @SerializedName("average_speed")
        private String average_speed;
        @SerializedName("achievements")
        private List<Achievements> achievements;
        @SerializedName("total_minutes")
        private String total_minutes;
        @SerializedName("correct_answer")
        private String correct_answer;
        @SerializedName("total_question")
        private String total_question;
        @SerializedName("device_link")
        private String device_link;
        @SerializedName("grade")
        private String grade;
        @SerializedName("no_of_questions")
        private String no_of_questions;
        @SerializedName("avatar")
        private String avatar;
        @SerializedName("device_info")
        private String device_info;
        @SerializedName("device_token")
        private String device_token;
        @SerializedName("child_link_code")
        private String child_link_code;
        @SerializedName("android_id")
        private String android_id;
        @SerializedName("childname")
        private String childname;
        @SerializedName("parent_id")
        private String parent_id;
        @SerializedName("child_id")
        private String child_id;
        @SerializedName("last_answer")
        private String last_answer;
        @SerializedName("grade_color")
        private String grade_color;
        @SerializedName("grade_background_color")
        private String grade_background_color;

        public String getGrade_color() {
            return grade_color;
        }

        public void setGrade_color(String grade_color) {
            this.grade_color = grade_color;
        }

        public String getGrade_background_color() {
            return grade_background_color;
        }

        public void setGrade_background_color(String grade_background_color) {
            this.grade_background_color = grade_background_color;
        }

        public String getLast_answer() {
            return last_answer;
        }

        public void setLast_answer(String last_answer) {
            this.last_answer = last_answer;
        }

        public String getAverage_speed() {
            return average_speed;
        }

        public void setAverage_speed(String average_speed) {
            this.average_speed = average_speed;
        }

        public List<Achievements> getAchievements() {
            return achievements;
        }

        public void setAchievements(List<Achievements> achievements) {
            this.achievements = achievements;
        }

        public String getTotal_minutes() {
            return total_minutes;
        }

        public void setTotal_minutes(String total_minutes) {
            this.total_minutes = total_minutes;
        }

        public String getCorrect_answer() {
            return correct_answer;
        }

        public void setCorrect_answer(String correct_answer) {
            this.correct_answer = correct_answer;
        }

        public String getTotal_question() {
            return total_question;
        }

        public void setTotal_question(String total_question) {
            this.total_question = total_question;
        }

        public String getDevice_link() {
            return device_link;
        }

        public void setDevice_link(String device_link) {
            this.device_link = device_link;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getNo_of_questions() {
            return no_of_questions;
        }

        public void setNo_of_questions(String no_of_questions) {
            this.no_of_questions = no_of_questions;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getDevice_info() {
            return device_info;
        }

        public void setDevice_info(String device_info) {
            this.device_info = device_info;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getChild_link_code() {
            return child_link_code;
        }

        public void setChild_link_code(String child_link_code) {
            this.child_link_code = child_link_code;
        }

        public String getAndroid_id() {
            return android_id;
        }

        public void setAndroid_id(String android_id) {
            this.android_id = android_id;
        }

        public String getChildname() {
            return childname;
        }

        public void setChildname(String childname) {
            this.childname = childname;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getChild_id() {
            return child_id;
        }

        public void setChild_id(String child_id) {
            this.child_id = child_id;
        }
    }

    public static class Achievements {
        @SerializedName("image")
        private String image;
        @SerializedName("name")
        private String name;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
