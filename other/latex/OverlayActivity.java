package com.parental.control.teen.kid.math.education.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.loopeer.shadow.ShadowView;
import com.parental.control.teen.kid.math.education.R;
import com.parental.control.teen.kid.math.education.application.SharedPreferenceUtils;
import com.parental.control.teen.kid.math.education.models.AppLockNewModel;
import com.parental.control.teen.kid.math.education.models.QuestionsNewModel;
import com.parental.control.teen.kid.math.education.models.ReportRequestModel;
import com.parental.control.teen.kid.math.education.receivers.AppBroadcastReceiver;
import com.parental.control.teen.kid.math.education.services.LockService;
import com.parental.control.teen.kid.math.education.utility.AppConstants;
import com.parental.control.teen.kid.math.education.utility.Utils;
import com.parental.control.teen.kid.math.education.webservice.RxApiRequestHandler;
import com.parental.control.teen.kid.math.education.webservice.ServiceCallBack;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import katex.hourglass.in.mathlib.MathView;

public class OverlayActivity extends AppCompatActivity implements ServiceCallBack {

    AppLockNewModel.Data appdata;
    Context context;
    @BindView(R.id.txtQuestion)
    MathView txtQuestion;
    @BindView(R.id.txtOne)
    MathView txtOne;
    @BindView(R.id.txtTwo)
    MathView txtTwo;
    @BindView(R.id.txtThree)
    MathView txtThree;
    @BindView(R.id.txtFour)
    MathView txtFour;
    @BindView(R.id.cardOne)
    ShadowView cardOne;
    @BindView(R.id.cardTwo)
    ShadowView cardTwo;
    @BindView(R.id.cardThree)
    ShadowView cardThree;
    @BindView(R.id.cardFour)
    ShadowView cardFour;
    @BindView(R.id.edtPin)
    EditText edtPin;
    @BindView(R.id.layoutBase)
    LinearLayout layoutBase;

    int currentQueNumber = 0;
    boolean firtAttemp = true;
    String reported_QID = "";
    QuestionsNewModel questionsNewModel;
    QuestionsNewModel.Data currentQue;
    @BindView(R.id.layoutBackground)
    LinearLayout layoutBackground;
    @BindView(R.id.txtTopic)
    TextView txtTopic;
    @BindView(R.id.layoutHint)
    LinearLayout layoutHint;
    @BindView(R.id.layoutTooltipDim)
    View layoutTooltipDim;
    @BindView(R.id.layoutTooltip)
    LinearLayout layoutTooltip;

    long oldtimemillis = 0, timeDifference = 0;
    boolean isHintVisible=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = OverlayActivity.this;
        try {
            Settings.System.putInt(getContentResolver(), "multi_window_enabled", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (isTablet(context)) {
//            ((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_overlay_l);
//            ((TextView)findViewById(R.id.txtTopic)).setText("testT");


        } else {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_overlay);
//            ((TextView)findViewById(R.id.txtTopic)).setText("testP");

        }

        ButterKnife.bind(this);
        overridePendingTransition(0, 0);
        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
            layoutBase.setVisibility(View.VISIBLE);
        }
        init();

    }


    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }


    private void init() {
        Utils.setpasswordBigDot(context, null, edtPin);

        Utils.resetNewDay();

        appdata = new Gson().fromJson(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.OVERLAY_INFO), AppLockNewModel.Data.class);
//        Log.e("TAG", "runoverlay0: " );

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!OverlayActivity.this.isFinishing()) {
//                    Log.e("TAG", "runoverlay: " );
                    try {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String formattedDate = df.format(c.getTime());
                        RxApiRequestHandler.getOverlayQuestionsApi(context, OverlayActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID)
                                , SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID), formattedDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 400);

        edtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (StringUtils.isNoneEmpty(Utils.gettext(edtPin)) && Utils.gettext(edtPin).length() >= 4) {
                    if (checkValidation()) {
                        Utils.hideKeypad(context, edtPin);
                        Utils.setPause(context, 30);
                        finish();
//                        finishAndRemoveTask();
                        overridePendingTransition(0, 0);
                    }

                }
            }
        });

    }


    private boolean checkValidation() {
        if (!Utils.gettext(edtPin).equalsIgnoreCase(SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.PIN))) {
            Utils.setToast(context, context.getResources().getString(R.string.err_wrong_pin));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if(isHintVisible){
            isHintVisible=false;
            layoutTooltip.setVisibility(View.GONE);
            layoutTooltipDim.setVisibility(View.GONE);
        }else {
            try {
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_HOME);
                this.startActivity(homeIntent);
                this.finish();
//            this.finishAndRemoveTask();
                overridePendingTransition(0, 0);
                Utils.startService(OverlayActivity.this, LockService.class, AppBroadcastReceiver.class, AppConstants.ServiceTypes.LOCK_REQ);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

   /* @OnClick({R.id.txtRequest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtRequest:
                SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK,data.getApp_id());
                finish();
                break;
        }
    }*/


    @OnClick({R.id.txtOne, R.id.txtTwo, R.id.txtThree, R.id.txtFour, R.id.layoutBackground,R.id.layoutHint,R.id.layoutTooltipDim})
    public void onViewClicked(View view) {
        try {
            Utils.hideKeypad(context, view);
            clearButtonsTheme();
            if (edtPin.hasFocus()) {
                edtPin.setText("");
                edtPin.clearFocus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (view.getId()) {
            case R.id.layoutBackground:
                break;
            case R.id.txtOne:
                checkAnswer(currentQue.getAnswer().get(0).getIs_correct(), cardOne, txtOne, currentQue.getAnswer().get(0));
                break;
            case R.id.txtTwo:
                checkAnswer(currentQue.getAnswer().get(1).getIs_correct(), cardTwo, txtTwo, currentQue.getAnswer().get(1));
                break;
            case R.id.txtThree:
                checkAnswer(currentQue.getAnswer().get(2).getIs_correct(), cardThree, txtThree, currentQue.getAnswer().get(2));
                break;
            case R.id.txtFour:
                checkAnswer(currentQue.getAnswer().get(3).getIs_correct(), cardFour, txtFour, currentQue.getAnswer().get(3));
                break;
            case R.id.layoutHint:
            case R.id.layoutTooltipDim:
                showHideHint();
                break;
        }
    }

    private void showHideHint() {
        if(isHintVisible){
            isHintVisible=false;
            layoutTooltip.setVisibility(View.GONE);
            layoutTooltipDim.setVisibility(View.GONE);
        }else {
            isHintVisible=true;
            layoutTooltip.setVisibility(View.VISIBLE);
            layoutTooltipDim.setVisibility(View.VISIBLE);
        }
    }

    private void setTime() {
        //Calculate time difference ( last action to current action)
        timeDifference = System.currentTimeMillis() - oldtimemillis;
        Log.e("TAG", "Time till last action: " + timeDifference);
        //Reset timer
        oldtimemillis = System.currentTimeMillis();
    }

    private void checkAnswer(String isCorrect, ShadowView shadowView, MathView txt, QuestionsNewModel.Answer answer) {
        setTime();
        if (isCorrect.equalsIgnoreCase("1")) {
            reportApi(true, answer);
            playSound(shadowView, txt, true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (currentQueNumber + 1 == questionsNewModel.getData().size()) {
                        nodata();
                    } else {
                        currentQueNumber++;
                        setQuestion(true);
                    }
                }
            }, 500);
        } else {
//            firtAttemp = false;
            reportApi(false, answer);
            playSound(shadowView, txt, false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    firtAttemp = false;
                    setQuestion(false);
                }
            }, 500);
        }
    }

    private void reportApi(boolean iscorrect, QuestionsNewModel.Answer answer) {
        try {
//            if (!reported_QID.equalsIgnoreCase(currentQue.getFq_id())) {
//                reported_QID = currentQue.getFq_id();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c.getTime());

//                RxApiRequestHandler.sendReportApi(context, OverlayActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID)
//                        , firtAttemp, iscorrect, formattedDate, currentQue.getFq_id(), SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID));

            ReportRequestModel.Answer answerModel = new ReportRequestModel.Answer();
            answerModel.setIndex(answer.getIndex());
            answerModel.setId(answer.getId());
            answerModel.setTime_duration(String.valueOf(timeDifference));
//            Toast.makeText(context, "firtAttemp:"+firtAttemp+"\n"+"text:"+answer.getName()+"\n"
//                    +"index:"+answer.getIndex()+"\n"+"id:"+answer.getId()+"\n"
//                    +"diff:"+String.valueOf(timeDifference)+"\n", Toast.LENGTH_LONG).show();

            RxApiRequestHandler.sendReportApi2(context, OverlayActivity.this, SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.C_ID)
                    , firtAttemp, iscorrect, formattedDate, currentQue.getFq_id(), SharedPreferenceUtils.preferenceGetString(AppConstants.SharedPreferenceKeys.P_ID), answerModel);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playSound(ShadowView shadowView, MathView txt, boolean isCorrect) {
//        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (isCorrect) {
            txt.setTextColor(ContextCompat.getColor(context, R.color.selected));
            shadowView.setShadowColor(ContextCompat.getColor(context, R.color.selected));
//            if (audio.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
//                MediaPlayer.create(context, R.raw.correct).start();
//            }
        } else {
            txt.setTextColor(ContextCompat.getColor(context, R.color.red));
            shadowView.setShadowColor(ContextCompat.getColor(context, R.color.red));
//            if (audio.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
//                MediaPlayer.create(context, R.raw.incorrect).start();
//            }
        }
    }


    private void clearButtonsTheme() {
        txtOne.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        txtTwo.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        txtThree.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        txtFour.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        cardOne.setShadowColor(ContextCompat.getColor(context, android.R.color.darker_gray));
        cardTwo.setShadowColor(ContextCompat.getColor(context, android.R.color.darker_gray));
        cardThree.setShadowColor(ContextCompat.getColor(context, android.R.color.darker_gray));
        cardFour.setShadowColor(ContextCompat.getColor(context, android.R.color.darker_gray));
    }

    @Override
    public void onSuccess(String requestTag, JsonElement data1) {
        if (!OverlayActivity.this.isFinishing()) {
            if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SEND_REPORT)) {
                Log.e("TAG", "onSuccess: report");
            } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_OVERLAY_QUESTIONS)) {
                if (!OverlayActivity.this.isFinishing()) {
                    try {
                        questionsNewModel = new Gson().fromJson(data1, QuestionsNewModel.class);
                        if (questionsNewModel != null && questionsNewModel.getData() != null && questionsNewModel.getData().size() > 0) {
                            Utils.statusbarTransparent(OverlayActivity.this, false, false);
                            if (isTablet(context)) {
                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int width = displayMetrics.widthPixels;
                                int height = displayMetrics.heightPixels;
                                int newwidth = 0, newheight = 0;
                                if (height > width) {
                                    newwidth = height;
                                    newheight = width;
                                } else {
                                    newwidth = width;
                                    newheight = height;
                                }
                                newheight = newheight - (int) context.getResources().getDimension(R.dimen._10sdp);
                                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(newwidth, newheight);
                                layoutBase.setLayoutParams(param);
                            }
                            layoutBase.setVisibility(View.VISIBLE);
                            setQuestion(true);
                        } else {
                            nodata();
                        }

                    } catch (Exception e) {
                        nodata();
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height_landscape", "dimen", "android");
        if (resourceId <= 0) {
            resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        }
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getNavBarHeight() {

        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height_landscape", "dimen", "android");
        if (resourceId <= 0) {
            resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        }
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private void nodata() {
        SharedPreferenceUtils.preferencePutString(AppConstants.SharedPreferenceKeys.TEMP_UNLOCK, appdata.getApp_id());
        finish();
//        finishAndRemoveTask();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onFailure(String requestTag, String message, String errorcode) {
        if (!OverlayActivity.this.isFinishing()) {
            if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.SEND_REPORT)) {
                Log.e("TAG", "onSuccess: report");
            } else if (requestTag.equalsIgnoreCase(AppConstants.ApiTags.GET_OVERLAY_QUESTIONS)) {
                nodata();
            }
        }
    }

    private void setQuestion(boolean newquestion) {
        oldtimemillis = System.currentTimeMillis();
        if (newquestion) {
            firtAttemp = true;
        }
        clearButtonsTheme();
        currentQue = questionsNewModel.getData().get(currentQueNumber);
        txtTopic.setText(currentQue.getTopic_name());
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtQuestion.setText(Html.fromHtml(currentQue.getQuestion_name(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            txtQuestion.setText(Html.fromHtml(currentQue.getQuestion_name()));
        }*/
        txtQuestion.setDisplayText(currentQue.getQuestion_name());
//        currentQue.getAnswer().clear();
//        List<QuestionsNewModel.Answer> arrayans=new ArrayList<>();
//        arrayans.add(new Gson().fromJson("{index:1,id:3917,name:41,is_correct:0}",QuestionsNewModel.Answer.class));
//        arrayans.add(new Gson().fromJson("{\"index\": 2,\"id\": \"3917\",\"name\": \"42\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
//        arrayans.add(new Gson().fromJson("{\"index\": 3,\"id\": \"3917\",\"name\": \"43\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
//        arrayans.add(new Gson().fromJson("{\"index\": 4,\"id\": \"3917\",\"name\": \"44\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
//        arrayans.add(new Gson().fromJson("{\"index\": 5,\"id\": \"3917\",\"name\": \"45\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
//        arrayans.add(new Gson().fromJson("{\"index\": 6,\"id\": \"3917\",\"name\": \"46\",\"is_correct\": \"0\"}",QuestionsNewModel.Answer.class));
//        currentQue.setAnswer(arrayans);
//        txtQuestion.setText(currentQue.getQuestion_name());
//        Collections.shuffle(currentQue.getAnswer(),new Random());

        if(!newquestion) {
            try {
         /*   ArrayList<Integer> intarrat = new ArrayList<>();
            final int min = 0;
            final int max = 3;
            while (intarrat.size() < 4) {
                int random = new Random().nextInt((max - min) + 1) + min;
                if (!intarrat.contains(random)) {
                }
                intarrat.add(random);
            }*/


                ArrayList<Integer> content = new ArrayList<Integer>();

                for (int i = 0; i < currentQue.getAnswer().size(); i++) {
                    content.add(i);
                }

                ArrayList<Integer> intarrat = new ArrayList<Integer>();

                Random random = new Random();
                while (content.size() > 0) {
                    int randomIndex = random.nextInt(content.size());
                    intarrat.add(content.remove(randomIndex));
                }


                ArrayList<QuestionsNewModel.Answer> answers = new ArrayList<>();
                for (int i = 0; i < intarrat.size(); i++) {
                    Log.e("TAG", "setQuestion: " + intarrat.get(i));
                    try {
                        answers.add(currentQue.getAnswer().get(intarrat.get(i)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                currentQue.getAnswer().clear();

                currentQue.setAnswer(answers);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        Collections.shuffle(currentQue.getAnswer());

        try {
            cardOne.setVisibility(View.VISIBLE);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtOne.setText(Html.fromHtml(currentQue.getAnswer().get(0).getName(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                txtOne.setText(Html.fromHtml(currentQue.getAnswer().get(0).getName()));
            }*/
            txtOne.setDisplayText(currentQue.getAnswer().get(0).getName());
        } catch (Exception e) {
            cardOne.setVisibility(View.GONE);
        }
        try {
            cardTwo.setVisibility(View.VISIBLE);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtTwo.setText(Html.fromHtml(currentQue.getAnswer().get(1).getName(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                txtTwo.setText(Html.fromHtml(currentQue.getAnswer().get(1).getName()));
            }*/
            txtTwo.setDisplayText(currentQue.getAnswer().get(1).getName());
        } catch (Exception e) {
        }
        cardTwo.setVisibility(View.GONE);
        try {
            cardThree.setVisibility(View.VISIBLE);
           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtThree.setText(Html.fromHtml(currentQue.getAnswer().get(2).getName(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                txtThree.setText(Html.fromHtml(currentQue.getAnswer().get(2).getName()));
            }*/
            txtThree.setDisplayText(currentQue.getAnswer().get(2).getName());
        } catch (Exception e) {
            cardThree.setVisibility(View.GONE);
        }
        try {
            cardFour.setVisibility(View.VISIBLE);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtFour.setText(Html.fromHtml(currentQue.getAnswer().get(3).getName(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                txtFour.setText(Html.fromHtml(currentQue.getAnswer().get(3).getName()));
            }*/
            txtFour.setDisplayText(currentQue.getAnswer().get(3).getName());
        } catch (Exception e) {
            cardFour.setVisibility(View.GONE);
        }

        txtTopic.setText(currentQue.getTopic_name());
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtQuestion.setText(Html.fromHtml(currentQue.getQuestion_name(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            txtQuestion.setText(Html.fromHtml(currentQue.getQuestion_name()));
        }*/we
        txtQuestion.setDisplayText(currentQue.getQuestion_name());

    }

}
