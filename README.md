# 1Question

1Question that enables rote learning of foundational elements of a curriculum by integrating the delivery of flashcard-style questions into a user’s daily device usage. The 1Question app allows admin (parents) to define parts of a curriculum to be practiced (eg, year 4 math) and the app will then present the device user (child) with flashcard questions before they are allowed to access particular apps (admin decides trigger apps).

## Objectives

Admin experience – easy setup, customisation, simple reporting, uncomplicated adjustments to setting, requiring minimal interaction after setup. 

User experience – presented with repeated opportunities for revision of relevant information in the form of quick interruptions (a question to answer) to specific apps, with minimal overall disruption once answered correctly.

## Installation

Download & Install [Android Studio IDE](https://developer.android.com/studio/?gclid=CjwKCAjw-5v7BRAmEiwAJ3DpuH16xib3Ui5sgeah3kXzotzDASUKyhgiuTbhgqcuGz1lT6PDR3U0ohoCQnoQAvD_BwE&gclsrc=aw.ds#downloads)

## Clone 

Clone project from [Bitbucket](https://dev5-itpathsolutions@bitbucket.org/dev5-itpathsolutions/dca_android.git)

## Import

```
   Start Android Studio & Click 
   Open an existing android studio project
   Navigate to your project directory select android studio icon of your project folder
   Press => OK
```

## Gradle

   Gradle will start build your project do connect your android device via usb with your machine

## API URL

   Find AppConstant.java file and change api url with your local ip based api url

```
   ..\dca_android\app\src\main\java\com\example\dca\utility\AppConstants.java
```
   Change below api url with your ip url
```
   public static final String API_URL = "http://questionapp.project-demo.info:8081/api/"
```

## Run
   Build & Run the project in emulator/physical android device

## Build Flavours

   Select the flavour you want to run from the Build varient from android project settings
```
   productFlavors
   - normal (Is for normal OS android devices)
   - go     (Is for GO OS android devices)
```
Selecting flavour would dynamically change the code base and app conditions as per selection

Manual change need as per selected Flavour
```
   productFlavors
   - normal (Comment <uses-feature android:name="android.hardware.ram.low" android:required="true">)
   - go     (Uncomment <uses-feature android:name="android.hardware.ram.low" android:required="true">)
```